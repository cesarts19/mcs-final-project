package com.example.mcs.interviewhelper.Model.data;

import java.security.Timestamp;
import java.util.ArrayList;
import java.util.Date;

/**
 * Implements the set of questions for the consultant to answer
 */

public class Quiz {

    private ArrayList<BasicQuestion> mQuestion;
    private Date mSubmissionDate;
    private String mAnsweredBy;

    public Quiz(){}

    public Quiz(QuizWrapper quizWrapper) {
        mQuestion = new ArrayList<BasicQuestion>();
        for (int i = 0; i < quizWrapper.Questions.size(); i++) {
            mQuestion.add(new BasicQuestion(quizWrapper.Questions.get(i)));
        }
        try{
            mSubmissionDate = new Date(quizWrapper.Timestamp);
        }catch(Exception e){
            e.printStackTrace();
            mSubmissionDate = new Date();
        }

        mSubmissionDate = new Date();//Long.parseLong(quizWrapper.Timestamp));
        mAnsweredBy = fromAscRepresentation(quizWrapper.RespondedBy);
    }

    private String fromAscRepresentation(String input) {
        String output = "";
        String[] parts = input.split("a");

        for (int i = 0; i < parts.length; i++) {
            if(parts[i].equals("")){
                continue;
            }
            output += (char) Integer.parseInt(parts[i]);
        }
        return output;
    }

    public ArrayList<BasicQuestion> getmQuestion() {
        return mQuestion;
    }

    public void setmQuestion(ArrayList<BasicQuestion> mQuestion) {
        this.mQuestion = mQuestion;
    }

    public Date getmSubmissionDate() {
        return mSubmissionDate;
    }

    public void setmSubmissionDate(Date mSubmissionDate) {
        this.mSubmissionDate = mSubmissionDate;
    }

    public String getmAnsweredBy() {
        return mAnsweredBy;
    }

    public void setmAnsweredBy(String mAnsweredBy) {
        this.mAnsweredBy = mAnsweredBy;
    }

    @Override
    public String toString() {
        return "Quiz{" +
                "mQuestion=" + mQuestion +
                ", mSubmissionDate=" + mSubmissionDate +
                ", mAnsweredBy='" + mAnsweredBy + '\'' +
                '}';
    }
}
