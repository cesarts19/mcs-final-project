package com.example.mcs.interviewhelper.View.Suggestions;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.mcs.interviewhelper.R;

/**
 * @itzcoatl90
 * @roberto.cervantes1988@gmail.com
 *
 * Fragment that holds the first Screen for both, consultant and trainer.
 *
 * */
public class QuestionTypeSuggestionSelectionScreenFragment extends Fragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_question_type_suggestion_selection_screen, container, false);
    }

}
