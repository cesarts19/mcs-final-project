package com.example.mcs.interviewhelper.Model.data;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.example.mcs.interviewhelper.LoginActivity;
import com.example.mcs.interviewhelper.MainActivity;
import com.example.mcs.interviewhelper.R;

import java.util.HashSet;

import static android.content.Context.MODE_PRIVATE;

public class Review_Tags_Wrapper_Command implements Command {

    Context mContext;

    public Review_Tags_Wrapper_Command(Context mContext){
        this.mContext = mContext;
    }

    @Override
    public void executeOnSendSuccessful() {
        throw new UnsupportedOperationException("NOT YET SUPPORTED");
    }

    @Override
    public void executeOnSendFail() {
        throw new UnsupportedOperationException("NOT YET SUPPORTED");
    }

    @Override
    public void executeOnReceiveSuccessful() {
        if(MainActivity.dataBaseTags.size() > 0){
            SharedPreferences preferences = ((Activity) mContext).getPreferences(MODE_PRIVATE);
            String tagsKey = mContext.getString(R.string.preference_key_tags);
            preferences.getStringSet(tagsKey, new HashSet<>(MainActivity.dataBaseTags));
        }
    }

    @Override
    public void executeOnReceiveFail() {

    }
}
