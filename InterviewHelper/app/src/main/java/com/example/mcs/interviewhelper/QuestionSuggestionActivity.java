package com.example.mcs.interviewhelper;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.RadioButton;
import android.widget.Toast;

import com.example.mcs.interviewhelper.Model.BackendCallers.BackendCaller;
import com.example.mcs.interviewhelper.Model.BackendCallers.FirebaseCaller;
import com.example.mcs.interviewhelper.Model.Interactors.ConsultantInteractor;
import com.example.mcs.interviewhelper.Model.Interactors.RolInteractor;
import com.example.mcs.interviewhelper.Model.Interactors.TrainerInteractor;
import com.example.mcs.interviewhelper.Model.data.BasicQuestion;
import com.example.mcs.interviewhelper.Model.data.Command;
import com.example.mcs.interviewhelper.Model.data.MultipleChoiceQuestion;
import com.example.mcs.interviewhelper.Model.data.Review_Tags_Wrapper_Command;
import com.example.mcs.interviewhelper.Model.data.User;
import com.example.mcs.interviewhelper.Presenter.GenericPresenter;
import com.example.mcs.interviewhelper.Presenter.SuggestionPresenters.MultipleChoiceQuestionSuggestionPresenter;
import com.example.mcs.interviewhelper.Presenter.SuggestionPresenters.OpenQuestionSuggestionPresenter;
import com.example.mcs.interviewhelper.Presenter.SuggestionPresenters.QuestionSuggestionPresenter;
import com.example.mcs.interviewhelper.View.Suggestions.QuestionEditingScreenFragment;

public class QuestionSuggestionActivity extends AppCompatActivity implements Command {

    public static RolInteractor INTERACTOR = null;
    public static QuestionSuggestionPresenter PRESENTER = null;
    public static BasicQuestion QUESTION_PRESENTING = null;
    public GenericPresenter GENERIC_PRESENTER = null;
    public User USER = null;
    public static Fragment NEXT_FRAGMENT = null;
    public Fragment PREVIOUS_FRAGMENT = null;
    public static QuestionSuggestionActivity CONTEXT = null;
    public int difficulty = -1;
    public ProgressDialog proDialog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question_suggestion);
        inits();
        INTERACTOR.startFirstFragmentOnSuggestion(this);
    }

    private void inits(){
        USER = getIntent().getExtras().getParcelable(User.USER_PARCELABLE_NAME);
        INTERACTOR = (USER.getUserRol() == 1) ? new ConsultantInteractor() : new TrainerInteractor();
        GENERIC_PRESENTER = new GenericPresenter();
        CONTEXT = this;
    }

    /**
     * @itzcoatl90
     *
     * This method is for changing the fragment instead of using a fragment stack.
     * A better practice here is to use that stack.
     *
     * */
    public void startNextFragment(Fragment fragment){
        NEXT_FRAGMENT = fragment;
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.suggestion_container, NEXT_FRAGMENT)
                .commit();
    }

    /**
     * @itzcoatl90
     *
     * This two methods startTrainingSuggestionValidation and startInterviewSuggestionValidation
     * are the listeners for the buttons on the first fragment with buttons (and starts the presenter and next screen
     * depending on the Rol).
     *
     * */
    public void startTrainingSuggestionValidation(View view) {
        QUESTION_PRESENTING = null;
        PRESENTER = new MultipleChoiceQuestionSuggestionPresenter();
        INTERACTOR.goToNextScreen(this);
    }

    public void startInterviewSuggestionValidation(View view) {
        QUESTION_PRESENTING = null;
        PRESENTER = new OpenQuestionSuggestionPresenter();
        INTERACTOR.goToNextScreen(this);
    }

    /**
     * @itzcoatl90
     *
     * This four methods validateInput, cancelSuggestion, acceptSuggestion and dismissSuggestion
     * are the listener for the buttons on the last fragment of the tree with toolbar.
     *
     * What they do is save to db depending with different states:
     *     10 if is the first suggestion made by a consultant.
     *     5 if is dismissed by the trainer
     *     1 if is accepted by the trainer
     *
     * The cancelSuggestion only returns to last screen (fragment).
     *
     * */

    public void validateInput(View view) {
        QuestionEditingScreenFragment fragment = (QuestionEditingScreenFragment) NEXT_FRAGMENT;
        if(validate(fragment)){
            attempSendQuestion(fragment,10);
        }
    }

    public void cancelSuggestion(View view){
        INTERACTOR.goToPreviousScreen(this);
    }

    public void acceptSugestion(View view){
        QuestionEditingScreenFragment fragment = (QuestionEditingScreenFragment) NEXT_FRAGMENT;
        if(validate(fragment)){
            attempSendQuestion(fragment,1);
        }
    }

    public void dismissSugestion(View view){
        QuestionEditingScreenFragment fragment = (QuestionEditingScreenFragment) NEXT_FRAGMENT;
        if(validate(fragment)){
            attempSendQuestion(fragment,5);
        }
    }

    /**
     * @itzcoatl90
     *
     * This method makes the validation of the input. It cheks:
     *     1.- Question is filled for multiple choice and open questions.
     *     2.- At least 1 correct answer is filled for multiple choice question.
     *     3.- At least 3 wrong answer are filled for multiple choice question.
     *     4.- At least 1 topic tag is filled for multiple choice and open questions.
     *
     * */

    private boolean validate(QuestionEditingScreenFragment fragment){
        if(NEXT_FRAGMENT != null &&
                ((QuestionEditingScreenFragment) NEXT_FRAGMENT).question != null) {
            fragment.question.requestFocus();
        }

        if(fragment.question.getText().toString().equals("")){
            Toast.makeText(this,"You should have a question to send.",Toast.LENGTH_SHORT).show();
            return false;
        }
        if(QuestionEditingScreenFragment.TRUE_ANSWERS != null){
            if(QuestionEditingScreenFragment.TRUE_ANSWERS.size() < 1) {
                Toast.makeText(this,"You should have, at least 1 Correct Answers.",Toast.LENGTH_SHORT).show();
                return false;
            }
            for (int i = 0; i < QuestionEditingScreenFragment.TRUE_ANSWERS.size(); i++) {
                if(QuestionEditingScreenFragment.TRUE_ANSWERS.get(i).equals("")){
                    Toast.makeText(this,"You can't send empty Correct Answers.",Toast.LENGTH_SHORT).show();
                    return false;
                }
            }
        }
        if(QuestionEditingScreenFragment.WRONG_ANSWERS != null){
            if(QuestionEditingScreenFragment.WRONG_ANSWERS.size() < 3){
                Toast.makeText(this,"You can't send less than 3 Wrong Answers.",Toast.LENGTH_SHORT).show();
                return false;
            }
            for (int i = 0; i < QuestionEditingScreenFragment.WRONG_ANSWERS.size(); i++) {
                if(QuestionEditingScreenFragment.WRONG_ANSWERS.get(i).equals("")){
                    Toast.makeText(this,"You can't send empty Wrong Answers.",Toast.LENGTH_SHORT).show();
                    return false;
                }
            }
        }
        try {
            if (QuestionEditingScreenFragment.TOPIC_TAGS.size() < 1) {
                Toast.makeText(this, "You should have, at least 1 Topic Tag.", Toast.LENGTH_SHORT).show();
                return false;
            }
            for (int i = 0; i < QuestionEditingScreenFragment.TOPIC_TAGS.size(); i++) {
                if(QuestionEditingScreenFragment.TOPIC_TAGS.get(i).equals("")){
                    Toast.makeText(this,"You can't send empty Topic Tags.",Toast.LENGTH_SHORT).show();
                    return false;
                }
            }
        } catch(Exception e){
            e.printStackTrace();
            Log.e(this.getClass().getSimpleName(),"Seems like it's happening a null on a not nullable list <TAGS>.");
            return false;
        }
        if(difficulty == -1){
            Toast.makeText(this,"You should select a difficulty.",Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    /**
     * @itzcoatl90
     *
     * This method is called when validations are full-filled,
     * it sends the question to the backend caller.
     *
     * */

    private void attempSendQuestion(QuestionEditingScreenFragment fragment,int status){
        BackendCaller aux = new FirebaseCaller();
        if(QuestionEditingScreenFragment.TRUE_ANSWERS == null &&
                QuestionEditingScreenFragment.WRONG_ANSWERS == null) {
            BasicQuestion toSend;
            if(QUESTION_PRESENTING == null){
                toSend = new BasicQuestion(fragment.question.getText().toString(),
                        USER.getUserId(),"","",QuestionEditingScreenFragment.TOPIC_TAGS,
                        difficulty,status,-1, 0);
            } else {
                toSend = QUESTION_PRESENTING;
                toSend.setmQuestion(fragment.question.getText().toString());
                toSend.setmValidator(USER.getUserId());
                toSend.setmTags(QuestionEditingScreenFragment.TOPIC_TAGS);
                toSend.setmDifficulty(difficulty);
                toSend.setmStatus(status);
            }
            startProDialog();
            aux.sendQuestionSuggestion(toSend,this,this);
        } else {
            MultipleChoiceQuestion toSend;
            if(QUESTION_PRESENTING == null){
                toSend = new MultipleChoiceQuestion(fragment.question.getText().toString(),
                        USER.getUserId(),"","",QuestionEditingScreenFragment.TOPIC_TAGS,
                        difficulty,status,-1, 0,
                        QuestionEditingScreenFragment.TRUE_ANSWERS,
                        QuestionEditingScreenFragment.WRONG_ANSWERS);
            } else {
                toSend = (MultipleChoiceQuestion) QUESTION_PRESENTING;
                toSend.setmQuestion(fragment.question.getText().toString());
                toSend.setmValidator(USER.getUserId());
                toSend.setmTags(QuestionEditingScreenFragment.TOPIC_TAGS);
                toSend.setmDifficulty(difficulty);
                toSend.setmStatus(status);
                toSend.setmRightAnswer(QuestionEditingScreenFragment.TRUE_ANSWERS);
                toSend.setmWrongAnswer(QuestionEditingScreenFragment.WRONG_ANSWERS);
            }
            startProDialog();
            aux.sendQuestionSuggestion(toSend,this,this);
        }
    }

    /**
     * @itzcoatl90
     *
     * This methods are the implementation for the command pattern design.
     * Regardless of Async of Sync, this methods are called from the backends
     * to continue execution.
     * */

    @Override
    public void executeOnSendSuccessful() {
        if (proDialog.isShowing()) {
            proDialog.dismiss();
        }
        Toast.makeText(this,"Saved to DB.",Toast.LENGTH_LONG).show();
        inits();
        // Let's start everything over.
        PRESENTER = null;
        QUESTION_PRESENTING = null;
        NEXT_FRAGMENT = null;
        PREVIOUS_FRAGMENT = null;
        difficulty = -1;
        proDialog = null;
        BackendCaller firebase = new FirebaseCaller();
        firebase.requestTags(MainActivity.dataBaseTags,new Review_Tags_Wrapper_Command(MainActivity.CONTEXT));
        INTERACTOR.startFirstFragmentOnSuggestion(this);
    }

    @Override
    public void executeOnSendFail() {
        if (proDialog.isShowing()) {
            proDialog.dismiss();
        }
    }

    @Override
    public void executeOnReceiveSuccessful(){
        if (proDialog.isShowing()) {
            proDialog.dismiss();
        }
        PRESENTER.deployList();
    }

    @Override
    public void executeOnReceiveFail(){
        if (proDialog.isShowing()) {
            proDialog.dismiss();
        }
        inits();
        // Let's start everything over.
        PRESENTER = null;
        QUESTION_PRESENTING = null;
        NEXT_FRAGMENT = null;
        PREVIOUS_FRAGMENT = null;
        difficulty = -1;
        proDialog = null;
        INTERACTOR.startFirstFragmentOnSuggestion(this);
    }

    /**
     * @itzcoatl90
     *
     * Methods for multiple uses.
     *
     * */

    public void startProDialog(){
        proDialog = new ProgressDialog(this);
        proDialog.setMessage("Please wait...");
        proDialog.setCancelable(false);
        proDialog.show();
    }

    public void setDifficulty(View view){
        if(getString(R.string.difficulty_easy)
                .equals(((RadioButton) view).getText().toString())){
            difficulty = 10;
        } else if(getString(R.string.difficulty_medium)
                .equals(((RadioButton) view).getText().toString())){
            difficulty = 20;
        } else if(getString(R.string.difficulty_advanced)
                .equals(((RadioButton) view).getText().toString())){
            difficulty = 30;
        }
    }

    public Toolbar getMainToolbar(){
        return (Toolbar) findViewById(R.id.toolbar);
    }

    public Toolbar getConsultantToolbar(){
        return (Toolbar) findViewById(R.id.toolbar_consultant);
    }

    public Toolbar getTrainerToolbar(){
        return (Toolbar) findViewById(R.id.toolbar_trainer);
    }

    public void setMainToolbar(){
        GENERIC_PRESENTER.setToolbarForSuggestionActivity(this);
    }

}
