package com.example.mcs.interviewhelper.Model.data.ContentProvider;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;


public class TrainerFeedbackTable {
    public static  final  String TABLE_NAME = "TRAINERFEEDBACK";
    public static  final  String COLUMN_ID = "_id";
    public static  final  String COLUMN_USER = "user";
    public static  final  String COLUMN_TOTAL = "total";
    public static  final  String COLUMN_CORRECT = "correct";

    public static final String CREATE_TRAINER_FEEDBACK_TABLE =
            "CREATE TABLE IF NOT EXISTS " + TABLE_NAME
                    + "("+ COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + COLUMN_USER + " TEXT NOT NULL, "
                    + COLUMN_TOTAL + " INTEGER DEFAULT 0, "
                    + COLUMN_CORRECT + " INTEGER DEFAULT 0"
                    + ");";

    public static void onCreate(SQLiteDatabase database){
        database.execSQL(CREATE_TRAINER_FEEDBACK_TABLE);
    }
    public static void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion){
        Log.d(TrainerFeedbackTable.class.getSimpleName(), "Upgrading database from " + oldVersion + " to " + newVersion + ", destroying local data.");
        database.execSQL("DROP TABLE IF EXIST " + TABLE_NAME);
        onCreate(database);
    }

}
