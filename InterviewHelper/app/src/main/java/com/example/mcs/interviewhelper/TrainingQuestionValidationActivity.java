package com.example.mcs.interviewhelper;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.mcs.interviewhelper.View.Suggestions.QuestionListScreenFragment;

public class TrainingQuestionValidationActivity extends AppCompatActivity {

    private QuestionListScreenFragment validatingScreenFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_training_question_validation);
        validatingScreenFragment = new QuestionListScreenFragment();
        this.getSupportFragmentManager().beginTransaction().replace(R.id.validate_question, validatingScreenFragment).commit();
    }
}
