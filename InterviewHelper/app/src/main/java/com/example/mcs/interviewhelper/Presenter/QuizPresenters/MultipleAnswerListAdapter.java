package com.example.mcs.interviewhelper.Presenter.QuizPresenters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;

import com.example.mcs.interviewhelper.QuizActivity;
import com.example.mcs.interviewhelper.R;

import java.util.ArrayList;

import static com.example.mcs.interviewhelper.QuizActivity.NQUESTIONS;

public class MultipleAnswerListAdapter extends BaseAdapter {
    private ArrayList<String> mItem;
    private Context mContext;
    private View top;

    MultipleAnswerListAdapter(ArrayList<String> mItem, Context mContext, View top) {
        this.mItem = mItem;
        this.mContext = mContext;
        this.top = top;
    }

    @Override
    public int getCount() {
        return mItem.size();
    }

    @Override
    public Object getItem(int position) {
        return mItem.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            LayoutInflater layoutInflater = LayoutInflater.from(mContext);
            convertView = layoutInflater.inflate(R.layout.quiz_multiple_answer_list_item_layout, null);
        }
        String value = mItem.get(position);
        Button AnswerText = (Button) convertView.findViewById(R.id.AnswerItem);
        QuizActivity.QPRESENTER.drawAnswer(AnswerText,value);

        AnswerText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                QuizActivity.RESPONSES.set(QuizActivity.CURRENTQUESTION,position);
                QuizActivity.STRINGS_RESPONSES.add(((Button) v).getText().toString());

                if(QuizActivity.CURRENTQUESTION < NQUESTIONS-1) {
                    QuizActivity.INTERACTOR.loadNextQuestion(top);
                } else {
                    QuizActivity.INTERACTOR.createDialog();
                }

            }
        });

        return convertView;
    }


}
