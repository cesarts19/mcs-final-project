package com.example.mcs.interviewhelper.Presenter.QuizPresenters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.mcs.interviewhelper.QuizActivity;
import com.example.mcs.interviewhelper.R;
import com.example.mcs.interviewhelper.View.QuizViews.QuizFragment;

import static com.example.mcs.interviewhelper.R.string.next;

public class OpenQuestionPresenter implements QuestionPresenter {

    @Override
    public void doAnswerLayout(QuizFragment fragment, View answerContainer,
                               View NextButton, View BackButton) {
        LayoutInflater layoutInflater = (LayoutInflater)
                QuizActivity.CONTEXT.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View answer = layoutInflater.inflate(R.layout.quiz_open_answer_layout,null);
        answer.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
        ((LinearLayout) answerContainer).addView(answer);
        QuizActivity.INTERACTOR.setNavigationNextButtonListener(NextButton);
        QuizActivity.INTERACTOR.setNavigationBackButtonListener(BackButton);


    }

    @Override
    public void presentNextQuestion(View view) {
        TextView question = QuizActivity.INTERACTOR.getQuestionView(view);
        question.setText(QuizActivity.QUIZ2SHOW.get(QuizActivity.CURRENTQUESTION).getmQuestion());

    }

    @Override
    public void drawAnswer(View view, String Value) {
        EditText answer = (EditText)QuizActivity.INTERACTOR.getAnswerView(view);
        answer.setText(Value);

        //TODO Check this and use it to present the answer on the EditText
    }

}
