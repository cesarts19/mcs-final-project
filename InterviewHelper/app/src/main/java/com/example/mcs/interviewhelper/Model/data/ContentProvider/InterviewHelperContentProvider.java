package com.example.mcs.interviewhelper.Model.data.ContentProvider;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.Arrays;
import java.util.HashSet;


public class InterviewHelperContentProvider extends ContentProvider {

    public static final  String AUTHORITY ="com.example.mcs.interviewhelper.Model.data." +
            "ContentProvider.InterviewHelperContentProvider";
    private SQLiteDatabase database;
    private static final UriMatcher sUriMatcher;
    private static final String BASE_PATH = "tags";
    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + BASE_PATH);

    //Urimarcher ID's
    private static final int TAGS = 10;
    private static final int TAG_ID = 20;

    static {
        sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        sUriMatcher.addURI(AUTHORITY, BASE_PATH, TAGS);
        sUriMatcher.addURI(AUTHORITY, BASE_PATH + "/#", TAG_ID);
    }

    @Override
    public boolean onCreate() {
        InterviewHelperDatabaseHelper databaseHelper = new InterviewHelperDatabaseHelper(getContext());
        database = databaseHelper.getWritableDatabase();
        return databaseHelper != null;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        checkColumns(projection);
        queryBuilder.setTables(TagsTable.TABLE_NAME);
        int uriType = sUriMatcher.match(uri);

        switch (uriType){
            case TAGS:
                break;
            case TAG_ID:
                queryBuilder.appendWhere(TagsTable.COLUMN_ID + "=" + uri.getLastPathSegment());
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }

        Cursor cursor = queryBuilder.query(database,projection,selection,selectionArgs,null,null,sortOrder);
        return cursor;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        int uriType = sUriMatcher.match(uri);
        long id = 0;

        switch (uriType){
            case TAGS:
                id = database.insert(TagsTable.TABLE_NAME, null, values);
                break;
            default:
                throw new IllegalArgumentException("Unknown Uri " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return Uri.parse(BASE_PATH + "/" + id);
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        int uriType = sUriMatcher.match(uri);
        int id = 0;

        switch (uriType){
            case TAGS:
                id = database.delete(TagsTable.TABLE_NAME, selection, selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Unknown Uri " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return id;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        int uriType = sUriMatcher.match(uri);
        int updated = 0;

        switch (uriType){
            case TAGS:
                updated = database.update(TagsTable.TABLE_NAME, values, selection, selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Unknown Uri " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return updated;
    }

    private void checkColumns(String [] projection){
        String [] available = {TagsTable.COLUMN_ID,
                TagsTable.COLUMN_USER,
                TagsTable.COLUMN_TAG,
                TagsTable.COLUMN_TOTAL,
                TagsTable.COLUMN_CORRECT
        };


        if(projection != null){
            HashSet<String> requestedColumns = new HashSet<String>(Arrays.asList(projection));
            HashSet<String> availableColumns = new HashSet<String>(Arrays.asList(available));


            //Check if all the columns requested are available
            if(availableColumns.containsAll(requestedColumns)) {
                Log.d("TRUE", "IT IS");
            }else{
                throw new IllegalArgumentException("NOT EQUALS");
            }

        }else{
            Log.d("EMPTY","It is Empty");
        }
    }
}
