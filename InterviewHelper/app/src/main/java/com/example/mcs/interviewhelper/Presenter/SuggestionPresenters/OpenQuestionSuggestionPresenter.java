package com.example.mcs.interviewhelper.Presenter.SuggestionPresenters;

import android.content.Context;
import android.os.Parcel;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;

import com.example.mcs.interviewhelper.Model.BackendCallers.BackendCaller;
import com.example.mcs.interviewhelper.Model.BackendCallers.FirebaseCaller;
import com.example.mcs.interviewhelper.Model.data.Constants;
import com.example.mcs.interviewhelper.QuestionSuggestionActivity;
import com.example.mcs.interviewhelper.R;
import com.example.mcs.interviewhelper.View.Suggestions.QuestionEditingScreenFragment;
import com.example.mcs.interviewhelper.View.Suggestions.QuestionListScreenFragment;

import java.util.ArrayList;

/**
 * Presenter implementation for interview questions suggestion
 */

public class OpenQuestionSuggestionPresenter extends QuestionSuggestionPresenter {

    AddAndRemoveItemsAdapter TAGS_ADAPTER;
    ShowListItemsAdapter SUGGESTED_ADAPTER;
    View listContainer;

    public OpenQuestionSuggestionPresenter(){
        TAGS_ADAPTER = null;
        SUGGESTED_ADAPTER = null;
    }

    @Override
    public void doEditPresentation(QuestionEditingScreenFragment fragment, View answerContainer,
                                   View tagsContainer, View buttonAddTag) {
        QuestionEditingScreenFragment.TOPIC_TAGS.add("");
        TAGS_ADAPTER =
                new AddAndRemoveItemsAdapter(
                        QuestionEditingScreenFragment.TOPIC_TAGS,
                        QuestionSuggestionActivity.CONTEXT,
                        QuestionSuggestionActivity.INTERACTOR);
        ListView topicTagListView =
                (ListView) tagsContainer.findViewById(R.id.lay_topic_tags_container);
        topicTagListView.setAdapter(TAGS_ADAPTER);
        QuestionSuggestionActivity.INTERACTOR.bindListenersToAddButtons(
                null,null,null,null,null,null,
                buttonAddTag,
                QuestionEditingScreenFragment.TOPIC_TAGS,
                TAGS_ADAPTER
        );
    }

    /**
     * @roberto.cervantes1988@gmail.com
     *
     * Function that perform the presentation of the selected suggested question from the list of suggestions.
     * It receives the views involved and the data to be showed within every view.
     * For Open Questions type it is showing:
     * Question Suggested, Radio Button checked according to the difficulty suggested,
     * List of tags suggested for the question.
     *
     * @itzcoatl90
     *
     * Made changes on @roberto.cervantes1988@gmail.com code, the "correct" parameter on doSuggestPresentation
     * is now an ArrayList.
     *
     * */
    @Override
    public void doSuggestPresentation(QuestionEditingScreenFragment fragment,
                                      View questionContainer, View answerContainer,
                                      View tagsContainer, View buttonAdd,
                                      View radioEasy, View radioMedium, View radioAdvance,
                                      String question, ArrayList<String> tags,
                                       ArrayList<String> wrongAnswers, ArrayList<String> correct, int difficulty) {

        ((EditText)questionContainer).setText(question);

        switch (difficulty){
            case 10:
                ((RadioButton)radioEasy).setChecked(true);
                QuestionSuggestionActivity.CONTEXT.setDifficulty(radioEasy);
                break;
            case 20:
                ((RadioButton)radioMedium).setChecked(true);
                QuestionSuggestionActivity.CONTEXT.setDifficulty(radioMedium);
                break;
            case 30:
                ((RadioButton)radioAdvance).setChecked(true);
                QuestionSuggestionActivity.CONTEXT.setDifficulty(radioAdvance);
                break;
        }

        QuestionEditingScreenFragment.TOPIC_TAGS.clear();
        for(int i = 0; i < tags.size(); i++){
            QuestionEditingScreenFragment.TOPIC_TAGS.add(tags.get(i));
        }

        TAGS_ADAPTER =
                new AddAndRemoveItemsAdapter(
                        QuestionEditingScreenFragment.TOPIC_TAGS,
                        QuestionSuggestionActivity.CONTEXT,
                        QuestionSuggestionActivity.INTERACTOR);

        ListView topicTagListView =
                (ListView) tagsContainer.findViewById(R.id.lay_topic_tags_container);

        topicTagListView.setAdapter(TAGS_ADAPTER);

        QuestionSuggestionActivity.INTERACTOR.bindListenersToAddButtons(
                null,null,null,null,null,null,
                buttonAdd,
                QuestionEditingScreenFragment.TOPIC_TAGS,
                TAGS_ADAPTER
        );

    }


    /**
     * @roberto.cervantes1988@gmail.com
     *
     * Function that shows the list of open questions type.
     * It recovers a list of Basic Questions objects using a call to the BackEndCaller FirebaseCaller.
     * It use SUGGESTED_ADAPTER in order to manage the data to be showed in the list.
     *
     * * @itzcoatl90
     *
     * Edited a little bit, all questions regardless of type are stored on BASIC_SUGGESTIONS.
     *
     * */
    @Override
    public void doQuestionListPresentation(QuestionListScreenFragment fragment, View listContainer) {
        QuestionListScreenFragment.BASIC_SUGGESTIONS = new ArrayList<>();
        this.listContainer = listContainer;
        BackendCaller aux = new FirebaseCaller();
        QuestionSuggestionActivity.CONTEXT.startProDialog();
        aux.receiveBasicQuestionSuggestions(-1,
                QuestionSuggestionActivity.CONTEXT,
                Constants.QUESTION_SUGGESTION_INDEX,
                QuestionSuggestionActivity.CONTEXT);
    }

    @Override
    public void deployList(){
        ListView suggestedQuestionsListView = (ListView)listContainer.findViewById(R.id.list_questions);
        SUGGESTED_ADAPTER = new ShowListItemsAdapter(QuestionListScreenFragment.BASIC_SUGGESTIONS);
        suggestedQuestionsListView.setAdapter(SUGGESTED_ADAPTER);
    }

}
