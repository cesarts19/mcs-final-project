package com.example.mcs.interviewhelper.Presenter.QuizPresenters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.example.mcs.interviewhelper.QuizActivity;
import com.example.mcs.interviewhelper.R;
import com.example.mcs.interviewhelper.View.QuizViews.QuizFragment;

import java.util.ArrayList;

public class MultipleChoiceQuestionPresenter implements QuestionPresenter {

    @Override
    public void doAnswerLayout(QuizFragment fragment, View answerContainer,
                               View NextButton, View BackButton) {
        LayoutInflater layoutInflater = (LayoutInflater)
                QuizActivity.CONTEXT.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View answer = layoutInflater.inflate(R.layout.quiz_multiple_answer_layout,null);
        answer.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
        ((LinearLayout) answerContainer).addView(answer);
        QuizActivity.INTERACTOR.setNavigationNextButtonListener(NextButton);
        QuizActivity.INTERACTOR.setNavigationBackButtonListener(BackButton);
    }

    public void drawAnswer(View view, String Value){
        ((Button)view).setText(Value);
    }

    @Override
    public void presentNextQuestion(View view) {
        TextView question = QuizActivity.INTERACTOR.getQuestionView(view);

        ArrayList<String> answers = QuizActivity.INTERACTOR.getAnswerArray(QuizActivity.CURRENTQUESTION);
        ListView answersView = (ListView) view.findViewById(R.id.AnswersMultipleQuestion);

        MultipleAnswerListAdapter adapter =
                new MultipleAnswerListAdapter(answers,QuizActivity.CONTEXT,view);
        answersView.setAdapter(adapter);

        question.setText(QuizActivity.QUIZ2SHOW
                .get(QuizActivity.CURRENTQUESTION).getmQuestion());


    }
}
