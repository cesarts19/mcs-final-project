package com.example.mcs.interviewhelper.View.QuizViews;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.mcs.interviewhelper.QuizActivity;
import com.example.mcs.interviewhelper.R;
/**franco_trujillo
 * this fragment is in charge of show the questions from the quiz
 */
public class QuizFragment extends Fragment {


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_quiz, container, false);
        QuizActivity.QPRESENTER.doAnswerLayout(this,view.findViewById(R.id.AnswerHolder)
        ,view.findViewById(R.id.NextButton),view.findViewById(R.id.PreviousButton));
        QuizActivity.INTERACTOR.loadQuestionnaire(view);
        return view;
    }




}
