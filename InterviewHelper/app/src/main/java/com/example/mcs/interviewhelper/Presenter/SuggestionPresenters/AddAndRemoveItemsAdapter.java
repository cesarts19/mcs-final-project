package com.example.mcs.interviewhelper.Presenter.SuggestionPresenters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ListAdapter;

import com.example.mcs.interviewhelper.Model.Interactors.RolInteractor;
import com.example.mcs.interviewhelper.R;

import java.util.ArrayList;

/**
 * @itzcoatl90@gmail.com
 *
 * Add and Remove adapter that manage the data to be showed in the list.
 * Also make a call to interactor.bindListenerToRemoveButton.
 *
 * */
public class AddAndRemoveItemsAdapter extends BaseAdapter implements ListAdapter {

    public ArrayList<String> list;
    private Context context;
    private RolInteractor interactor;
    public int positionHolder;
    //public int lastRemoved;

    public AddAndRemoveItemsAdapter(ArrayList<String> list, Context context, RolInteractor interactor) {
        this.list = list;
        this.context = context;
        this.interactor = interactor;
        //this.lastRemoved = 100;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.wrapper_item_adding_layout, null);
        }
        //View deleteBtn = convertView.findViewById(R.id.btn_remove_item);
        EditText editText = (EditText) convertView.findViewById(R.id.etxt_item_input);
        editText.setText(list.get(position));
        //interactor.bindListenerToRemoveButton(deleteBtn,editText,position,list,this,/*lastRemoved*/100);
        //interactor.bindListenerToRemoveButton(deleteBtn,position,this);
        interactor.bindListenerToEditText(editText,position,this);
        if(position == positionHolder) {
            editText.requestFocus();
        }
        /*if(position == list.size()-1) {
            lastRemoved = 100;
        }*/
        return convertView;
    }


}
