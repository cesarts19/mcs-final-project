package com.example.mcs.interviewhelper.Model.data;

import java.util.ArrayList;
import java.util.HashMap;

public class MultipleQuestionWrapper {
    public ArrayList<Long> CorrectResponses;
    public ArrayList<String> Tags;
    public ArrayList<Long> WrongResponses;
    public int difficulty;
    public long id;
    public String question;
    public int status;
    public String submmitedBy;
    public String type;
    public String validatedBy;
    public MultipleQuestionWrapper(){}
    public MultipleQuestionWrapper(MultipleChoiceQuestion question){
        CorrectResponses = new ArrayList<Long>();
        int size = question.getmCorrectAnswerIndexes().size();
        for (int i = 0; i < size; i++) {
            CorrectResponses.add(question.getmCorrectAnswerIndexes().get(i));
        }
        Tags = new ArrayList<String>();
        size = question.getmTags().size();
        for (int i = 0; i < size; i++) {
            Tags.add(question.getmTags().get(i));
        }
        WrongResponses = new ArrayList<Long>();
        size = question.getmWrongAnswerIndexes().size();
        for (int i = 0; i < size; i++) {
            WrongResponses.add(question.getmWrongAnswerIndexes().get(i));
        }
        difficulty = question.getmDifficulty();
        id = question.getmId();
        this.question = question.getmQuestion();
        status = question.getmStatus();
        submmitedBy = question.getmCreator();
        type = "training";
        validatedBy = question.getmValidator();
    }

    @Override
    public String toString() {
        return "MultipleQuestionWrapper{" +
                "CorrectResponses=" + CorrectResponses +
                ", Tags=" + Tags +
                ", WrongResponses=" + WrongResponses +
                ", difficulty=" + difficulty +
                ", id=" + id +
                ", question='" + question + '\'' +
                ", status=" + status +
                ", submmitedBy='" + submmitedBy + '\'' +
                ", type='" + type + '\'' +
                ", validatedBy='" + validatedBy + '\'' +
                '}';
    }
}
