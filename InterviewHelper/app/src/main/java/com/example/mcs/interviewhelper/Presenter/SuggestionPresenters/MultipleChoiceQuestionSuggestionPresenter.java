package com.example.mcs.interviewhelper.Presenter.SuggestionPresenters;

import android.content.Context;
import android.os.Parcel;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;

import com.example.mcs.interviewhelper.Model.BackendCallers.BackendCaller;
import com.example.mcs.interviewhelper.Model.BackendCallers.FirebaseCaller;
import com.example.mcs.interviewhelper.Model.data.Constants;
import com.example.mcs.interviewhelper.QuestionSuggestionActivity;
import com.example.mcs.interviewhelper.R;
import com.example.mcs.interviewhelper.View.Suggestions.QuestionEditingScreenFragment;
import com.example.mcs.interviewhelper.View.Suggestions.QuestionListScreenFragment;

import java.util.ArrayList;

/**
 * Presenter implementation for the training questions suggestions
 */

public class MultipleChoiceQuestionSuggestionPresenter extends QuestionSuggestionPresenter {

    AddAndRemoveItemsAdapter TRUE_ANSWERS_ADAPTER;
    AddAndRemoveItemsAdapter WRONG_ANSWERS_ADAPTER;
    AddAndRemoveItemsAdapter TAGS_ADAPTER;
    ShowListItemsAdapter SUGGESTED_ADAPTER;
    View listContainer;

    public MultipleChoiceQuestionSuggestionPresenter() {
        TRUE_ANSWERS_ADAPTER = null;
        WRONG_ANSWERS_ADAPTER = null;
        TAGS_ADAPTER = null;
        SUGGESTED_ADAPTER = null;
    }

    /**
     * @itzcoatl90@gmail.com
     *
     * Function that manage the presentation in the UI of the widgets involved in the Edit Presentation Screen.
     *
     * */
    @Override
    public void doEditPresentation(QuestionEditingScreenFragment fragment,
                                   View answerContainer, View tagsContainer,
                                   View buttonAddTag) {
        LayoutInflater layoutInflater = (LayoutInflater)
                QuestionSuggestionActivity.CONTEXT.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ViewGroup answers_wrapper = (ViewGroup) layoutInflater.inflate(R.layout.wrapper_answers_container,null);
        ((LinearLayout) answerContainer).addView(answers_wrapper,0);
        (QuestionEditingScreenFragment.TRUE_ANSWERS = new ArrayList<>()).add("");
        (QuestionEditingScreenFragment.WRONG_ANSWERS = new ArrayList<>()).add("");
        QuestionEditingScreenFragment.TOPIC_TAGS.add("");
        TRUE_ANSWERS_ADAPTER =
                new AddAndRemoveItemsAdapter(
                        QuestionEditingScreenFragment.TRUE_ANSWERS,
                        QuestionSuggestionActivity.CONTEXT,
                        QuestionSuggestionActivity.INTERACTOR);
        WRONG_ANSWERS_ADAPTER =
                new AddAndRemoveItemsAdapter(
                        QuestionEditingScreenFragment.WRONG_ANSWERS,
                        QuestionSuggestionActivity.CONTEXT,
                        QuestionSuggestionActivity.INTERACTOR);
        TAGS_ADAPTER =
                new AddAndRemoveItemsAdapter(
                        QuestionEditingScreenFragment.TOPIC_TAGS,
                        QuestionSuggestionActivity.CONTEXT,
                        QuestionSuggestionActivity.INTERACTOR);
        ListView correctAnswerListView =
                (ListView) answerContainer.findViewById(R.id.lay_true_answers_container);
        ListView wrongAnswerListView =
                (ListView) answerContainer.findViewById(R.id.lay_false_answers_container);
        ListView topicTagListView =
                (ListView) tagsContainer.findViewById(R.id.lay_topic_tags_container);
        correctAnswerListView.setAdapter(TRUE_ANSWERS_ADAPTER);
        wrongAnswerListView.setAdapter(WRONG_ANSWERS_ADAPTER);
        topicTagListView.setAdapter(TAGS_ADAPTER);
        View buttonAddCorrectAnswer = answers_wrapper.findViewById(R.id.btn_add_true_answer_to_suggestion);
        View buttonAddIncorrectAnswer = answers_wrapper.findViewById(R.id.btn_add_false_answer_to_suggestion);
        QuestionSuggestionActivity.INTERACTOR.bindListenersToAddButtons(
                buttonAddCorrectAnswer,
                QuestionEditingScreenFragment.TRUE_ANSWERS,
                TRUE_ANSWERS_ADAPTER,
                buttonAddIncorrectAnswer,
                QuestionEditingScreenFragment.WRONG_ANSWERS,
                WRONG_ANSWERS_ADAPTER,
                buttonAddTag,
                QuestionEditingScreenFragment.TOPIC_TAGS,
                TAGS_ADAPTER
        );
    }


    /**
     * @roberto.cervantes1988@gmail.com
     *
     * Function that perform the presentation of the selected suggested question from the list of suggestions.
     * It receives the views involved and the data to be showed within every view.
     * For Multiple Choice Questions type it is showing:
     * Question Suggested, Radio Button checked according to the difficulty suggested,
     * List of correct answers suggested for the question,
     * List of wrong answers suggested for the question,
     * List of tags suggested for the question.
     *
     * */
    @Override
    public void doSuggestPresentation(QuestionEditingScreenFragment fragment,
                                      View questionContainer, View answerContainer,
                                      View tagsContainer, View buttonAdd,
                                      View radioEasy, View radioMedium, View radioAdvance,
                                      String value,  ArrayList<String> tags,
                                      ArrayList<String> wrongAnwers, ArrayList<String> correct, int difficulty) {

        LayoutInflater layoutInflater = (LayoutInflater)
                QuestionSuggestionActivity.CONTEXT.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        ViewGroup answers_wrapper = (ViewGroup) layoutInflater.inflate(R.layout.wrapper_answers_container,null);

        ((LinearLayout) answerContainer).addView(answers_wrapper,0);

        ((EditText)questionContainer).setText(value);

        switch (difficulty){
            case 10:
                ((RadioButton)radioEasy).setChecked(true);
                QuestionSuggestionActivity.CONTEXT.setDifficulty(radioEasy);
                break;
            case 20:
                ((RadioButton)radioMedium).setChecked(true);
                QuestionSuggestionActivity.CONTEXT.setDifficulty(radioMedium);
                break;
            case 30:
                ((RadioButton)radioAdvance).setChecked(true);
                QuestionSuggestionActivity.CONTEXT.setDifficulty(radioAdvance);
                break;
        }

        QuestionEditingScreenFragment.TRUE_ANSWERS = new ArrayList<>();
        for (int i = 0; i < correct.size(); i++) {
            QuestionEditingScreenFragment.TRUE_ANSWERS.add(correct.get(i));
        }

        QuestionEditingScreenFragment.WRONG_ANSWERS = new ArrayList<>();
        for(int i = 0; i < wrongAnwers.size(); i++){
            QuestionEditingScreenFragment.WRONG_ANSWERS.add(wrongAnwers.get(i));
        }

        for(int i = 0; i < tags.size(); i++){
            QuestionEditingScreenFragment.TOPIC_TAGS.add(tags.get(i));
        }

        TRUE_ANSWERS_ADAPTER =
                new AddAndRemoveItemsAdapter(
                        QuestionEditingScreenFragment.TRUE_ANSWERS,
                        QuestionSuggestionActivity.CONTEXT,
                        QuestionSuggestionActivity.INTERACTOR);
        WRONG_ANSWERS_ADAPTER =
                new AddAndRemoveItemsAdapter(
                        QuestionEditingScreenFragment.WRONG_ANSWERS,
                        QuestionSuggestionActivity.CONTEXT,
                        QuestionSuggestionActivity.INTERACTOR);
        TAGS_ADAPTER =
                new AddAndRemoveItemsAdapter(
                        QuestionEditingScreenFragment.TOPIC_TAGS,
                        QuestionSuggestionActivity.CONTEXT,
                        QuestionSuggestionActivity.INTERACTOR);

        ListView correctAnswerListView =
                (ListView) answerContainer.findViewById(R.id.lay_true_answers_container);
        ListView wrongAnswerListView =
                (ListView) answerContainer.findViewById(R.id.lay_false_answers_container);
        ListView topicTagListView =
                (ListView) tagsContainer.findViewById(R.id.lay_topic_tags_container);

        correctAnswerListView.setAdapter(TRUE_ANSWERS_ADAPTER);
        wrongAnswerListView.setAdapter(WRONG_ANSWERS_ADAPTER);
        topicTagListView.setAdapter(TAGS_ADAPTER);

        View buttonAddCorrectAnswer = answers_wrapper.findViewById(R.id.btn_add_true_answer_to_suggestion);
        View buttonAddIncorrectAnswer = answers_wrapper.findViewById(R.id.btn_add_false_answer_to_suggestion);

        QuestionSuggestionActivity.INTERACTOR.bindListenersToAddButtons(
                buttonAddCorrectAnswer,
                QuestionEditingScreenFragment.TRUE_ANSWERS,
                TRUE_ANSWERS_ADAPTER,
                buttonAddIncorrectAnswer,
                QuestionEditingScreenFragment.WRONG_ANSWERS,
                WRONG_ANSWERS_ADAPTER,
                buttonAdd,
                QuestionEditingScreenFragment.TOPIC_TAGS,
                TAGS_ADAPTER
        );


    }

    /**
     * @roberto.cervantes1988@gmail.com
     *
     * Function that shows the list of multiple questions type.
     * It recovers a list of Multiple Questions objects using a call to the BackEndCaller FirebaseCaller.
     * It use SUGGESTED_ADAPTER in order to manage the data to be showed in the list.
     *
     * @itzcoatl90
     *
     * Edited a little bit, all questions regardless of type are stored on BASIC_SUGGESTIONS.
     *
     * */
    @Override
    public void doQuestionListPresentation(QuestionListScreenFragment fragment, View listContainer) {
        QuestionListScreenFragment.BASIC_SUGGESTIONS = new ArrayList<>();
        this.listContainer = listContainer;
        BackendCaller aux = new FirebaseCaller();
        QuestionSuggestionActivity.CONTEXT.startProDialog();
        aux.receiveMultipleChoiceQuestionSuggestions(-1,
                QuestionSuggestionActivity.CONTEXT,
                Constants.QUESTION_SUGGESTION_INDEX,
                QuestionSuggestionActivity.CONTEXT);
    }

    @Override
    public void deployList(){
        ListView suggestedQuestionsListView = (ListView)listContainer.findViewById(R.id.list_questions);
        SUGGESTED_ADAPTER = new ShowListItemsAdapter(QuestionListScreenFragment.BASIC_SUGGESTIONS);
        suggestedQuestionsListView.setAdapter(SUGGESTED_ADAPTER);
    }

}
