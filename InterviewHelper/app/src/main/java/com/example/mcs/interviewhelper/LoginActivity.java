package com.example.mcs.interviewhelper;

import android.support.v7.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;

import com.example.mcs.interviewhelper.Model.GenericInteractor;

import java.util.ArrayList;

public class LoginActivity extends AppCompatActivity {

    public AutoCompleteTextView mEmailView;
    public EditText mPasswordView;
    public ArrayList<String> DUMMY_CREDENTIALS_CONSULTANTS;
    public ArrayList<String> DUMMY_CREDENTIALS_TRAINER;
    private GenericInteractor INTERACTOR;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        inits();
        initUIElements();

    }

    private void inits(){
        INTERACTOR = new GenericInteractor();
        DUMMY_CREDENTIALS_CONSULTANTS = INTERACTOR.getConsultors();
        DUMMY_CREDENTIALS_TRAINER = INTERACTOR.getTrainers();
    }

    private void initUIElements(){
        mEmailView = (AutoCompleteTextView) findViewById(R.id.email);
        mPasswordView = (EditText) findViewById(R.id.password);
        Button mEmailSignInButton = (Button) findViewById(R.id.btn_sign_in_button);
        INTERACTOR.bindListenersToLogin(this,mPasswordView,mEmailSignInButton);
    }

    public void simulateConsultant(View view){
        INTERACTOR.simulateConsultant(this);
    }

    public void simulateTrainer(View view){
        INTERACTOR.simulateTrainer(this);
    }

}

