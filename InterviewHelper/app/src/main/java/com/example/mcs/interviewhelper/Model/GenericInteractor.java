package com.example.mcs.interviewhelper.Model;

import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mcs.interviewhelper.LoginActivity;
import com.example.mcs.interviewhelper.MainActivity;
import com.example.mcs.interviewhelper.Model.data.User;
import com.example.mcs.interviewhelper.Presenter.GenericPresenter;
import com.example.mcs.interviewhelper.R;

import java.util.ArrayList;
import java.util.Collections;

public class GenericInteractor {
    private GenericPresenter PRESENTER;
    public GenericInteractor(){
        PRESENTER = new GenericPresenter();
    }


    /**
     * This id DUMMY credentials for Consultors.
     * */
    public ArrayList<String> getConsultors(){
        ArrayList<String> consultors;
        Collections.addAll( (consultors = new ArrayList<>())
                , "itzcoatl90@gmail.com"
                , "roberto.cervantes1988@gmail.com"
                , "carloscastes@gmail.com"
                , "franco.e.trujillo@gmail.com"
                , "cesarts190@gmail.com"
                , "erick.garcia.mcs@gamil.com");
        return consultors;
    }

    /**
     * This is DUMMY credentials for Trainers.
     * */
    public ArrayList<String> getTrainers(){
        ArrayList<String> trainers;
        Collections.addAll( (trainers = new ArrayList<>())
                , "erick.garcia@mobileconsultingsolutions.com"
                , "mariana.guinea@mobileconsultingsolutions.com");
        return trainers;
    }

    /**
     * @itzcoatl90
     * This method is meant to bind the listeners for the login (button and password enter).
     *
     * */
    public void bindListenersToLogin(final LoginActivity activity,EditText mPasswordView,Button mEmailSignInButton){
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin(activity);
                    return true;
                }
                return false;
            }
        });
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin(activity);
            }
        });
    }

    // This is for simulating Consultant until having the login functionality.
    public void simulateConsultant(LoginActivity activity){
        NetworkInfo netInfo = ((ConnectivityManager) activity.getSystemService(activity.CONNECTIVITY_SERVICE))
                .getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            Intent intent = new Intent(activity, MainActivity.class);
            User user = new User("itzcoatl90@gmail.com",1);
            intent.putExtra(User.USER_PARCELABLE_NAME,user);
            activity.startActivity(intent);
        }else{
            //Intent for the app first screen
            Toast.makeText(activity, "Login Failed... Please check your internet connection!!!", Toast.LENGTH_LONG).show();
            Intent intent = new Intent(activity, LoginActivity.class);
            activity.startActivity(intent);
        }

    }

    // This is for simulating Trainer until having the login functionality.
    public void simulateTrainer(LoginActivity activity){
        NetworkInfo netInfo = ((ConnectivityManager) activity.getSystemService(activity.CONNECTIVITY_SERVICE))
                .getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            Intent intent = new Intent(activity, MainActivity.class);
            User user = new User("robertosatanas@gmail.com",2);
            intent.putExtra(User.USER_PARCELABLE_NAME,user);
            activity.startActivity(intent);
        } else {
            //Intent for the app first screen
            Toast.makeText(activity, "Login Failed... Please check your internet connection!!!", Toast.LENGTH_LONG).show();
            Intent intent = new Intent(activity, LoginActivity.class);
            activity.startActivity(intent);
        }

    }

    // Email validation.
    private boolean isEmailValid(String email) {
        return email.indexOf("@") < email.lastIndexOf(".") && email.indexOf("@") > 0 && email.length() > 6;
    }

    // Password validation.
    private boolean isPasswordValid(String password) {
        return password.length() > 4;
    }

    // This is the attempting to login... binded on the siging button and in the password enter.
    public void attemptLogin(LoginActivity activity) {

        PRESENTER.clean(activity);

        String email = activity.mEmailView.getText().toString();
        String password = activity.mPasswordView.getText().toString();

        boolean cancel = false;

        if (TextUtils.isEmpty(password) || !isPasswordValid(password)) {
            PRESENTER.errorOnPassword(activity);
            cancel = true;
        }

        if (TextUtils.isEmpty(email)) {
            PRESENTER.errorOnEMail(activity,activity.getString(R.string.error_field_required));
            cancel = true;
        } else if (!isEmailValid(email)) {
            PRESENTER.errorOnEMail(activity,activity.getString(R.string.error_invalid_email));
            cancel = true;
        }

        if (cancel) {
            PRESENTER.focusMessage();
        } else {
            if(activity.DUMMY_CREDENTIALS_CONSULTANTS.contains(email)){
                Intent intent = new Intent(activity, MainActivity.class);
                User user = new User(email,1);
                intent.putExtra(User.USER_PARCELABLE_NAME,user);
                activity.startActivity(intent);
            } else if (activity.DUMMY_CREDENTIALS_TRAINER.contains(email)) {
                Intent intent = new Intent(activity, MainActivity.class);
                User user = new User(email,2);
                intent.putExtra(User.USER_PARCELABLE_NAME,user);
                activity.startActivity(intent);
            } else {
                Toast.makeText(activity, activity.getString(R.string.resend_credentials_login),
                        Toast.LENGTH_SHORT).show();
            }
        }
    }

}
