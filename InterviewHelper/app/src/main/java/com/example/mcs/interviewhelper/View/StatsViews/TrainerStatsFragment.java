package com.example.mcs.interviewhelper.View.StatsViews;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.mcs.interviewhelper.MainActivity;
import com.example.mcs.interviewhelper.R;

/**franco_trujillo
 * this fragment is in charge of show the questions from the quiz
 */
public class TrainerStatsFragment extends android.app.Fragment {
    int percentage;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        percentage = bundle.getInt(MainActivity.INTERACTOR.PERCENTAGE_OVERALL);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_trainer_stats, container, false);
        MainActivity.INTERACTOR.initiateOverallFragment(view, percentage);
        return view;
    }




}
