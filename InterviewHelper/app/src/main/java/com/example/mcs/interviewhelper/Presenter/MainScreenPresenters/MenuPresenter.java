package com.example.mcs.interviewhelper.Presenter.MainScreenPresenters;

import android.app.Fragment;
import android.content.Context;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.mcs.interviewhelper.MainActivity;
import com.example.mcs.interviewhelper.Model.Interactors.RolInteractor;
import com.example.mcs.interviewhelper.Model.data.Stat;
import com.example.mcs.interviewhelper.Presenter.SuggestionPresenters.StatsSectionAdapter;

import java.util.ArrayList;

/**
 * Base interface for menu presenters
 */

public abstract class MenuPresenter {


    /**
     * @itzcoatl90
     * This method is for presenting the Main Screen on the MainActivity.
     * The main screen includes the drawer menu and the Toolbar.
     *
     * NOTE: On the next sprint, it should include the Content (statistics on the consultant,
     * generics on the trainer). As this is going to depend on the Rol, that presentation should
     * be done throught the Rol interactor from the Parameters.
     *
     * */
    public void PresentMainView(MainActivity activity, RolInteractor interactor){

        // This is the Toolbar and the Toogle button.
        activity.setSupportActionBar(activity.getToolbar());
        ActionBarDrawerToggle toggle = interactor.getDrawer(activity,activity.getDrawerLayout(),activity.getToolbar());
        activity.getDrawerLayout().addDrawerListener(toggle);
        toggle.syncState();

        // This is the drawer menu.
        LayoutInflater inflater = (LayoutInflater) activity.getApplicationContext().getSystemService
                (Context.LAYOUT_INFLATER_SERVICE);
        activity.getNavigationView().inflateMenu(
                interactor.getIdDrawer()
        );
        //This is where we bind the listeners.
        activity.getNavigationView().setNavigationItemSelectedListener(activity);


        postMainPresenting();

    }

    public abstract void postMainPresenting();

    public abstract void PresentOverallStatisticsFragment(android.app.FragmentManager Manager, Fragment fragment, int container);
    public abstract void AddFragmentsToViewPager(ArrayList<android.support.v4.app.Fragment> fragments, StatsSectionAdapter adapter, ViewPager pager);
    public abstract void SetTopicStatsListAdapter(View view, ArrayList<Stat> list);
    public abstract void SetConsultantStatsListAdapter(View view, ArrayList<Stat> list);
    public abstract void SetOverallProgressBar(View view, int percentage);


}
