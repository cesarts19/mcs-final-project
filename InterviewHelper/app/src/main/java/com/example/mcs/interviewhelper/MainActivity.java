package com.example.mcs.interviewhelper;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.example.mcs.interviewhelper.Model.Interactors.ConsultantInteractor;
import com.example.mcs.interviewhelper.Model.Interactors.RolInteractor;
import com.example.mcs.interviewhelper.Model.Interactors.TrainerInteractor;
import com.example.mcs.interviewhelper.Model.data.Quiz;
import com.example.mcs.interviewhelper.Model.data.Stat;
import com.example.mcs.interviewhelper.Model.data.User;
import com.example.mcs.interviewhelper.Presenter.MainScreenPresenters.ConsultorPresenter;
import com.example.mcs.interviewhelper.Presenter.MainScreenPresenters.MenuPresenter;
import com.example.mcs.interviewhelper.Presenter.MainScreenPresenters.TrainerPresenter;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    public static MainActivity CONTEXT = null;
    public static RolInteractor INTERACTOR = null;
    public static MenuPresenter PRESENTER = null;
    public static ArrayList<Quiz> QUIZES = new ArrayList<Quiz>();
    public static ArrayList<String> dataBaseTags = new ArrayList<>();
    public static User mUser = null;
    public static ArrayList<Stat> TAGS_STATS = null;
    public static ArrayList<Stat> CONSULTANTS_STATS = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        inits();
        INTERACTOR.bindListenersToMainScreen(this);

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        INTERACTOR.getQuestionsFromDB();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mUser = null;
        INTERACTOR = null;
    }

    private void inits() {
        mUser = getIntent().getExtras().getParcelable(User.USER_PARCELABLE_NAME);
        if(mUser.getUserRol() == 1){
            PRESENTER = new ConsultorPresenter();
            INTERACTOR = new ConsultantInteractor(PRESENTER);
        } else if (mUser.getUserRol() == 2) {
            PRESENTER = new TrainerPresenter();
            INTERACTOR = new TrainerInteractor(PRESENTER);
        } else {
            throw new UnsupportedOperationException("Role not yet implemented");
        }
        CONTEXT = this;
    }

    @Override
    public void onBackPressed() {
        INTERACTOR.bindListenerToBackButton(this);
    }

    public void superOnBackPressed(){
        super.onBackPressed();
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        return INTERACTOR.onNavigationItemSelected(this,item);
    }

    public Toolbar getToolbar(){
        return (Toolbar) findViewById(R.id.toolbar);
    }


    public DrawerLayout getDrawerLayout(){
        return (DrawerLayout) findViewById(R.id.drawer_layout);
    }

    public NavigationView getNavigationView(){
        return (NavigationView) findViewById(R.id.nav_view);
    }

}
