package com.example.mcs.interviewhelper.Model.data.ContentProvider;


import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class QuizTable {

    public static final String TABLE_NAME = "QUIZTABLE";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_USER = "user";
    public static final String COLUMN_TYPE = "type";
    public static final String COLUMN_QUIZ_INDEX = "quindex";
    public static final String COLUMN_QUESTION_INDEX = "qindex";
    public static final String COLUMN_TAG = "tag";
    public static final String COLUMN_QUESTION = "question";
    public static final String COLUMN_RESPONSES = "response";
    public static final String COLUMN_QUIZ_STATUS = "status";
    public static final String COLUMN_DIFFICULTY = "difficulty";

    public static final String CREATE_QUIZ_TABLE =
            "CREATE TABLE IF NOT EXISTS " + TABLE_NAME
            + "(" + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + COLUMN_USER + " TEXT NOT NULL, "
            + COLUMN_TYPE + " TEXT NOT NULL, "
            + COLUMN_QUIZ_INDEX + " INTEGER DEFAULT 0, "
            + COLUMN_QUESTION_INDEX + " INTEGER DEFAULT 0, "
            + COLUMN_TAG + " TEXT NOT NULL, "
            + COLUMN_QUESTION + " TEXT NOT NULL, "
            + COLUMN_RESPONSES + " TEXT, "
            + COLUMN_QUIZ_STATUS + " INTEGER DEFAULT 0, "
            + COLUMN_DIFFICULTY + " INTEGER DEFAULT 0"
            + ");";

    public static void onCreate(SQLiteDatabase database){
        database.execSQL(CREATE_QUIZ_TABLE);
    }
    public static void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion){
        Log.d(QuizTable.class.getSimpleName(), "Upgrading database from " + oldVersion + " to " + newVersion + ", destroying local data.");
        database.execSQL("DROP TABLE IF EXIST " + TABLE_NAME);
        onCreate(database);
    }
}
