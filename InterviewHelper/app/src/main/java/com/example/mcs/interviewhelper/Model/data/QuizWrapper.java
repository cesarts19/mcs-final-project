package com.example.mcs.interviewhelper.Model.data;

import java.util.ArrayList;

public class QuizWrapper {
    public String Timestamp;
    public String RespondedBy;
    public ArrayList<QuestionInsiderWrapper> Questions;

    public QuizWrapper(){}
    public QuizWrapper(Quiz quiz){
        this.Timestamp = quiz.getmSubmissionDate().toString();
        this.RespondedBy = toAscRepresentation(quiz.getmAnsweredBy());
        this.Questions = new ArrayList<>();
        for (int i = 0; i < quiz.getmQuestion().size(); i++) {
            this.Questions.add(new QuestionInsiderWrapper(quiz.getmQuestion().get(i)));
        }
    }

    public String toAscRepresentation(String input){
        String output = "";
        for (int i = 0; i < input.length(); i++) {
            output += ("a" + (int)input.charAt(i));
        }
        return output;
    }
}
