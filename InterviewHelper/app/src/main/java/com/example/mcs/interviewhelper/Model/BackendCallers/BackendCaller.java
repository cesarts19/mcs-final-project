package com.example.mcs.interviewhelper.Model.BackendCallers;

import android.content.Context;

import com.example.mcs.interviewhelper.Model.data.BasicQuestion;
import com.example.mcs.interviewhelper.Model.data.Command;
import com.example.mcs.interviewhelper.Model.data.MultipleChoiceQuestion;
import com.example.mcs.interviewhelper.Model.data.Quiz;

import java.util.ArrayList;

/**
 * Interface to be implemented by the data source callers
 *
 * @Carloscastes
 */

public interface BackendCaller {
    /*
     * This method is deprecated. Please use requestBasicQuestions method instead* /
    @Deprecated
    ArrayList<BasicQuestion> getBasicQuestions(int questionQuantity, int difficultyLevel, ArrayList<String> topicTags);

    /*This method is deprecated. Please use requestMultipleChoiceQuestions instead* /
    @Deprecated
    ArrayList<MultipleChoiceQuestion> getMultipleChoiceQuestions
    (int questionQuantity, int difficultyLevel, ArrayList<String> topicTags, int responseQuantity);*/


    /* // Analize this work...
    interface BackendCallerListener {
        void onBasicQuestionResponse(ArrayList<BasicQuestion> basicQuestionsResult);

        void onMultipleChoiceQuestionResponse(ArrayList<MultipleChoiceQuestion> multipleChoiceQuestionsResult);
    }
    */

    /**
     * @itzcoatl90
     *
     * This methods sendInterviewQuiz and sendTrainingQuiz, are for storing on db the quiz on db.
     * requestBasicQuestions and requestMultipleChoiceQuestion are for retrieving a set
     * of random questions with the specifications from the Backend.
     *
     * */

    void sendInterviewQuiz(Quiz quiz,Context mContext,Command mCommand);

    void sendTrainingQuiz(Quiz quiz,Context mContext,Command mCommand);

    void receiveBasicQuestions(int questionQuantity,
                               int difficultyLevel,
                               ArrayList<String> topicTags,
                               Context mContext,
                               int activity_index,
                               Command mCommand);

    void receiveMultipleChoiceQuestions(int questionQuantity,
                                        int difficultyLevel,
                                        ArrayList<String> topicTags,
                                        int maxQuantityAnswers,
                                        Context mContext,
                                        int activity_index,
                                        Command mCommand);

    /**
     * @itzcoatl90
     *
     * This methods sendQuestionSuggestion(BasicQuestion) and sendQuestionSuggestion(MultipleChoiceQuestion)
     * are for storing questionSuggestion on QuestionSuggestionActivity, receiveBasicQuestionSuggestions and
     * receiveMultipleChoiceQuestionSuggestions are for listing suggestion previously stored, usually the trainer.
     *
     * */

    boolean sendQuestionSuggestion(BasicQuestion question,Context mContext,Command mCommand);

    boolean sendQuestionSuggestion(MultipleChoiceQuestion question,Context mContext,Command mCommand);

    void receiveBasicQuestionSuggestions(int maxQuantityAnswers,
                                         Context mContext,
                                         int activity_index,
                                         Command mCommand);

    void receiveMultipleChoiceQuestionSuggestions(int maxQuantityAnswers,
                                                  Context mContext,
                                                  int activity_index,
                                                  Command mCommand);

    void receiveConsultantsInfo(Context context, Command mComand);
    void requestTags(final ArrayList<String> tagsArray, Command command);
}
