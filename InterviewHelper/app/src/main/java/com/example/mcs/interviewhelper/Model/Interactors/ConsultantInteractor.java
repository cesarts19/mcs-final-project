package com.example.mcs.interviewhelper.Model.Interactors;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.mcs.interviewhelper.MainActivity;
import com.example.mcs.interviewhelper.Model.data.Stat;
import com.example.mcs.interviewhelper.Model.data.User;
import com.example.mcs.interviewhelper.Presenter.MainScreenPresenters.MenuPresenter;
import com.example.mcs.interviewhelper.Presenter.MainScreenPresenters.TopicStatsListAdapter;
import com.example.mcs.interviewhelper.Presenter.QuizPresenters.MultipleChoiceQuestionPresenter;
import com.example.mcs.interviewhelper.Presenter.QuizPresenters.OpenQuestionPresenter;
import com.example.mcs.interviewhelper.Presenter.QuizPresenters.QuizPresenter;
import com.example.mcs.interviewhelper.Presenter.SuggestionPresenters.StatsSectionAdapter;
import com.example.mcs.interviewhelper.QuestionSuggestionActivity;
import com.example.mcs.interviewhelper.QuizActivity;
import com.example.mcs.interviewhelper.R;
import com.example.mcs.interviewhelper.View.StatsViews.ConsultantStatsByTopicFragment;
import com.example.mcs.interviewhelper.View.StatsViews.ConsultantStatsFragment;
import com.example.mcs.interviewhelper.View.Suggestions.QuestionEditingScreenFragment;
import com.example.mcs.interviewhelper.View.Suggestions.QuestionTypeSuggestionSelectionScreenFragment;

import java.util.ArrayList;


public class ConsultantInteractor extends RolInteractor {

    public ConsultantInteractor(MenuPresenter PRESENTER) {
        super(PRESENTER);
    }

    public ConsultantInteractor(){}

    /**
     * @itzcoatl90@gmail.com
     *
     * Function that according the instance received is initializing a
     * fragment sending this fragment to the startNextFragment method in the activity.
     *
     * */
    @Override
    public void goToNextScreen(QuestionSuggestionActivity activity) {
        if(activity.NEXT_FRAGMENT instanceof QuestionTypeSuggestionSelectionScreenFragment){
            QuestionEditingScreenFragment nextFragment = new QuestionEditingScreenFragment();
            activity.GENERIC_PRESENTER.prepareToolbarOnEditingScreen(activity,getIdToolbar());
            activity.PREVIOUS_FRAGMENT = activity.NEXT_FRAGMENT;
            activity.startNextFragment(nextFragment);
            return;
        }
        throw new UnsupportedOperationException("CAN'T GO TO NEXT SCREEN");
    }


    /**
     * @itzcoatl90@gmail.com
     *
     * Function that returns the id of the consultant toolbar
     *
     * */
    @Override
    public int getIdToolbar() {
        return R.id.toolbar_consultant;
    }

    /**
     * @itzcoatl90@gmail.com
     *
     * Function that returns the id of the Activity Splash Drawer
     *
     * */
    @Override
    public int getIdDrawer() {
        return R.menu.activity_splash_drawer;
    }



    /**
     * @itzcoatl90@gmail.com
     *
     * Temporal Function used to Firebase implementations
     * */
    @Override
    public boolean onNavigationItemSelected(MainActivity activity, MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.nav_training) {

            if(QuizActivity.INTERACTOR == null){
                QuizActivity.INTERACTOR = new TrainingInteractor();
                QuizActivity.PRESENTER = new QuizPresenter();
                QuizActivity.QPRESENTER = new MultipleChoiceQuestionPresenter();
            }
            Intent intent = new Intent(activity, QuizActivity.class);
            intent.putExtra(User.USER_PARCELABLE_NAME,activity.mUser);
            activity.startActivity(intent);

        } else if (id == R.id.nav_interview) {

            if(QuizActivity.INTERACTOR == null){
                QuizActivity.INTERACTOR = new InterviewInteractor();
                QuizActivity.PRESENTER = new QuizPresenter();
                QuizActivity.QPRESENTER = new OpenQuestionPresenter();
            }
            Intent intent = new Intent(activity, QuizActivity.class);
            intent.putExtra(User.USER_PARCELABLE_NAME,activity.mUser);
            activity.startActivity(intent);

        } else if (id == R.id.nav_suggest_question) {

            Intent intent = new Intent(activity, QuestionSuggestionActivity.class);
            intent.putExtra(User.USER_PARCELABLE_NAME,activity.mUser);
            activity.startActivity(intent);

        }

        DrawerLayout drawer = (DrawerLayout) activity.findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    /**
     * Call the presenter that loads fragment used for the general stat overview.
     * Initialize the ViewPager.
     * */
    @Override
    public void loadStatsFragment(Activity activity) {
        Bundle bundle = new Bundle();
        android.app.FragmentManager Manager = activity.getFragmentManager();
        ConsultantStatsFragment consultantStatsFragment = new ConsultantStatsFragment();
        getStats(3);
        bundle.putInt(MainActivity.INTERACTOR.PERCENTAGE_OVERALL, RolInteractor.questionCorrectGeneral);
        consultantStatsFragment.setArguments(bundle);
        MainActivity.PRESENTER.PresentOverallStatisticsFragment(Manager, consultantStatsFragment, R.id.Statistics);
        ViewPager viewPager = (ViewPager)activity.findViewById(R.id.StatisticsBy);
        setupStatsFragments(viewPager,((AppCompatActivity) activity));

    }

    /**
     * Sets the ViewPager and assign the adapter to it.
     * */
    private void setupStatsFragments(ViewPager viewPager, AppCompatActivity activity){
        //TODO add bundles and send objects information to the fragments
        StatsSectionAdapter adapter = new StatsSectionAdapter(activity.getSupportFragmentManager());
        ConsultantStatsByTopicFragment statsByTopic = new ConsultantStatsByTopicFragment();
        adapter.addFragment(statsByTopic);
        viewPager.setAdapter(adapter);
    }

    /**
     * Call the presenter that loads the fragment for Stats by topic
     * */
    @Override
    public void initiateStatsByTopicFragment(View view) {
        //TODO implement if needed
        ArrayList<Stat> listTopic = getStats(3);
        MainActivity.PRESENTER.SetTopicStatsListAdapter(view, listTopic);
    }

    /**
     * No needed for the consultant
     * */
    @Override
    public void initiateStatsByConsultantFragment(View view) {
        //TODO implement if needed
        throw new UnsupportedOperationException("NOT SUPPORTED YET");
    }

    /**
     * Call the presenter that shows the circular progress bar
     * */
    @Override
    public void initiateOverallFragment(View view, int percentage) {
        MainActivity.PRESENTER.SetOverallProgressBar(view, percentage);
    }

    /**
     * After a call to Firebase call the Stats Fragment initialization
     * */
    @Override
    public void getQuestionsFromDB() {
        loadStatsFragment(MainActivity.CONTEXT);

    }

    /**
     * No needed for the consultant
     * */
    @Override
    public void executeOnSendSuccessful() {
        throw new UnsupportedOperationException("NOT YET IMPLEMENTED");
    }

    /**
     * No needed for the consultant
     * */
    @Override
    public void executeOnSendFail() {
        throw new UnsupportedOperationException("NOT YET IMPLEMENTED");
    }

    /**
     * No needed for the consultant
     * */
    @Override
    public void executeOnReceiveSuccessful() {
        throw new UnsupportedOperationException("NOT YET IMPLEMENTED");
    }

    /**
     * No needed for the consultant
     * */
    @Override
    public void executeOnReceiveFail() {
        throw new UnsupportedOperationException("NOT YET IMPLEMENTED");
    }

}
