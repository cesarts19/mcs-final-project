package com.example.mcs.interviewhelper.Model.data;

import java.util.ArrayList;

public class QuestionInsiderWrapper {
    public ArrayList<String> Tags;
    public String Question;
    public String Answer;
    public int Status;
    public int Difficulty;

    public QuestionInsiderWrapper(){}
    public QuestionInsiderWrapper(BasicQuestion question){
        this.Tags = question.getmTags();
        this.Question = question.getmQuestion();
        this.Answer = question.getmResponse();
        this.Status = question.getmCheckedStatus();
        this.Difficulty = question.getmDifficulty();
    }
}
