package com.example.mcs.interviewhelper;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.example.mcs.interviewhelper.Model.BackendCallers.BackendCaller;
import com.example.mcs.interviewhelper.Model.BackendCallers.FirebaseCaller;
import com.example.mcs.interviewhelper.Model.data.Command;

import java.util.ArrayList;
import java.util.HashSet;

public class SplashActivity extends AppCompatActivity implements Command {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
//        (new HolderTask(this)).execute();

        NetworkInfo netInfo = ((ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE))
                .getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            BackendCaller dbCaller = new FirebaseCaller();
            if (MainActivity.dataBaseTags == null) {
                MainActivity.dataBaseTags = new ArrayList<>();
            }
            dbCaller.requestTags(MainActivity.dataBaseTags, this);
        } else {
            //        Intent for the app first screen
            Intent intent = new Intent(this, LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
            startActivity(intent);
            finish();
        }
    }

    @Override
    public void executeOnSendSuccessful() {

    }

    @Override
    public void executeOnSendFail() {

    }

    @Override
    public void executeOnReceiveSuccessful() {
        if (MainActivity.dataBaseTags.size() > 0) {
            SharedPreferences preferences = getPreferences(MODE_PRIVATE);
            String tagsKey = getString(R.string.preference_key_tags);
            preferences.getStringSet(tagsKey, new HashSet<>(MainActivity.dataBaseTags));
        }

//        Intent for the app first screen
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
        startActivity(intent);
        finish();
    }

    @Override
    public void executeOnReceiveFail() {
        // Intent for the app first screen
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
        startActivity(intent);
        finish();
    }
}
