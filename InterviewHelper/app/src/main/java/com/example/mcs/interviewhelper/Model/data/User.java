package com.example.mcs.interviewhelper.Model.data;

import android.os.Parcel;
import android.os.Parcelable;

public class User  implements Parcelable {
    /*
    * userId it's meant for the user email.
    * userType: 1 for Consultant, 2 for Trainer.
    * */
    public static final String USER_PARCELABLE_NAME = "com.example.mcs.interviewhelper.Model.data.User";
    String userId;
    int userRol;

    public User(String userId,int userRol){
        this.userId = userId;
        this.userRol = userRol;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getUserRol() {
        return userRol;
    }

    public void setUserRol(int userRol) {
        this.userRol = userRol;
    }

    @Override
    public String toString() {
        return "User{" +
                "userId='" + userId + '\'' +
                ", userRol=" + userRol +
                '}';
    }

    public User(Parcel in) {
        userId = in.readString();
        userRol = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(userId);
        dest.writeInt(userRol);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<User> CREATOR = new Parcelable.Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

}
