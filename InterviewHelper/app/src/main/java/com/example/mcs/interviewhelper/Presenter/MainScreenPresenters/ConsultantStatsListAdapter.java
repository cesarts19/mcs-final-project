package com.example.mcs.interviewhelper.Presenter.MainScreenPresenters;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.mcs.interviewhelper.MainActivity;
import com.example.mcs.interviewhelper.Model.data.Stat;
import com.example.mcs.interviewhelper.R;

import java.util.ArrayList;
import java.util.HashSet;

public class ConsultantStatsListAdapter  extends BaseAdapter{
    private ArrayList<Stat> ConsultantStats;

    public ConsultantStatsListAdapter(ArrayList<Stat> consultantStats) {
        ConsultantStats = consultantStats;

    }

    @Override
    public int getCount() {
        return ConsultantStats.size();
    }

    @Override
    public Object getItem(int position) {
        return ConsultantStats.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null) {
            LayoutInflater layoutInflater = LayoutInflater.from(MainActivity.CONTEXT);
            convertView = layoutInflater.inflate(R.layout.stats_consultant_item, null);
        }
        TextView ConsultantName = (TextView)convertView.findViewById(R.id.ConsultantName);
        ProgressBar bar = (ProgressBar) convertView.findViewById(R.id.ConsultantProgress);
        int range = ConsultantStats.get(position).getmRange();
        int correct = ConsultantStats.get(position).getmCorrect();
        int ratio = 100*correct/range;
        ConsultantName.setText(" ("+ratio + " %) - "+ConsultantStats.get(position).getmTag());
        bar.setMax(range);
        bar.setProgress(correct);
        return convertView;
    }
}
