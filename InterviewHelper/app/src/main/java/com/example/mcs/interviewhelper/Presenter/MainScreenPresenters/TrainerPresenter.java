package com.example.mcs.interviewhelper.Presenter.MainScreenPresenters;

import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.mcs.interviewhelper.MainActivity;
import com.example.mcs.interviewhelper.Model.data.Stat;
import com.example.mcs.interviewhelper.Presenter.SuggestionPresenters.StatsSectionAdapter;
import com.example.mcs.interviewhelper.R;

import java.util.ArrayList;

public class TrainerPresenter extends MenuPresenter {

    /**
     * Wrapping results.
    * */
    @Override
    public void postMainPresenting() {
        MainActivity.INTERACTOR.getQuestionsFromDB();
    }

    /**@franco_trujillo
     *Methods to present the statistics information on trainer screen
     **/

    @Override
    public void PresentOverallStatisticsFragment(FragmentManager Manager, Fragment fragment, int container){
        Manager.beginTransaction().replace(container,fragment).commit();

    }

    @Override
    public void AddFragmentsToViewPager(ArrayList<android.support.v4.app.Fragment> fragments, StatsSectionAdapter adapter, ViewPager pager) {
        adapter.addFragments(fragments);
        pager.setAdapter(adapter);
    }

    @Override
    public void SetTopicStatsListAdapter(View view, ArrayList<Stat> list) {
        ListView statsView = (ListView) view.findViewById(R.id.ByTopic);
        TopicStatsListAdapter adapter = new TopicStatsListAdapter(list);
        statsView.setAdapter(adapter);
    }

    @Override
    public void SetConsultantStatsListAdapter(View view, ArrayList<Stat> list) {
        ListView statsView = (ListView) view.findViewById(R.id.ByConsultant);
        ConsultantStatsListAdapter adapter = new ConsultantStatsListAdapter(list);
        statsView.setAdapter(adapter);
    }

    @Override
    public void SetOverallProgressBar(View view, int percentage) {
        ProgressBar progressBar;
        ProgressBar progressBar2;
        TextView textStats;
        progressBar = (ProgressBar)view.findViewById(R.id.circular_progress_bar);
        textStats = (TextView)view.findViewById(R.id.text_stat_total);
        progressBar2 = (ProgressBar)view.findViewById(R.id.circular_progress_bar_2);
        progressBar2.setMax(100);
        progressBar2.setProgress(100);
        progressBar.setMax(100);
        progressBar.setProgress(percentage);
        textStats.setText(""+percentage + "%");

    }
}
