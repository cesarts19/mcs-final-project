package com.example.mcs.interviewhelper.Model.data;

import com.google.firebase.database.Exclude;

import java.util.ArrayList;

/**
 * Implements the specialization for a multiple choice question
 *
 * @itzcoatl90
 *
 * Made changes on @roberto.cervantes1988@gmail.com code...
 * mRightAnswer is now an ArrayList instead of String.
 *
 * id and answer indexes are now long for db matters.
 *
 */

public class MultipleChoiceQuestion extends BasicQuestion {

    public static final String MULTIPLE_PARCELABLE_NAME = "com.example.mcs.interviewhelper.MultipleChoiceQuestion";
    private ArrayList<String> mRightAnswer;
    private ArrayList<String> mWrongAnswer;

    private ArrayList<Long> mCorrectAnswerIndexes;
    private ArrayList<Long> mWrongAnswerIndexes;

    public MultipleChoiceQuestion(String mQuestion, String mCreator,
                                  String mValidator, String mResponse,
                                  ArrayList<String> mTags, int mDifficulty,
                                  int mStatus, long mId, int mCheckStatus,
                                  ArrayList<String> mRightAnswer, ArrayList<String> mWrongAnswer) {
        super(mQuestion, mCreator, mValidator, mResponse, mTags, mDifficulty, mStatus, mId, mCheckStatus);
        this.mRightAnswer = mRightAnswer;
        this.mWrongAnswer = mWrongAnswer;
        this.mCorrectAnswerIndexes = new ArrayList<>();
        this.mWrongAnswerIndexes = new ArrayList<>();
    }

    public MultipleChoiceQuestion(MultipleQuestionWrapper input){
        this.mQuestion = input.question;
        this.mCreator = input.submmitedBy;
        this.mValidator = input.validatedBy;
        this.mResponse = "";
        this.mTags = input.Tags;
        this.mDifficulty = input.difficulty;
        this.mStatus = input.status;
        this.mId = input.id;
        this.mCheckedStatus = 0;
        this.mRightAnswer = new ArrayList<String>();
        this.mWrongAnswer = new ArrayList<String>();
        this.mCorrectAnswerIndexes = input.CorrectResponses;
        this.mWrongAnswerIndexes = input.WrongResponses;
    }

    public MultipleChoiceQuestion() {
        this.mWrongAnswer = new ArrayList<>();
        this.mCorrectAnswerIndexes = new ArrayList<>();
        this.mWrongAnswerIndexes = new ArrayList<>();
    }
    @Exclude
    public ArrayList<String> getmRightAnswer() {
        return mRightAnswer;
    }

    @Exclude
    public void setmRightAnswer(ArrayList<String> mRightAnswer) {
        this.mRightAnswer = mRightAnswer;
    }

    @Exclude
    public ArrayList<String> getmWrongAnswer() {
        return mWrongAnswer;
    }

    @Exclude
    public void setmWrongAnswer(ArrayList<String> mWrongAnswer) {
        this.mWrongAnswer = mWrongAnswer;
    }

    @Exclude
    public ArrayList<Long> getmCorrectAnswerIndexes() {
        return mCorrectAnswerIndexes;
    }

    @Exclude
    public void setmCorrectAnswerIndexes(ArrayList<Long> mCorrectAnswerIndexes) {
        this.mCorrectAnswerIndexes = mCorrectAnswerIndexes;
    }

    @Exclude
    public ArrayList<Long> getmWrongAnswerIndexes() {
        return mWrongAnswerIndexes;
    }

    @Exclude
    public void setmWrongAnswerIndexes(ArrayList<Long> mWrongAnswerIndexes) {
        this.mWrongAnswerIndexes = mWrongAnswerIndexes;
    }

    @Override
    public String toString() {
        return "MultipleChoiceQuestion{" +
                "mRightAnswer=" + mRightAnswer +
                ", mWrongAnswer=" + mWrongAnswer +
                ", mCorrectAnswerIndexes=" + mCorrectAnswerIndexes +
                ", mWrongAnswerIndexes=" + mWrongAnswerIndexes +
                '}';
    }
}
