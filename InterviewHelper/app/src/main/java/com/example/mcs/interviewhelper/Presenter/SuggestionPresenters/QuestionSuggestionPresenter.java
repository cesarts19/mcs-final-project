package com.example.mcs.interviewhelper.Presenter.SuggestionPresenters;

import android.os.Parcelable;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.example.mcs.interviewhelper.QuestionSuggestionActivity;
import com.example.mcs.interviewhelper.R;
import com.example.mcs.interviewhelper.View.Suggestions.QuestionEditingScreenFragment;
import com.example.mcs.interviewhelper.View.Suggestions.QuestionListScreenFragment;

import java.util.ArrayList;

/**
 * Base class for the question suggestion implementations
 *
 * @itzcoatl90
 *
 * Made changes on @roberto.cervantes1988@gmail.com code...
 * String parameter "correct" in do SuggestPResentation is now ArrayList.
 *
 */

public abstract class QuestionSuggestionPresenter {
    public abstract void doEditPresentation(QuestionEditingScreenFragment fragment,
                                   View answerContainer, View tagsContainer,
                                   View buttonAdd);

    public abstract void doSuggestPresentation(QuestionEditingScreenFragment fragment,
                                               View questionContainer, View answerContainer,
                                                View tagsContainer,View buttonAdd,
                                                View radioEasy, View radioMedium, View radioAdvance,
                                                String question, ArrayList<String> tags,
                                               ArrayList<String> wrongAnswers, ArrayList<String> correct, int difficulty);

    public void prepareToolbarOnEditingScreen(QuestionSuggestionActivity mContext,int toolbarId){
        Toolbar mToolbar = (Toolbar) mContext.findViewById(toolbarId);
        mContext.setSupportActionBar(mToolbar);
        //mContext.getSupportActionBar().setTitle(null);
    }
    public abstract void doQuestionListPresentation(QuestionListScreenFragment fragment, View listContainer);
    public abstract void deployList();
}
