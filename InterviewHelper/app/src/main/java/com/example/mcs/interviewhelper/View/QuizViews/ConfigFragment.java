package com.example.mcs.interviewhelper.View.QuizViews;

import android.graphics.Color;
import android.os.Bundle;
import android.preference.MultiSelectListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Toast;

import com.example.mcs.interviewhelper.MainActivity;
import com.example.mcs.interviewhelper.Presenter.QuizPresenters.MultipleChoiceQuestionPresenter;
import com.example.mcs.interviewhelper.QuizActivity;
import com.example.mcs.interviewhelper.R;

import static com.example.mcs.interviewhelper.MainActivity.dataBaseTags;

public class ConfigFragment extends PreferenceFragment{

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(QuizActivity.QPRESENTER instanceof MultipleChoiceQuestionPresenter){
            addPreferencesFromResource(R.xml.preference_quiz_training);

            findPreference(getString(R.string.preference_key_maximum_responses))
                    .setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                        @Override
                        public boolean onPreferenceChange(Preference preference, Object newValue) {
                            return isStringValidInteger((String)newValue);
                        }
                    });
        } else {
            addPreferencesFromResource(R.xml.preference_quiz_interview);
        }

        findPreference(getString(R.string.preference_key_question_number))
                .setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                    @Override
                    public boolean onPreferenceChange(Preference preference, Object newValue) {
                        return isStringValidInteger((String) newValue);
                    }
                });

        MultiSelectListPreference multiSelectListPreference =
                (MultiSelectListPreference) getPreferenceManager()
                .findPreference(getString(R.string.preference_key_tags));

//        If they are available, use the tags existing in the database as entries for the list
        if (dataBaseTags.size() > 0){
            CharSequence[] entriesArray = MainActivity.dataBaseTags.toArray(new CharSequence[]{});
            multiSelectListPreference.setEntries(entriesArray);
            multiSelectListPreference.setEntryValues(entriesArray);
            multiSelectListPreference.setDefaultValue(entriesArray);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        Setting a background color to avoid transparency
        view.setBackgroundColor(Color.WHITE);
    }

    private boolean isStringValidInteger(String input){
        if (input.equals("")){
            Toast.makeText(getActivity()
                    ,"You need a number in here"
                    , Toast.LENGTH_SHORT).show();
            return false;
        }
        try {
            int temp = Integer.parseInt(input);
            return true;
        } catch (NumberFormatException nfE){
            Toast.makeText(getActivity()
                    ,"Try with a smaller number"
                    , Toast.LENGTH_SHORT).show();
            return false;
        }
    }
}
