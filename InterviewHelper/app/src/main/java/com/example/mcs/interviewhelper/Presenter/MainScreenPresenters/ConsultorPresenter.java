package com.example.mcs.interviewhelper.Presenter.MainScreenPresenters;

import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mcs.interviewhelper.MainActivity;
import com.example.mcs.interviewhelper.Model.Interactors.RolInteractor;
import com.example.mcs.interviewhelper.Model.data.Stat;
import com.example.mcs.interviewhelper.Presenter.SuggestionPresenters.StatsSectionAdapter;
import com.example.mcs.interviewhelper.R;

import java.util.ArrayList;

public class ConsultorPresenter extends MenuPresenter {

    /**
     * @roberto.cervantes1988@gmail.com
     * Wrapping results.
     * */
    @Override
    public void postMainPresenting() {
        MainActivity.INTERACTOR.loadStatsFragment(MainActivity.CONTEXT);
    }

    /**
     * @roberto.cervantes1988@gmail.com
     * Presenting consultant stats fragment
     * */
    @Override
    public void PresentOverallStatisticsFragment(FragmentManager Manager, Fragment fragment, int container) {
        Manager.beginTransaction().replace(container,fragment).commit();
    }

    /**
     * @roberto.cervantes1988@gmail.com
     * Adding the stats by topic fragment to the view pager
     * */
    @Override
    public void AddFragmentsToViewPager(ArrayList<android.support.v4.app.Fragment> fragments, StatsSectionAdapter adapter, ViewPager pager) {
        //TODO if needed for MVP implement this
        throw new UnsupportedOperationException("UNSUPPORTED YET");
    }

    /**
     * @roberto.cervantes1988@gmail.com
     * Setting the adapter to the list of topics to be showed in the fragment
     * */
    @Override
    public void SetTopicStatsListAdapter(View view, ArrayList<Stat> list) {
        if(list.size() == 0)
            Toast.makeText(MainActivity.CONTEXT, "First time here?" +
                    " please make a Quiz!!!", Toast.LENGTH_LONG).show();
        ListView listStatByTopic = (ListView)view.findViewById(R.id.list_by_topic);
        TopicStatsListAdapter adapter = new TopicStatsListAdapter(list);
        listStatByTopic.setAdapter(adapter);
    }

    /**
     * @roberto.cervantes1988@gmail.com
     * No needed for the consultant
     * */
    @Override
    public void SetConsultantStatsListAdapter(View view, ArrayList<Stat> list) {
        //TODO implement if needed
        throw new UnsupportedOperationException("NOT SUPPORTED YET");
    }

    /**
     * @roberto.cervantes1988@gmail.com
     * Setting the data to be showed in the circular progress bar
     * */
    @Override
    public void SetOverallProgressBar(View view, int percentage) {
        ProgressBar progressBar;
        ProgressBar progressBar2;
        TextView textStats;
        if(RolInteractor.questionTotalGeneral == 0){
            RolInteractor.questionTotalGeneral = 1;
        }
        int correctQuestions = 100*percentage/RolInteractor.questionTotalGeneral;
        progressBar = (ProgressBar)view.findViewById(R.id.circular_progress_bar);
        progressBar2 = (ProgressBar)view.findViewById(R.id.circular_progress_bar_2);
        textStats = (TextView)view.findViewById(R.id.text_stat_total);
        progressBar2.setMax(RolInteractor.questionTotalGeneral);
        progressBar2.setProgress(RolInteractor.questionTotalGeneral);
        progressBar.setMax(RolInteractor.questionTotalGeneral);
        progressBar.setProgress(percentage);
        textStats.setText("" + correctQuestions + "%");
        RolInteractor.questionTotalGeneral = 0;
    }
}
