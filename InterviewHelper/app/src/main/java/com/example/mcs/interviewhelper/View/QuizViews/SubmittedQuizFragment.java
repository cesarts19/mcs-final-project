package com.example.mcs.interviewhelper.View.QuizViews;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.example.mcs.interviewhelper.Model.Interactors.TrainingInteractor;
import com.example.mcs.interviewhelper.QuizActivity;
import com.example.mcs.interviewhelper.R;

/** @franco_trujillo
 *This fragment is the one that is shown once a quiz is submitted by the consultant
 **/
public class SubmittedQuizFragment extends Fragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_submitted_quiz, container, false);
        int Score;
        TextView Message = (TextView) view.findViewById(R.id.MessageText);
        //check the type of question
        if(QuizActivity.INTERACTOR instanceof TrainingInteractor){
            Score = QuizActivity.INTERACTOR.getGrade();
            Message.setText(Integer.toString(Score) + "/" + QuizActivity.NQUESTIONS);
        } else {
            Message.setText("Submitted");
        }
        return view;
    }



}
