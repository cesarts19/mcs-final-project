package com.example.mcs.interviewhelper.View.StatsViews;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.mcs.interviewhelper.MainActivity;
import com.example.mcs.interviewhelper.R;


public class ConsultantStatsByTopicFragment extends Fragment {

    ListView listTopic;

    public ConsultantStatsByTopicFragment() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_consultant_stats_by_topic, container, false);
        MainActivity.INTERACTOR.initiateStatsByTopicFragment(view);
        return view;
    }

}
