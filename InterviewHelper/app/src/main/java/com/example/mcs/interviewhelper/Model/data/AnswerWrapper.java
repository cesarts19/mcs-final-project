package com.example.mcs.interviewhelper.Model.data;

/**
 * @itzcoatl90
 *
 * Made changes, ids are now longs.
 *
 **/
public class AnswerWrapper {
    public long id;
    public String value;
    public AnswerWrapper(){}
    public AnswerWrapper(long id, String answer){
        this.id = id;
        this.value = answer;
    }
}
