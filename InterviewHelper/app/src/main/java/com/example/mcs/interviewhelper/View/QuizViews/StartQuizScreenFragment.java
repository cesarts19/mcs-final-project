package com.example.mcs.interviewhelper.View.QuizViews;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.mcs.interviewhelper.QuizActivity;
import com.example.mcs.interviewhelper.R;
/**franco_trujillo
 * this fragment is in charge of show the screen before start the quiz
 * configure the quiz, and start when ready
 */
public class StartQuizScreenFragment extends Fragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_start_quiz_screen, container, false);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        QuizActivity.PRESENTER.doQuizStartPresentation(this);
    }

    public View getStartButton(){
        return getView().findViewById(R.id.startQuestionnaire);

    }

    public View getConfigurationButton(){
        return getView().findViewById(R.id.config);
    }


}
