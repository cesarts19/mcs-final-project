package com.example.mcs.interviewhelper.View.StatsViews;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.mcs.interviewhelper.MainActivity;
import com.example.mcs.interviewhelper.Model.data.Stat;
import com.example.mcs.interviewhelper.Presenter.MainScreenPresenters.ConsultantStatsListAdapter;
import com.example.mcs.interviewhelper.Presenter.SuggestionPresenters.StatsSectionAdapter;
import com.example.mcs.interviewhelper.R;

import java.util.ArrayList;

import static android.os.Build.VERSION_CODES.M;

/**franco_trujillo
 * this fragment is in charge of show the questions from the quiz
 */
public class TrainerStatsByConsultantFragment extends Fragment {


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_trainer_stats_consultant, container, false);
        MainActivity.INTERACTOR.initiateStatsByConsultantFragment(view);

        return view;
    }







}
