package com.example.mcs.interviewhelper.Model.data.ContentProvider;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class InterviewHelperDatabaseHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "interviewhelper.db";
    private static final int DATABASE_VERSION = 1;

    public InterviewHelperDatabaseHelper(Context context) {
        super(context,DATABASE_NAME,null,DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        TagsTable.onCreate(db);
        TrainerFeedbackTable.onCreate(db);
        QuizTable.onCreate(db);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        TagsTable.onUpgrade(db,oldVersion,newVersion);
        TrainerFeedbackTable.onUpgrade(db, oldVersion, newVersion);
        QuizTable.onUpgrade(db, oldVersion, newVersion);
    }
}
