package com.example.mcs.interviewhelper.View.Suggestions;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.mcs.interviewhelper.Model.data.BasicQuestion;
import com.example.mcs.interviewhelper.Model.data.MultipleChoiceQuestion;
import com.example.mcs.interviewhelper.QuestionSuggestionActivity;
import com.example.mcs.interviewhelper.R;

import java.util.ArrayList;


/**
 *@roberto.cervantes1988@gmail.com
 *
 * Fragment that holds a List of the suggested question posted by a Consultant.
 * Every item in the list it is showing the Question, Name of the cobsultant that posted the question
 * and the date of the post.
 *
 * */
public class QuestionListScreenFragment extends Fragment {

    public static ArrayList<BasicQuestion> BASIC_SUGGESTIONS = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_question_validating_screen, container, false);
        //Call to the presenter method doQuestionListPresentation
        QuestionSuggestionActivity.PRESENTER.doQuestionListPresentation(this, view.findViewById(R.id.list_questions));
        return view;
    }

}
