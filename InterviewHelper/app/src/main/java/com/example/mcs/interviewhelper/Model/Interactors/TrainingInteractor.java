package com.example.mcs.interviewhelper.Model.Interactors;

import android.content.ContentValues;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.net.Uri;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.view.View;

import com.example.mcs.interviewhelper.Model.BackendCallers.BackendCaller;
import com.example.mcs.interviewhelper.Model.BackendCallers.FirebaseCaller;
import com.example.mcs.interviewhelper.Model.data.BasicQuestion;
import com.example.mcs.interviewhelper.Model.data.Constants;
import com.example.mcs.interviewhelper.Model.data.ContentProvider.InterviewHelperContentProvider;
import com.example.mcs.interviewhelper.Model.data.ContentProvider.TagsTable;
import com.example.mcs.interviewhelper.Model.data.MultipleChoiceQuestion;
import com.example.mcs.interviewhelper.Model.data.Quiz;
import com.example.mcs.interviewhelper.QuizActivity;
import com.example.mcs.interviewhelper.R;
import com.example.mcs.interviewhelper.View.QuizViews.ConfigFragment;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

/**
 * @franco_trujillo This class is the interactor in charge of the specific implementations
 * that the multiple choice questions needs
 **/

public class TrainingInteractor extends QuizInteractor {

    /**
     * @franco_trujillo set listener for start button, ask questions to DB, initialize required variables, and
     * load next fragment
     * @itzcoatl90 Made changes on @franco_trujillo code... Connected to backend caller on Firebase.
     * Also, QUIZ2SHOWMULTIPLE is stored on QUIZ2SHOW instead of been two separated attributes.
     **/
    @Override
    public void setStartQuizListener(View view) {
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getQuestionsFromDB();
            }
        });
    }

    @Override
    public void resumeInteraction() {
        initializeStatics();
        QuizActivity.INTERACTOR.goToNextScreen(QuizActivity.NEXT_FRAGMENT);
    }

    private void getQuestionsFromDB() {
        BackendCaller aux = new FirebaseCaller();
        QuizActivity.CONTEXT.startProDialog();
        // Here is meant to get the parametrizations from configuration

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(
                QuizActivity.CONTEXT
        );
        Resources res = QuizActivity.CONTEXT.getResources();
        int questionQuantity = Integer.parseInt(
                preferences.getString(
//                        Getting the key name for the preference
                        res.getString(R.string.preference_key_question_number),
//                        Default value in case the preference hasn't been set
                        String.valueOf(res.getInteger(R.integer.number_maximum_of_questions_default))
                )
        );

        int difficultyLevel = Integer.parseInt(
                preferences.getString(
//                        Getting the key name for the preference
                        res.getString(R.string.preference_key_difficulty),
//                        Default value in case the preference hasn't been set
                        String.valueOf(res.getInteger(R.integer.number_question_difficulty_default))
                )
        );

//        Preparing the default set value
        Set<String> setDefaultTags = new HashSet<>(
                Arrays.asList(res.getStringArray(R.array.array_empty))
        );

        Set<String> topicTags = preferences.getStringSet(
                res.getString(R.string.preference_key_tags),
                setDefaultTags
        );

        int maximumResponseQuantity = Integer.parseInt(
                preferences.getString(
                        res.getString(R.string.preference_key_maximum_responses),
                        String.valueOf(res.getInteger(R.integer.number_maximum_of_responses_default))
                )
        );
        //////////////////////
        aux.receiveMultipleChoiceQuestions(
                questionQuantity,
                difficultyLevel,
                new ArrayList<>(topicTags),
                maximumResponseQuantity,
                QuizActivity.CONTEXT,
                Constants.QUIZ_INDEX,
                QuizActivity.CONTEXT);
    }

    @Override
    public void setConfigButtonListener(View view) {
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PreferenceFragment configFragment = new ConfigFragment();
                android.app.FragmentManager fragmentManager = QuizActivity.CONTEXT.getFragmentManager();
                fragmentManager.beginTransaction()
                        .add(R.id.Act5Fragment, configFragment)
                        .addToBackStack(null)
                        .commit();
            }
        });
    }

    /**
     * @itzcoatl90
     *
     * Added STRING_RESPONSES
     *
    **/

    private void initializeStatics(){
        QuizActivity.CURRENTQUESTION = 0;
        QuizActivity.NQUESTIONS = QuizActivity.QUIZ2SHOW.size();
        QuizActivity.RESPONSES =
                new ArrayList<>(Collections.nCopies(QuizActivity.QUIZ2SHOW.size(), -1));
        QuizActivity.RIGHTRESPONSES =
                new ArrayList<>(Collections.nCopies(QuizActivity.QUIZ2SHOW.size(),-2));
        QuizActivity.STRINGS_RESPONSES =
                new ArrayList<>();
    }

    /**
     * @franco_trujillo load and present next question
     **/
    @Override
    public void loadNextQuestion(View view) {
        QuizActivity.CURRENTQUESTION++;
        QuizActivity.QPRESENTER.presentNextQuestion(view);
    }

    /**
     * @franco_trujillo load and present last question
     **/
    @Override
    public void loadLastQuestion(View view) {
        QuizActivity.CURRENTQUESTION--;
        QuizActivity.QPRESENTER.presentNextQuestion(view);
    }

    /**
     * @franco_trujillo present first question
     **/
    @Override
    public void loadQuestionnaire(View view) {
        QuizActivity.QPRESENTER.presentNextQuestion(view);
        QuizActivity.INTERACTOR.deleteButtons(view);
    }

    @Override
    public void saveCurrentResponse(View view) {
        throw new UnsupportedOperationException("method not supported yet saving inside adapter");
    }

    @Override
    public View getAnswerView(View view) {
        return view.findViewById(R.id.AnswersMultipleQuestion);
    }

    /**
     * @franco_trujillo
     *
     * save and submit responses when finish the quiz
     *
     * @itzcoatl90
     *
     * Made changes here, as the callbacks may be Async, this doesn't return an answer.
     *
     **/
    @Override
    public void submitResponses() {
        BackendCaller caller = new FirebaseCaller();
        for (int i = 0; i < QuizActivity.QUIZ2SHOW.size(); i++) {
            QuizActivity.QUIZ2SHOW.get(i)
                    .setmResponse(QuizActivity.STRINGS_RESPONSES.get(i));
            if(QuizActivity.RESPONSES.get(i).equals(QuizActivity.RIGHTRESPONSES.get(i))){
                QuizActivity.QUIZ2SHOW.get(i)
                        .setmCheckedStatus(1);
            }else{
                QuizActivity.QUIZ2SHOW.get(i)
                        .setmCheckedStatus(2);
            }
        }
        Quiz quiz = new Quiz();
        quiz.setmQuestion(QuizActivity.QUIZ2SHOW);
        quiz.setmSubmissionDate(new Date());
        quiz.setmAnsweredBy(QuizActivity.CONTEXT.mUser.getUserId());
        caller.sendTrainingQuiz(quiz,QuizActivity.CONTEXT,QuizActivity.CONTEXT);
        setStatsToDB(quiz);
        RolInteractor.retrieveFromDB(quiz.getmAnsweredBy());

    }

    /**
     * @roberto.cervantes1988@gmail.com
     *
     * Function that obtain for quiz object the current question, the user and the tags to be
     * stored in the Internal SQLite Database
     *
     * */
    public void setStatsToDB(Quiz quiz){
        ArrayList<BasicQuestion> questions = new ArrayList<>();
        ArrayList<String> tags = new ArrayList<>();
        BasicQuestion question;
        String user, tag;
        int response;
        questions = quiz.getmQuestion();
        user = quiz.getmAnsweredBy();
        for(int i = 0; i < questions.size(); i++){
            question = questions.get(i);
            tags = question.getmTags();
            response = question.getmCheckedStatus();
            for(int j = 0; j < tags.size(); j++){
                tag = tags.get(j);
                storeStatsToBD(user, tag, 1, response);
            }
        }
    }

    /**
     * @roberto.cervantes1988@gmail.com
     *
     * Function that receives a tag object and store it in the internal SQLite Database
     * using a Content Provider as an abstraction to manage the data.
     *
     * */
    public void storeStatsToBD(String user, String tag, int total, int status){
        ContentValues values = new ContentValues();
        values.put(TagsTable.COLUMN_USER, user);
        values.put(TagsTable.COLUMN_TAG, tag);
        values.put(TagsTable.COLUMN_TOTAL, total);
        values.put(TagsTable.COLUMN_CORRECT, status);

        //New Stat to be stored in the local SQLite DB using a Content Provider
        Uri statUri = QuizActivity.CONTEXT.getContentResolver().insert(InterviewHelperContentProvider.CONTENT_URI,
                values);
    }

    /**
     * @franco_trujillo load the answers of certain question, randomize answers,and save right answer,
     * retrieve the array with the answers
     * @itzcoatl90 Made changes on @franco_trujillo code, as  .getmRightAnswer() on Multiple choice is now an ArrayList,
     * the method randomize the correct answer as the array may contain several correct answers.
     **/
    @Override
    public ArrayList<String> getAnswerArray(int position) {
        String right = ((MultipleChoiceQuestion) QuizActivity.QUIZ2SHOW.get(position))
                .getmRightAnswer().get(
                        (new Random()).nextInt(((MultipleChoiceQuestion) QuizActivity.QUIZ2SHOW.get(position))
                                .getmRightAnswer().size())
                );
        ArrayList<String> answers = new ArrayList<>();
        answers.add(right);
        answers.addAll(((MultipleChoiceQuestion) QuizActivity.QUIZ2SHOW.get(position))
                .getmWrongAnswer());
        Collections.shuffle(answers);
        int rightPos = answers.indexOf(right);
        QuizActivity.RIGHTRESPONSES.set(position, rightPos);
        return answers;
    }


}
