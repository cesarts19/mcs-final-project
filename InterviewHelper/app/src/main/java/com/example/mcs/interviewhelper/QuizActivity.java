package com.example.mcs.interviewhelper;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import com.example.mcs.interviewhelper.Model.Interactors.QuizInteractor;
import com.example.mcs.interviewhelper.Model.data.BasicQuestion;
import com.example.mcs.interviewhelper.Model.data.Command;
import com.example.mcs.interviewhelper.Model.data.User;
import com.example.mcs.interviewhelper.Presenter.QuizPresenters.QuestionPresenter;
import com.example.mcs.interviewhelper.Presenter.QuizPresenters.QuizPresenter;

import java.util.ArrayList;
/**franco_trujillo
 * Quiz activity in charge of showing, saving, and submitting the test
 */
public class QuizActivity extends AppCompatActivity implements Command {

    public static QuizInteractor INTERACTOR = null;
    public static QuizPresenter PRESENTER = null;
    public static QuestionPresenter QPRESENTER = null;
    public static QuizActivity CONTEXT = null;
    public static Fragment NEXT_FRAGMENT = null;
    public static ArrayList<BasicQuestion> QUIZ2SHOW = null;
    public static Integer CURRENTQUESTION = null;
    public static Integer NQUESTIONS = null;
    public static ArrayList<Integer> RESPONSES = null;
    public static ArrayList<String> STRINGS_RESPONSES = null;
    public static ArrayList<Integer> RIGHTRESPONSES = null;
    public ProgressDialog proDialog = null;
    public User mUser = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);
        if(mUser == null){
            mUser = getIntent().getExtras().getParcelable(User.USER_PARCELABLE_NAME);
        }
        CONTEXT = this;
        INTERACTOR.goToNextScreen(NEXT_FRAGMENT);
    }

    public void startNextFragment(Fragment fragment){
        NEXT_FRAGMENT = fragment;
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.Act5Fragment, NEXT_FRAGMENT)
                .commit();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        INTERACTOR = null;
        PRESENTER = null;
        QPRESENTER = null;
        CONTEXT = null;
        NEXT_FRAGMENT = null;
        QUIZ2SHOW = null;
        CURRENTQUESTION = null;
        NQUESTIONS = null;
        RESPONSES = null;
        RIGHTRESPONSES = null;
    }

    /**
     * @itzcoatl90
     *
     * Methods for multiple uses
     *
     * */
    public void startProDialog(){
        proDialog = new ProgressDialog(this);
        proDialog.setMessage("Please wait...");
        proDialog.setCancelable(false);
        proDialog.show();
    }

    public void stopProDialog(){
        if (proDialog.isShowing()) {
            proDialog.dismiss();
        }
    }

    /**
     * @itzcoatl90
     *
     * This methods are the implementation for the command pattern design.
     * Regardless of Async of Sync, this methods are called from the backends
     * to continue execution.
     * */

    @Override
    public void executeOnSendSuccessful() {
        stopProDialog();
        QuizActivity.INTERACTOR.goToNextScreen(QuizActivity.NEXT_FRAGMENT);
    }

    @Override
    public void executeOnSendFail() {
        stopProDialog();
        // TODO send to local database.
        QuizActivity.INTERACTOR.goToNextScreen(QuizActivity.NEXT_FRAGMENT);
    }

    @Override
    public void executeOnReceiveSuccessful() {
        stopProDialog();
        INTERACTOR.resumeInteraction();
    }

    @Override
    public void executeOnReceiveFail() {
        stopProDialog();
        // I wonder if this work? (returs to previous activity)
        finish();
    }
}
