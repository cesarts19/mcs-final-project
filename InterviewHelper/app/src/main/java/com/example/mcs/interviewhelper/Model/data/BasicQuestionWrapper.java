package com.example.mcs.interviewhelper.Model.data;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;

public class BasicQuestionWrapper {
    public ArrayList<String> Tags;
    public int difficulty;
    public long id;
    public String question;
    public int status;
    public String submmitedBy;
    public String type;
    public String validatedBy;
    public BasicQuestionWrapper(){}
    public BasicQuestionWrapper(BasicQuestion question){
        Tags = new ArrayList<String>();
        Tags.addAll(question.getmTags());
        difficulty = question.getmDifficulty();
        id = question.getmId();
        this.question = question.getmQuestion();
        status = question.getmStatus();
        submmitedBy = question.getmCreator();
        type = "interview";
        validatedBy = question.getmValidator();
    }
}
