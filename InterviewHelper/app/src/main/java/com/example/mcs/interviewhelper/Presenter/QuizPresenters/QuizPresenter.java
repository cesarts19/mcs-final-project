package com.example.mcs.interviewhelper.Presenter.QuizPresenters;

import android.view.View;
import android.widget.Button;

import com.example.mcs.interviewhelper.QuizActivity;
import com.example.mcs.interviewhelper.View.QuizViews.StartQuizScreenFragment;

/**franco_trujillo
 * presenter in charge of changes the common elements and general views
 */
public class QuizPresenter {
    public void doQuizStartPresentation(StartQuizScreenFragment fragment){
        View view = fragment.getStartButton();
        //view.setBackgroundColor(Color.RED);
        QuizActivity.INTERACTOR.setStartQuizListener(view);

        View viewConfigButton = fragment.getConfigurationButton();
        QuizActivity.INTERACTOR.setConfigButtonListener(viewConfigButton);
    }

    public void changeNextForSubmit(View view) {
        ((Button)view).setText("SUBMIT");
    }

    public void changeSubmitForNext(View view) {
        ((Button)view).setText("NEXT");
    }

}
