package com.example.mcs.interviewhelper.Presenter;

import android.support.v7.widget.Toolbar;
import android.view.View;

import com.example.mcs.interviewhelper.LoginActivity;
import com.example.mcs.interviewhelper.Presenter.SuggestionPresenters.QuestionSuggestionPresenter;
import com.example.mcs.interviewhelper.QuestionSuggestionActivity;
import com.example.mcs.interviewhelper.R;

public class GenericPresenter {

    View focusView = null;

    /**
     * @itzcoatl90@gmail.com
     *
     * Function to clean the emailView and passwordView.
     *
     * */
    public void clean(LoginActivity activity){
        activity.mEmailView.setError(null);
        activity.mPasswordView.setError(null);
    }

    /**
     * @itzcoatl90@gmail.com
     *
     * Function to set the error when the user type a incorrect password.
     *
     * */
    public void errorOnPassword(LoginActivity activity){
        activity.mPasswordView.setError(activity.getString(R.string.error_invalid_password));
        focusView = activity.mPasswordView;
    }

    /**
     * @itzcoatl90@gmail.com
     *
     * Function to set the error when the user type a incorrect email.
     *
     * */
    public void errorOnEMail(LoginActivity activity,String errorMessage){
        activity.mEmailView.setError(errorMessage);
        focusView = activity.mEmailView;
    }

    /**
     * @itzcoatl90@gmail.com
     *
     * Function that give focus to the focusView.
     *
     * */
    public void focusMessage(){
        if(focusView != null){
            focusView.requestFocus();
        }
    }

    /**
     * @itzcoatl90@gmail.com
     *
     * Function that initialize the ToolBar widget to be showed on the editing screen.
     *
     * */
    public void prepareToolbarOnEditingScreen(QuestionSuggestionActivity activity,int toolbarId){
        Toolbar mToolbar = (Toolbar) activity.findViewById(toolbarId);
        activity.getMainToolbar().setVisibility(View.GONE);
        mToolbar.setVisibility(View.VISIBLE);
        activity.setSupportActionBar(mToolbar);
    }

    /**
     * @itzcoatl90@gmail.com
     *
     * Function that initialize the ToolBar widget to be showed on the suggestion Activity.
     *
     * */
    public void setToolbarForSuggestionActivity(QuestionSuggestionActivity activity){
        activity.getMainToolbar().setVisibility(View.VISIBLE);
        activity.getConsultantToolbar().setVisibility(View.GONE);
        activity.getTrainerToolbar().setVisibility(View.GONE);
        activity.setSupportActionBar(activity.getMainToolbar());
    }

}
