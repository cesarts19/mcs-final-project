package com.example.mcs.interviewhelper.Presenter.MainScreenPresenters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mcs.interviewhelper.MainActivity;
import com.example.mcs.interviewhelper.Model.data.Stat;
import com.example.mcs.interviewhelper.QuestionSuggestionActivity;
import com.example.mcs.interviewhelper.R;

import java.text.DecimalFormat;
import java.util.ArrayList;

import static com.example.mcs.interviewhelper.R.id.ConsultantName;

/**
 * Adapter to manage the list of topics to be showed
 * */
public class TopicStatsListAdapter extends BaseAdapter{

    private ArrayList<Stat> mStatItem;

    public TopicStatsListAdapter(ArrayList<Stat> mStatItem) {
        this.mStatItem = mStatItem;
    }

    @Override
    public int getCount() {
        return mStatItem.size();
    }

    @Override
    public Object getItem(int position) {
        return mStatItem.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            LayoutInflater layoutInflater = LayoutInflater.from(MainActivity.CONTEXT);
            convertView = layoutInflater.inflate(R.layout.stats_topic_item, null);
        }

            Stat mStat = mStatItem.get(position);
            TextView textTagName = (TextView)convertView.findViewById(R.id.TopicName);
            ProgressBar progressTopic = (ProgressBar)convertView.findViewById(R.id.TopicProgress);
            int range = mStat.getmRange();
            int correct = mStat.getmCorrect();
            int ratio;
            try{
                ratio = 100*correct/range;
            }catch(java.lang.ArithmeticException e){
                ratio = 0;
            }
            textTagName.setText(" ("+ratio+ " %) - "+mStat.getmTag());

            progressTopic.setMax(range);
            progressTopic.setProgress(correct);

        return convertView;
    }
}
