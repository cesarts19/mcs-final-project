package com.example.mcs.interviewhelper.Presenter.SuggestionPresenters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.example.mcs.interviewhelper.Model.data.BasicQuestion;
import com.example.mcs.interviewhelper.Model.data.MultipleChoiceQuestion;
import com.example.mcs.interviewhelper.QuestionSuggestionActivity;
import com.example.mcs.interviewhelper.R;

import java.util.ArrayList;

import static android.R.attr.value;

/**
 * @roberto.cervantes1988@gmail.com
 *
 * Adapter class that manages the list of suggested questions according to the type of question
 * is going to call Multiple Choice Presenter or Open Question Presenter.
 *
 * */

public class ShowListItemsAdapter extends BaseAdapter implements ListAdapter{

    private ArrayList<BasicQuestion> mBasicItem;

    public ShowListItemsAdapter(ArrayList<BasicQuestion> mItem) {
        this.mBasicItem = mItem;
    }

    @Override
    public int getCount() {
        return  mBasicItem.size();
    }

    @Override
    public Object getItem(int position) {
        return mBasicItem.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            LayoutInflater layoutInflater = LayoutInflater.from(QuestionSuggestionActivity.CONTEXT);
            convertView = layoutInflater.inflate(R.layout.wrapper_item_suggestion_list, null);
        }

        final BasicQuestion basicItem = mBasicItem.get(position);
        TextView textConsultorName = (TextView)convertView.findViewById(R.id.text_consultor_name);
        textConsultorName.setText(basicItem.getmCreator() + " - " + basicItem.getmQuestion());

        // THIS BIND IS MEANT TO BE IN AN INTERACTOR, MAYBE FOR THE NEXT ONE...
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                QuestionSuggestionActivity.QUESTION_PRESENTING = basicItem;
                QuestionSuggestionActivity.INTERACTOR.goToNextScreen(QuestionSuggestionActivity.CONTEXT);
            }
        });

        return convertView;
    }
}
