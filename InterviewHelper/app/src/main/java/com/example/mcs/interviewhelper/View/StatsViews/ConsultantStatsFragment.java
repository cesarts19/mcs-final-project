package com.example.mcs.interviewhelper.View.StatsViews;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.mcs.interviewhelper.MainActivity;
import com.example.mcs.interviewhelper.R;




public class ConsultantStatsFragment extends android.app.Fragment {

    public int percentage;

    public ConsultantStatsFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            percentage = getArguments().getInt(MainActivity.INTERACTOR.PERCENTAGE_OVERALL);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_consultant_stats, container, false);
        MainActivity.INTERACTOR.initiateOverallFragment(view, percentage);
        return view;
    }
}
