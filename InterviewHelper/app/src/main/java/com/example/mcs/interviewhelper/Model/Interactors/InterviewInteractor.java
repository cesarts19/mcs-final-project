package com.example.mcs.interviewhelper.Model.Interactors;

import android.content.SharedPreferences;
import android.content.res.Resources;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.EditText;

import com.example.mcs.interviewhelper.MainActivity;
import com.example.mcs.interviewhelper.Model.BackendCallers.BackendCaller;
import com.example.mcs.interviewhelper.Model.BackendCallers.FirebaseCaller;
import com.example.mcs.interviewhelper.Model.data.Constants;
import com.example.mcs.interviewhelper.Model.data.Quiz;
import com.example.mcs.interviewhelper.QuizActivity;
import com.example.mcs.interviewhelper.R;
import com.example.mcs.interviewhelper.View.QuizViews.ConfigFragment;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**@franco_trujillo
 *This class is the interactor in charge of the specific implementations
 *that the interview questions needs
 **/
public class InterviewInteractor extends QuizInteractor {

    /** @franco_trujillo
     * set listener for start button, ask questions to DB, initialize required variables, and
     * load next fragment
     *
     * @itzcoatl90
     *
     * Made changes on @franco_trujillo code... Connected to backend caller on Firebase.
     * Also, QUIZ2SHOWMULTIPLE is stored on QUIZ2SHOW instead of been two separated attributes.
     **/
    @Override
    public void setStartQuizListener(View view) {
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getQuestionsFromDB();
            }
        });
    }

    @Override
    public void resumeInteraction(){
        initializeStatics();
        QuizActivity.INTERACTOR.goToNextScreen(QuizActivity.NEXT_FRAGMENT);
    }

    @Override
    public void setConfigButtonListener(View view) {
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PreferenceFragment configFragment = new ConfigFragment();
                android.app.FragmentManager fragmentManager = QuizActivity.CONTEXT.getFragmentManager();
                fragmentManager.beginTransaction()
                        .add(R.id.Act5Fragment, configFragment)
                        .addToBackStack(null)
                        .commit();
            }
        });
    }

    private void getQuestionsFromDB() {
        BackendCaller aux = new FirebaseCaller();
        QuizActivity.CONTEXT.startProDialog();

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(
                QuizActivity.CONTEXT
        );
        Resources res = QuizActivity.CONTEXT.getResources();
        int questionQuantity = Integer.parseInt(
                preferences.getString(
//                        Getting the key name for the preference
                        res.getString(R.string.preference_key_question_number),
//                        Default value in case the preference hasn't been set
                        String.valueOf(res.getInteger(R.integer.number_maximum_of_questions_default))
                )
        );

        int difficultyLevel = Integer.parseInt(
                preferences.getString(
//                        Getting the key name for the preference
                        res.getString(R.string.preference_key_difficulty),
//                        Default value in case the preference hasn't been set
                        String.valueOf(res.getInteger(R.integer.number_question_difficulty_default))
                )
        );

//        Preparing the default set value
        Set<String> defaultTags;
        if (MainActivity.dataBaseTags.size() > 0) {
            defaultTags = new HashSet<>(MainActivity.dataBaseTags);
        }
        else {
            defaultTags = new HashSet<>(
                    Arrays.asList(res.getStringArray(R.array.array_empty))
            );
        }
//        Setting the topic tags with their default value
        Set<String> topicTags = preferences.getStringSet(
                res.getString(R.string.preference_key_tags),
                defaultTags
        );

        //////////////////////
        aux.receiveBasicQuestions(
                questionQuantity,
                difficultyLevel,
                new ArrayList<>(topicTags),
                QuizActivity.CONTEXT,
                Constants.QUIZ_INDEX,
                QuizActivity.CONTEXT);
    }
    private void initializeStatics() {
        QuizActivity.CURRENTQUESTION = 0;
        QuizActivity.NQUESTIONS = QuizActivity.QUIZ2SHOW.size();
    }

    /** @franco_trujillo
     * load and present next question
     **/
    @Override
    public void loadNextQuestion(View view) {
        QuizActivity.CURRENTQUESTION++;
        QuizActivity.QPRESENTER.presentNextQuestion(view);
        String answerText = QuizActivity.QUIZ2SHOW.get(QuizActivity.CURRENTQUESTION).getmResponse();
        QuizActivity.QPRESENTER.drawAnswer(view, answerText);
        QuizActivity.INTERACTOR.updateButtons(view);

    }

    /** @franco_trujillo
     * load and present last question
     **/
    @Override
    public void loadLastQuestion(View view) {
        QuizActivity.CURRENTQUESTION--;
        QuizActivity.QPRESENTER.presentNextQuestion(view);
        String answerText = QuizActivity.QUIZ2SHOW.get(QuizActivity.CURRENTQUESTION).getmResponse();
        QuizActivity.QPRESENTER.drawAnswer(view, answerText);
        QuizActivity.INTERACTOR.updateButtons(view);


    }

    /** @franco_trujillo
     * present first question
     **/
    @Override
    public void loadQuestionnaire(View view) {
        QuizActivity.QPRESENTER.presentNextQuestion(view);
        String answerText = QuizActivity.QUIZ2SHOW.get(QuizActivity.CURRENTQUESTION).getmResponse();
        QuizActivity.QPRESENTER.drawAnswer(view, answerText);
    }

    /** @franco_trujillo
     * save the responses given from the consultant
     **/
    @Override
    public void saveCurrentResponse(View view) {
        EditText answerTyped = (EditText) view.findViewById(R.id.AnswerOpenQuestion);
        String TypedAnswer = answerTyped.getText().toString();
        QuizActivity.QUIZ2SHOW.get(QuizActivity.CURRENTQUESTION).setmResponse(TypedAnswer);
        QuizActivity.QUIZ2SHOW.get(QuizActivity.CURRENTQUESTION).setmCheckedStatus(3);
    }

    @Override
    public View getAnswerView(View view) {
        return view.findViewById(R.id.AnswerOpenQuestion);
    }

    /** @franco_trujillo
     * present first question
     *
     * @itzcoatl90
     *
     * Made changes here, as the callbacks may be Async, this doesn't return an answer.
     *
     **/
    @Override
    public void submitResponses() {
        BackendCaller caller = new FirebaseCaller();
        Quiz quiz = new Quiz();
        quiz.setmQuestion(QuizActivity.QUIZ2SHOW);
        quiz.setmSubmissionDate(new Date());
        quiz.setmAnsweredBy(QuizActivity.CONTEXT.mUser.getUserId());
        caller.sendInterviewQuiz(quiz,QuizActivity.CONTEXT,QuizActivity.CONTEXT);

    }

    @Override
    public ArrayList<String> getAnswerArray(int position) {
        throw new UnsupportedOperationException("Unsupported operation on interview type Interactor");
    }


}
