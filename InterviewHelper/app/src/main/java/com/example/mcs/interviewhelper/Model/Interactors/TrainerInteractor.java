package com.example.mcs.interviewhelper.Model.Interactors;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.mcs.interviewhelper.MainActivity;
import com.example.mcs.interviewhelper.Model.BackendCallers.BackendCaller;
import com.example.mcs.interviewhelper.Model.BackendCallers.FirebaseCaller;
import com.example.mcs.interviewhelper.Model.data.BasicQuestion;
import com.example.mcs.interviewhelper.Model.data.MultipleChoiceQuestion;
import com.example.mcs.interviewhelper.Model.data.Stat;
import com.example.mcs.interviewhelper.Model.data.User;
import com.example.mcs.interviewhelper.Presenter.MainScreenPresenters.ConsultantStatsListAdapter;
import com.example.mcs.interviewhelper.Presenter.MainScreenPresenters.MenuPresenter;
import com.example.mcs.interviewhelper.Presenter.SuggestionPresenters.AddAndRemoveItemsAdapter;
import com.example.mcs.interviewhelper.Presenter.MainScreenPresenters.TopicStatsListAdapter;
import com.example.mcs.interviewhelper.Presenter.SuggestionPresenters.StatsSectionAdapter;
import com.example.mcs.interviewhelper.QuestionSuggestionActivity;
import com.example.mcs.interviewhelper.R;
import com.example.mcs.interviewhelper.View.StatsViews.TrainerStatsByConsultantFragment;
import com.example.mcs.interviewhelper.View.StatsViews.TrainerStatsFragment;
import com.example.mcs.interviewhelper.View.StatsViews.TrainerStatsByTopicFragment;
import com.example.mcs.interviewhelper.View.Suggestions.QuestionEditingScreenFragment;
import com.example.mcs.interviewhelper.View.Suggestions.QuestionListScreenFragment;
import com.example.mcs.interviewhelper.View.Suggestions.QuestionTypeSuggestionSelectionScreenFragment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import static android.media.CamcorderProfile.get;
import static com.example.mcs.interviewhelper.MainActivity.TAGS_STATS;


public class TrainerInteractor extends RolInteractor {
    public ProgressDialog proDialog = null;


    public TrainerInteractor(MenuPresenter PRESENTER){
        super(PRESENTER);
    }

    public TrainerInteractor(){}

    /**
     * @roberto.cervantes1988@gmail.com
     *
     * Function that according the instance received is initializing a
     * fragment sending this fragment to the startNextFragment method in the activity.
     *
     * * Function that receives arguments from the Show List Items Adapter.
     * The arguments are stored in a Bundle and send into the fragment in order to be showed in the UI.
     *
     * */
    @Override
    public void goToNextScreen(QuestionSuggestionActivity activity) {
        if(activity.NEXT_FRAGMENT instanceof QuestionTypeSuggestionSelectionScreenFragment){
            QuestionSuggestionActivity.CONTEXT.startNextFragment(new QuestionListScreenFragment());
            return;
        } else if(activity.NEXT_FRAGMENT instanceof QuestionListScreenFragment){
            QuestionEditingScreenFragment nextFragment = new QuestionEditingScreenFragment();
            activity.GENERIC_PRESENTER.prepareToolbarOnEditingScreen(activity,getIdToolbar());
            activity.PREVIOUS_FRAGMENT = activity.NEXT_FRAGMENT;
            Bundle arguments = new Bundle();
            arguments.putString("Question", activity.QUESTION_PRESENTING.getmQuestion());
            arguments.putStringArrayList("Tags", activity.QUESTION_PRESENTING.getmTags());
            arguments.putStringArrayList("Wrong", (activity.QUESTION_PRESENTING instanceof MultipleChoiceQuestion) ?
                    ((MultipleChoiceQuestion) activity.QUESTION_PRESENTING).getmWrongAnswer() : new ArrayList<String>());
            arguments.putStringArrayList("Correct", (activity.QUESTION_PRESENTING instanceof MultipleChoiceQuestion) ?
                    ((MultipleChoiceQuestion) activity.QUESTION_PRESENTING).getmRightAnswer() : new ArrayList<String>());
            arguments.putInt("Difficulty", activity.QUESTION_PRESENTING.getmDifficulty());
            nextFragment.setArguments(arguments);
            activity.startNextFragment(nextFragment);
            return;
        }
        throw new UnsupportedOperationException("NOT YET SUPPORTED");
    }


    /**
     * @itzcoatl90@gmail.com
     *
     * Function that returns the id of the consultant toolbar
     *
     * */
    @Override
    public int getIdToolbar() {
        return R.id.toolbar_trainer;
    }

    /**
     * @itzcoatl90@gmail.com
     *
     * Function that returns the id of the Activity Splash Drawer
     *
     * */
    @Override
    public int getIdDrawer() {
        return R.menu.activity_splash_drawer_trainer;
    }

    /**
     * @itzcoatl90@gmail.com
     *
     * Temporal Function used to Firebase implementations
     * */
    @Override
    public boolean onNavigationItemSelected(MainActivity activity, MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.nav_review_quizes) {

            /*BackendCaller caller = new FirebaseCaller();
            final ArrayList<BasicQuestion> basicQuizQuestions = new ArrayList<>();
            final ArrayList<MultipleChoiceQuestion> trainingQuizQuestions = new ArrayList<>();
            //Subscribing a listener with the methods to process the Questions that will be requested.
            ((FirebaseCaller)caller).subscribeListener(new BackendCaller.BackendCallerListener() {
                //These methods will be called when the questions have been properly retrieved.
                @Override
                public void onBasicQuestionResponse(ArrayList<BasicQuestion> basicQuestionsResult) {
                    basicQuizQuestions.clear();
                    basicQuizQuestions.addAll(basicQuestionsResult);
                }

                @Override
                public void onMultipleChoiceQuestionResponse(ArrayList<MultipleChoiceQuestion> multipleChoiceQuestionsResult) {
                    trainingQuizQuestions.clear();
                    trainingQuizQuestions.addAll(multipleChoiceQuestionsResult);
                }
            });

            ArrayList<String> tags;
            Collections.addAll( (tags = new ArrayList<>())
                    , "Core Components","Android Basics");

            caller.requestBasicQuestions(2, 1, tags);
            caller.requestMultipleChoiceQuestions(2,1, tags, 3);*/

        } else if (id == R.id.nav_validation) {

            Intent intent = new Intent(activity, QuestionSuggestionActivity.class);
            intent.putExtra(User.USER_PARCELABLE_NAME,activity.mUser);
            activity.startActivity(intent);

        }

        DrawerLayout drawer = (DrawerLayout) activity.findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    /**@franco_trujillo
     * Methods to manage the statistics on the trainer screen bellow
     */
    @Override
    public void loadStatsFragment(Activity activity) {
        android.app.FragmentManager Manager = activity.getFragmentManager();
        Bundle bundle = new Bundle();
        int overall = calculateOverall();
        bundle.putInt(MainActivity.INTERACTOR.PERCENTAGE_OVERALL,overall);
        TrainerStatsFragment stats = new TrainerStatsFragment();
        stats.setArguments(bundle);
        //Presenter job to print on screen
        MainActivity.PRESENTER.PresentOverallStatisticsFragment(Manager,stats,R.id.Statistics);
        ViewPager viewPager = (ViewPager)activity.findViewById(R.id.StatisticsBy);
        setupStatsFragments(viewPager,((AppCompatActivity) activity));
    }

    private void setupStatsFragments(ViewPager viewPager,AppCompatActivity activity){
        StatsSectionAdapter adapter = new StatsSectionAdapter(activity.getSupportFragmentManager());
        //potentially secuential
        TrainerStatsByTopicFragment statsByTopic = new TrainerStatsByTopicFragment();
        //potentially secuential
        TrainerStatsByConsultantFragment statsByConsultant = new TrainerStatsByConsultantFragment();
        ArrayList<Fragment> fragments = new ArrayList<>();
        fragments.add(statsByTopic);
        fragments.add(statsByConsultant);
        MainActivity.PRESENTER.AddFragmentsToViewPager(fragments,adapter,viewPager);
    }

    @Override
    public void initiateStatsByConsultantFragment(View view) {
        //MainActivity.CONSULTANTS_STATS = getStats(1);
        MainActivity.PRESENTER.SetConsultantStatsListAdapter(view,MainActivity.CONSULTANTS_STATS);
    }

    @Override
    public void initiateStatsByTopicFragment(View view) {
        //MainActivity.TAGS_STATS = getStats(2);
        MainActivity.PRESENTER.SetTopicStatsListAdapter(view,MainActivity.TAGS_STATS);
    }

    @Override
    public void initiateOverallFragment(View view, int percentage) {
        MainActivity.PRESENTER.SetOverallProgressBar(view ,percentage);
    }

    private int calculateOverall(){
        int percentage = 0;
        int total = 0;
        int correct = 0;
        for (int i = 0; i < MainActivity.CONSULTANTS_STATS.size(); i++) {
            correct += MainActivity.CONSULTANTS_STATS.get(i).getmCorrect();
            total += MainActivity.CONSULTANTS_STATS.get(i).getmRange();
        }
        if(total == 0){
            return 0;
        }
        return (int) correct*100/total;
    }
    public void bindListenersToAddButtons(View addingTrueAnswersButton, final ArrayList<String> trueAnswersArray,
                                          final AddAndRemoveItemsAdapter trueAnswerAdapter,
                                          View addingFalseAnswersButton, final ArrayList<String> falseAnswersArray,
                                          final AddAndRemoveItemsAdapter falseAnswerAdapter,
                                          View addingTagsButton, final ArrayList<String> tagsArray,
                                          final AddAndRemoveItemsAdapter tagsAdapter){
        // First two buttons may be null if the question is BasicQuestion (Open Question type).
        if(addingTrueAnswersButton != null){
            addingTrueAnswersButton.setVisibility(View.GONE);
        }
        if(addingFalseAnswersButton != null){
            addingFalseAnswersButton.setVisibility(View.GONE);
        }
        // This button is never meant to be null, but we check it anyway.
        if(addingTagsButton != null){
            addingTagsButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    QuestionEditingScreenFragment.question.requestFocus();
                    if(!tagsArray.get(tagsArray.size()-1).equals("")){
                        tagsArray.add("");
                        tagsAdapter.notifyDataSetChanged();
                    } else {
                        Toast.makeText(QuestionSuggestionActivity.CONTEXT,
                                "Not blanks allowed in topic tags.",Toast.LENGTH_SHORT)
                                .show();
                    }
                }
            });
        }
    }

    @Override
    public void getQuestionsFromDB() {
        startProDialog();
        BackendCaller aux = new FirebaseCaller();
        aux.receiveConsultantsInfo(MainActivity.CONTEXT,this);
    }




    private void startProDialog(){
        proDialog = new ProgressDialog(MainActivity.CONTEXT);
        proDialog.setMessage("Please wait...");
        proDialog.setCancelable(false);
        proDialog.show();
    }

    public void stopProDialog(){
        if (proDialog.isShowing()) {
            proDialog.dismiss();
        }
    }

    private void processStatsTAGS(){
        HashMap<String,Stat> statsByTag = new HashMap<>();
        for(int i =0; i<MainActivity.QUIZES.size();i++)
        {
            ArrayList<BasicQuestion> questions = MainActivity.QUIZES.get(i).getmQuestion();

            for(int j =0; j<questions.size();j++){
                int status = questions.get(j).getmCheckedStatus();
                ArrayList<String> TAGS = questions.get(j).getmTags();
                for(int k =0; k<TAGS.size();k++){
                    if(!statsByTag.containsKey(TAGS.get(k))){
                        statsByTag.put(TAGS.get(k),new Stat(TAGS.get(k),0,0));
                    }

                    switch (status){
                        case 0:
                            break;
                        case 1:
                            statsByTag.get(TAGS.get(k)).increaseBoth(); //correct
                            break;
                        case 2:
                            statsByTag.get(TAGS.get(k)).increaseRange(); //incorrect
                            break;
                        default:
                            break;
                    }
                }
            }

        }
        MainActivity.TAGS_STATS = new ArrayList<>();
        for(String key: statsByTag.keySet()){
            MainActivity.TAGS_STATS.add(statsByTag.get(key));
        }
    }

    private void processStatsConsultant(){
        HashMap<String,Stat> statsByConsultant = new HashMap<>();
        MainActivity.CONSULTANTS_STATS = new ArrayList<>();
        for(int i =0; i<MainActivity.QUIZES.size();i++) {
            String consultant = MainActivity.QUIZES.get(i).getmAnsweredBy();
            if(!statsByConsultant.containsKey(consultant)){
                statsByConsultant.put(consultant,new Stat(
                        consultant,0,0
                ));
            }
            ArrayList<BasicQuestion> questions = MainActivity.QUIZES.get(i).getmQuestion();
            for(int j =0; j<questions.size();j++){
                switch(questions.get(j).getmCheckedStatus()){
                    case 1:
                        statsByConsultant.get(consultant).increaseBoth();
                        break;
                    case 2:
                        statsByConsultant.get(consultant).increaseRange();
                        break;
                    default:
                        break;
                }
            }
        }

        MainActivity.CONSULTANTS_STATS = new ArrayList<>();
        for(String key: statsByConsultant.keySet()){
            MainActivity.CONSULTANTS_STATS.add(statsByConsultant.get(key));
        }
    }

    @Override
    public void executeOnSendSuccessful() {
        throw new UnsupportedOperationException("NOT YET IMPLEMENTED");
    }

    @Override
    public void executeOnSendFail() {
        throw new UnsupportedOperationException("NOT YET IMPLEMENTED");
    }

    @Override
    public void executeOnReceiveSuccessful() {
        processStatsTAGS();
        processStatsConsultant();
        loadStatsFragment(MainActivity.CONTEXT);
        stopProDialog();
    }

    @Override
    public void executeOnReceiveFail() {
        stopProDialog();
    }

}
