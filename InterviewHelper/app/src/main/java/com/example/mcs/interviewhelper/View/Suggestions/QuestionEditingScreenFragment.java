package com.example.mcs.interviewhelper.View.Suggestions;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.example.mcs.interviewhelper.Presenter.SuggestionPresenters.QuestionSuggestionPresenter;
import com.example.mcs.interviewhelper.QuestionSuggestionActivity;
import com.example.mcs.interviewhelper.R;

import java.util.ArrayList;

/**
 * @itzcoatl90
 * @roberto.cervantes1988@gmail.com
 *
 * This is the Fragment that holds the editing Questions details, regardless of Rol.
 *
 * */
public class QuestionEditingScreenFragment extends Fragment {

    public static ArrayList<String> TRUE_ANSWERS;
    public static ArrayList<String> WRONG_ANSWERS;
    public static ArrayList<String> TOPIC_TAGS;
    QuestionSuggestionPresenter PRESENTER;
    public static EditText question;

    public QuestionEditingScreenFragment(){
        TRUE_ANSWERS = null;
        WRONG_ANSWERS = null;
        TOPIC_TAGS = new ArrayList<>();
        PRESENTER = null;
        question = null;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_question_editing_screen, container, false);
        question = (EditText) view.findViewById(R.id.etxt_question_input);
        Bundle bundle = getArguments();
        /*

        @roberto.cervantes1988@gmail.com
        Bundle received from the Trainer Interactor each time that the trainer clicks over a
        suggested question item from the suggestion list, if the Bundle has parameters the function
         is going to call the doSuggestPresentation method from the presenter

        @itzcoatl90
        THIS NORMALLY MUST BE IN THE INTERACTOR LOGIC...
        but must be done like this, because fragment view is not yet created, so elements on it
        can't been accessed from outside (even with getters on this inside, findViewById will fail).

        Sending the view as a parameter would fix the problem... but also break "MVP" architecture...
        Maybe for next iteration??

        NOTE: this also break MVP architecture, but this fix doesn't consume develop time.

         */
        if(bundle != null){
            String question = bundle.getString("Question");
            ArrayList<String> tags = bundle.getStringArrayList("Tags");
            ArrayList<String> wrong = bundle.getStringArrayList("Wrong");
            //String correct =  bundle.getString("Correct");
            ArrayList<String> correct =  bundle.getStringArrayList("Correct");
            int difficulty = bundle.getInt("Difficulty");
            QuestionSuggestionActivity.PRESENTER.doSuggestPresentation(this,
                    view.findViewById(R.id.etxt_question_input),
                    view.findViewById(R.id.lay_answers_container),
                    view.findViewById(R.id.lay_tags_container),
                    view.findViewById(R.id.btn_add_tag_to_question_suggestion),
                    view.findViewById(R.id.radio_easy),
                    view.findViewById(R.id.radio_medium),
                    view.findViewById(R.id.radio_advanced),
                    question, tags, wrong, correct, difficulty);

        } else {
            QuestionSuggestionActivity.PRESENTER.doEditPresentation(this,
                    view.findViewById(R.id.lay_answers_container),
                    view.findViewById(R.id.lay_tags_container),
                    view.findViewById(R.id.btn_add_tag_to_question_suggestion));
        }
        return view;
    }



}
