package com.example.mcs.interviewhelper.Model.Interactors;

import android.content.DialogInterface;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.mcs.interviewhelper.QuizActivity;
import com.example.mcs.interviewhelper.R;
import com.example.mcs.interviewhelper.View.QuizViews.QuizFragment;
import com.example.mcs.interviewhelper.View.QuizViews.StartQuizScreenFragment;
import com.example.mcs.interviewhelper.View.QuizViews.SubmittedQuizFragment;

import java.util.ArrayList;

import static com.example.mcs.interviewhelper.QuizActivity.NQUESTIONS;

/** @franco_trujillo
 * this class is the interactor base in charge of all the logic, backend calls, and business logic
 **/

public abstract class QuizInteractor {

    public abstract void setStartQuizListener(View view);
    public abstract void setConfigButtonListener(View view);
    public abstract void loadNextQuestion(View view);
    public abstract void loadLastQuestion(View view);
    public abstract void loadQuestionnaire(View view);
    public abstract void saveCurrentResponse(View view);
    public abstract View getAnswerView(View view);
    public abstract void submitResponses();
    public abstract ArrayList<String> getAnswerArray(int position);
    public abstract void resumeInteraction();

/** @franco_trujillo
 * load the next fragment on the work flow(start--quiz--submit)
 **/
    public void goToNextScreen(Fragment fragment) {
        if(fragment == null){
            StartQuizScreenFragment nextFragment = new StartQuizScreenFragment();
            QuizActivity.CONTEXT.startNextFragment(nextFragment);
            return;
        }
        if(fragment instanceof StartQuizScreenFragment){
            QuizFragment nextFragment = new QuizFragment();
            QuizActivity.CONTEXT.startNextFragment(nextFragment);
            return;
        }
        if(fragment instanceof QuizFragment){
            SubmittedQuizFragment nextFragment = new SubmittedQuizFragment();
            QuizActivity.CONTEXT.startNextFragment(nextFragment);
            return;
        }
    }

    public TextView getQuestionView(View view){
        return (TextView) view.findViewById(R.id.Question);
    }

    /** @franco_trujillo
     * set actions on the button listeners, and call to the alert dialog when needed
     **/
    public void setNavigationNextButtonListener(View view){
     view.setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View v) {
             View TopView =(View)v.getParent().getParent();
             QuizActivity.INTERACTOR.saveCurrentResponse(TopView);

             if(QuizActivity.CURRENTQUESTION < NQUESTIONS-1) {
                 QuizActivity.INTERACTOR.loadNextQuestion(TopView);
             } else {
                QuizActivity.INTERACTOR.createDialog();
             }
         }
     });
    }

    public void setNavigationBackButtonListener(View view){
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View TopView =(View)v.getParent().getParent();
                if(QuizActivity.CURRENTQUESTION > 0){
                    QuizActivity.INTERACTOR.loadLastQuestion(TopView);
                }
                QuizActivity.INTERACTOR.updateButtons(TopView);

            }
        });
    }

    /** @franco_trujillo
     * change button name between next and submit
     **/
    public void updateButtons(View view){
        Button next = (Button) view.findViewById(R.id.NextButton);
        if(QuizActivity.CURRENTQUESTION != QuizActivity.NQUESTIONS - 1) {
            QuizActivity.PRESENTER.changeSubmitForNext(next);
        }else if(QuizActivity.CURRENTQUESTION == QuizActivity.NQUESTIONS - 1){
            QuizActivity.PRESENTER.changeNextForSubmit(next);

        }
    }

    /** @franco_trujillo
     * create the alert dialog to confirm and submit
     *
     * @itzcoatl90
     *
     * Made changes to @franco_trujillo code, we are not waiting for the INTERACTOR.submitResponses anymore.
     * Here is not were goToNextScreen is called.
     *
     **/
    public void createDialog(){
        AlertDialog.Builder alertDialogBuilder =
                new AlertDialog.Builder(QuizActivity.CONTEXT);
        alertDialogBuilder.setMessage("Are you sure you want to submit");
        alertDialogBuilder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Invoke submitResponses, but not waiting for result.
                QuizActivity.CONTEXT.startProDialog();
                QuizActivity.INTERACTOR.submitResponses();
            }
        });
        alertDialogBuilder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                return;
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
    /** @franco_trujillo
     * remove buttons from the fragment
     **/
    public void deleteButtons(View view){
        View deletable = view.findViewById(R.id.ButtonsHolder);
        ((ViewGroup)deletable).removeAllViews();
    }

    /** @franco_trujillo
     * Calculate the the grade on your quiz on multiple-choice
     **/
    public int getGrade(){
        int score = 0;
        for (int i =0; i<QuizActivity.NQUESTIONS;i++){
            if(QuizActivity.RESPONSES.get(i) == QuizActivity.RIGHTRESPONSES.get(i)){
                score++;
            }
        }
        return score;

    }

}
