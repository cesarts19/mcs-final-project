package com.example.mcs.interviewhelper.Model.data;

import com.google.firebase.database.PropertyName;

import java.util.ArrayList;

/**
 * Contains the basic implementation for an open question
 *
 * @itzcoatl90
 *
 * Made some changes on @roberto.cervantes1988@gmail.com code...
 * id now is long for db matters.
 */

public class BasicQuestion {

    public static final String BASIC_PARCELABLE_NAME = "com.example.mcs.interviewhelper.BasicQuestion";

    // mQuestion, String having the question to be asked.
    protected String mQuestion;
    // mCreator, String having the creator of the question, usually consultant.
    protected String mCreator;
    // mValidator, String having the validator of the question, usually trainer.
    protected String mValidator;
    // mResponse, String for quiz usage, this holds the user's answer after a quiz.
    protected String mResponse;
    // mTags, Array of Strings having
    protected ArrayList<String> mTags;
    /* mDifficulty
    * 10 for basic questions
    * 20 for medium questions
    * 30 for advanced questions
    * */
    protected int mDifficulty;
    /* mStatus
    * 1 is for ready answers, validated, questions ready to be attach to a quiz.
    * 5 is for dismissed questions, validated by a trainer and dropped, not for quiz but just for the record.
    * 10 is for not validated questions suggestion, needs a trainer to change this to 5 or 1.
    * */
    protected int mStatus;
    // This is the id for the question and also the index on firebase.
    protected long mId;
    /* mCheckStatus
    * The current status of the response to the question:
    * 1 is for Correct Response.
    * 2 is for Wrong Response.
    * 3 is for Not Checked Response.
    * 0 is the default value(used for the constructor that receives a BasicQuestionWrapper).
    * */
    protected int mCheckedStatus;

    public BasicQuestion(String mQuestion, String mCreator,
                         String mValidator, String mResponse,
                         ArrayList<String> mTags, int mDifficulty,
                         int mStatus, long mId, int mCheckedStatus) {
        this.mQuestion = mQuestion;
        this.mCreator = mCreator;
        this.mValidator = mValidator;
        this.mResponse = mResponse;
        this.mTags = mTags;
        this.mDifficulty = mDifficulty;
        this.mStatus = mStatus;
        this.mId = mId;
        this.mCheckedStatus = mCheckedStatus;
    }

    public BasicQuestion(BasicQuestionWrapper input){
        this.mQuestion = input.question;
        this.mCreator = input.submmitedBy;
        this.mValidator = input.validatedBy;
        this.mResponse = "";
        this.mTags = input.Tags;
        this.mDifficulty = input.difficulty;
        this.mStatus = input.status;
        this.mId = input.id;
        this.mCheckedStatus = 0;
    }

    public BasicQuestion(QuestionInsiderWrapper questionInsiderWrapper){
        this.mQuestion = questionInsiderWrapper.Question;
        this.mCreator = "";
        this.mValidator = "";
        this.mResponse = questionInsiderWrapper.Answer;
        this.mTags = questionInsiderWrapper.Tags;
        this.mDifficulty = questionInsiderWrapper.Difficulty;
        this.mStatus = 0;
        this.mId = -1;
        this.mCheckedStatus = questionInsiderWrapper.Status;

    }

    public BasicQuestion() {
        this.mTags = new ArrayList<>();
    }

    @PropertyName("question")
    public String getmQuestion() {
        return mQuestion;
    }

    @PropertyName("question")
    public void setmQuestion(String mQuestion) {
        this.mQuestion = mQuestion;
    }

    @PropertyName("submmitedBy")
    public String getmCreator() {
        return mCreator;
    }

    @PropertyName("submmitedBy")
    public void setmCreator(String mCreator) {
        this.mCreator = mCreator;
    }

    @PropertyName("validatedBy")
    public String getmValidator() {
        return mValidator;
    }

    @PropertyName("validatedBy")
    public void setmValidator(String mValidator) {
        this.mValidator = mValidator;
    }

    public String getmResponse() {
        return mResponse;
    }

    public void setmResponse(String mResponse) {
        this.mResponse = mResponse;
    }

//    @PropertyName("Tags")
    public ArrayList<String> getmTags() {
        return mTags;
    }

//    @PropertyName("Tags")
    public void setmTags(ArrayList<String> mTags) {
        this.mTags = mTags;
    }

    @PropertyName("difficulty")
    public int getmDifficulty() {
        return mDifficulty;
    }

    @PropertyName("difficulty")
    public void setmDifficulty(int mDifficulty) {
        this.mDifficulty = mDifficulty;
    }

    @PropertyName("status")
    public int getmStatus() {
        return mStatus;
    }

    @PropertyName("status")
    public void setmStatus(int mStatus) {
        this.mStatus = mStatus;
    }

    @PropertyName("id")
    public long getmId() {
        return mId;
    }

    @PropertyName("id")
    public void setmId(long mId) {
        this.mId = mId;
    }

    public int getmCheckedStatus() {
        return mCheckedStatus;
    }

    public void setmCheckedStatus(int mCheckedStatus) {
        this.mCheckedStatus = mCheckedStatus;
    }

    @Override
    public String toString() {
        return "BasicQuestion{" +
                "mQuestion='" + mQuestion + '\'' +
                ", mCreator='" + mCreator + '\'' +
                ", mValidator='" + mValidator + '\'' +
                ", mResponse='" + mResponse + '\'' +
                ", mTags=" + mTags +
                ", mDifficulty=" + mDifficulty +
                ", mStatus=" + mStatus +
                ", mId=" + mId +
                ", mCheckedStatus=" + mCheckedStatus +
                '}';
    }
}
