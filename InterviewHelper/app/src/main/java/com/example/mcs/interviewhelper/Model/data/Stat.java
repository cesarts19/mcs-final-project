package com.example.mcs.interviewhelper.Model.data;

public class Stat {

    private String mTag;
    private int mRange;
    private int mCorrect;

    public Stat(String mTag, int mRange, int mCorrect) {
        this.mTag = mTag;
        this.mRange = mRange;
        this.mCorrect = mCorrect;
    }

    public String getmTag() {
        return mTag;
    }

    public void setmTag(String mTag) {
        this.mTag = mTag;
    }

    public int getmRange() {
        return mRange;
    }

    public void setmRange(int mRange) {
        this.mRange = mRange;
    }

    public int getmCorrect() {
        return mCorrect;
    }

    public void setmCorrect(int mCorrect) {
        this.mCorrect = mCorrect;
    }

    public void increaseRange(){
        mRange++;
    }

    public void increaseBoth(){
        mCorrect++;
        mRange++;
    }
    @Override
    public String toString() {
        return "Stat{" +
                "mTag='" + mTag + '\'' +
                ", mRange=" + mRange +
                ", mCorrect=" + mCorrect +
                '}';
    }
}
