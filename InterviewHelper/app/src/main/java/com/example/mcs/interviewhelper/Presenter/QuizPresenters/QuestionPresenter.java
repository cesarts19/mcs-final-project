package com.example.mcs.interviewhelper.Presenter.QuizPresenters;

import android.view.View;

import com.example.mcs.interviewhelper.View.QuizViews.QuizFragment;

/**franco_trujillo
 *QuestionPresenter interface
 */
public interface QuestionPresenter {

    void doAnswerLayout(QuizFragment fragment, View answerContainer, View NextButton, View BackButton);

    void presentNextQuestion(View view);

    void drawAnswer(View view, String Value);


}
