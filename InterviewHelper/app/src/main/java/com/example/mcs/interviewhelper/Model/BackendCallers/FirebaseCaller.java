package com.example.mcs.interviewhelper.Model.BackendCallers;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.example.mcs.interviewhelper.MainActivity;
import com.example.mcs.interviewhelper.Model.data.AnswerWrapper;
import com.example.mcs.interviewhelper.Model.data.BasicQuestion;
import com.example.mcs.interviewhelper.Model.data.BasicQuestionWrapper;
import com.example.mcs.interviewhelper.Model.data.Command;
import com.example.mcs.interviewhelper.Model.data.MultipleChoiceQuestion;
import com.example.mcs.interviewhelper.Model.data.MultipleQuestionWrapper;
import com.example.mcs.interviewhelper.Model.data.Quiz;
import com.example.mcs.interviewhelper.Model.data.QuizWrapper;
import com.example.mcs.interviewhelper.QuizActivity;
import com.example.mcs.interviewhelper.R;
import com.example.mcs.interviewhelper.View.Suggestions.QuestionListScreenFragment;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import static android.R.string.ok;

/**
 *
 * @Carloscastes
 *
 * In charge of retrieving the question data from the Firebase real time database and parsing it
 * to the proper question objects.
 *
 * @itzcoatl90
 *
 * Make implementation on sendQuestion(BasicQuestion) and sendQuestion(MultipleChoiceQuestion),
 * Make changes on constructor and attributes of the class.
 *
 */

public class FirebaseCaller implements BackendCaller {
    private final String OPEN_QUESTION_CHILDREN_REFERENCE = "BasicQuestions";
    private final String INDEXES_CHILDREN_REFERENCE = "Indexes";
    private final String MULTIPLE_QUESTION_CHILDREN_REFERENCE = "MultipleChoiceQuestions";
    private final String CONSULTANT_CHILDREN_REFERENCE = "Consultants";
    private final String TRAINER_CHILDREN_REFERENCE = "Trainers";
    private final String RESPONSES_CHILDREN_REFERENCE = "Responses";
    private final String TAGS_CHILDREN_REFERENCE = "Tags";
    private final String SUGGESTION_CHILDREN_REFERENCE = "QuestionSuggestion";
    private final String INDEX_MUTEX_LOCK_FLAG = "Mutex_Lock_Flag";
    private final String QUIZ_INTERVIEW_REFERENCE = "Interviews";
    private final String QUIZ_TRAININGS_REFERENCE = "Trainings";
    //    Tag for debug logs

    private FirebaseDatabase database;
    private DatabaseReference openQuestionTableReference;
    private DatabaseReference multipleQuestionTableReference;
    private DatabaseReference answerTableReference;
    private DatabaseReference tagsTableReference;
    private DatabaseReference indexTableReference;
    private DatabaseReference consultantTableReference;
    private DatabaseReference trainerTableReference;

    public FirebaseCaller() {
        database = FirebaseDatabase.getInstance();
        openQuestionTableReference = database.getReference(OPEN_QUESTION_CHILDREN_REFERENCE);
        multipleQuestionTableReference = database.getReference(MULTIPLE_QUESTION_CHILDREN_REFERENCE);
        answerTableReference = database.getReference(RESPONSES_CHILDREN_REFERENCE);
        tagsTableReference = database.getReference(TAGS_CHILDREN_REFERENCE);
        indexTableReference = database.getReference(INDEXES_CHILDREN_REFERENCE);
        consultantTableReference = database.getReference(CONSULTANT_CHILDREN_REFERENCE);
        trainerTableReference = database.getReference(TRAINER_CHILDREN_REFERENCE);
    }

    /**
     * @itzcoatl90
     *
     * This methods sendInterviewQuiz and sendTrainingQuiz, are for storing on db the quiz on db.
     * requestBasicQuestions and requestMultipleChoiceQuestion are for retrieving a set
     * of random questions with the specifications from the Backend.
     *
     * */

    @Override
    public void receiveBasicQuestions(final int questionQuantity,
                                      final int difficultyLevel,
                                      final ArrayList<String> topicTags,
                                      final Context mContext,
                                      final int activity_index,
                                      final Command mCommand) {
        database.getReference(TAGS_CHILDREN_REFERENCE).addListenerForSingleValueEvent(
                new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ArrayList<Long> ids = new ArrayList<Long>();
                GenericTypeIndicator<HashMap<String,Boolean>> t = new GenericTypeIndicator<HashMap<String,Boolean>>() {};
                GenericTypeIndicator<ArrayList<Boolean>> d = new GenericTypeIndicator<ArrayList<Boolean>>() {};
                for (int i = 0; i < topicTags.size(); i++) {
                    try {
                        HashMap<String,Boolean> aux = dataSnapshot
                                .child(topicTags.get(i))
                                .child(OPEN_QUESTION_CHILDREN_REFERENCE)
                                .child(""+difficultyLevel)
                                .getValue(t);
                        if(aux == null) {
                            continue;
                        }
                        Object[] stringsIds = aux.keySet().toArray();
                        for (int j = 0; j < stringsIds.length; j++) {
                            ids.add(new Long(""+ stringsIds[j]));
                        }
                    } catch(com.google.firebase.database.DatabaseException e) {
                        Log.e(this.getClass().getSimpleName(),"Datatype failed as Map, trying as Array.\n" + e.getMessage());
                        ArrayList<Boolean> aux = dataSnapshot
                                .child(topicTags.get(i))
                                .child(OPEN_QUESTION_CHILDREN_REFERENCE)
                                .child(""+difficultyLevel)
                                .getValue(d);
                        for (int j = 0; j < aux.size(); j++) {
                            ids.add(new Long(j));
                        }
                    }
                }
                Random r = new Random();
                // Auto complete (less than needed)
                if(ids.size() < questionQuantity){
                    ArrayList<Long> low_priority = new ArrayList<Long>();
                    GenericTypeIndicator<HashMap<String,Object>> u =
                            new GenericTypeIndicator<HashMap<String,Object>>() {};
                    // Filling other low_priority List with question on other difficulties same tags.
                    for (int i = 0; i < topicTags.size(); i++) {
                            HashMap<String,Object> aux =
                                    dataSnapshot
                                            .child(topicTags.get(i))
                                            .child(OPEN_QUESTION_CHILDREN_REFERENCE)
                                            .getValue(u);
                        if(aux != null) {
                            for (String value : aux.keySet()) {
                                if (!value.equals("" + difficultyLevel)) {
                                    Object temp = aux.get(value);
                                    if (temp instanceof ArrayList) {
                                        for (int j = 0; j < ((ArrayList) aux.get(value)).size(); j++) {
                                            ids.add(new Long(j));
                                        }
                                    } else {
                                        Object[] stringsIds = ((HashMap) aux.get(value)).keySet().toArray();
                                        for (int j = 0; j < stringsIds.length; j++) {
                                            ids.add(new Long("" + stringsIds[j]));
                                        }
                                    }
                                }
                            }
                        }
                    }
                    // No question at all, send error.
                    if(ids.size() + low_priority.size() == 0) {
                        Toast.makeText(mContext,R.string.no_questions_specified,Toast.LENGTH_SHORT).show();
                        mCommand.executeOnReceiveFail();
                        return;
                    }
                    // Less question than asked? Fill low_priority with repeated questions.
                    while(ids.size() + low_priority.size() < questionQuantity) {
                        low_priority.add(ids.get(r.nextInt(ids.size())));
                    }
                    //Let's add data to the ids List.
                    while(ids.size() < questionQuantity){
                        ids.add(low_priority.get(r.nextInt(low_priority.size())));
                    }
                }
                // Discarding leftovers (more than needed)
                while (ids.size() > questionQuantity){
                    ids.remove(r.nextInt(ids.size()));
                }
                // ids is meant to have the quantity of longs requiring, just need to bind question.
                getQuestionsFromDB(database.getReference().child(OPEN_QUESTION_CHILDREN_REFERENCE)
                        ,ids,false,-1,mContext,activity_index,mCommand);
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(getClass().getSimpleName(),databaseError.getMessage());
                Toast.makeText(mContext,R.string.event_cancel,Toast.LENGTH_SHORT).show();
                mCommand.executeOnReceiveFail();
            }
        });
    }

    @Override
    public void receiveMultipleChoiceQuestions(final int questionQuantity,
                                               final int difficultyLevel,
                                               final ArrayList<String> topicTags,
                                               final int maxQuantityAnswers,
                                               final Context mContext,
                                               final int activity_index,
                                               final Command mCommand) {
        database.getReference(TAGS_CHILDREN_REFERENCE).addListenerForSingleValueEvent(
                new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ArrayList<Long> ids = new ArrayList<Long>();
                GenericTypeIndicator<HashMap<String,Boolean>> t = new GenericTypeIndicator<HashMap<String,Boolean>>() {};
                GenericTypeIndicator<ArrayList<Boolean>> d = new GenericTypeIndicator<ArrayList<Boolean>>() {};
                for (int i = 0; i < topicTags.size(); i++) {
                    try {
                        HashMap<String,Boolean> aux = dataSnapshot
                                .child(topicTags.get(i))
                                .child(MULTIPLE_QUESTION_CHILDREN_REFERENCE)
                                .child(""+difficultyLevel)
                                .getValue(t);
                        if(aux == null) {
                            continue;
                        }
                        Object[] stringsIds = aux.keySet().toArray();
                        for (int j = 0; j < stringsIds.length; j++) {
                            ids.add(new Long(""+ stringsIds[j]));
                        }
                    } catch(com.google.firebase.database.DatabaseException e) {
                        Log.e(this.getClass().getSimpleName(),"Datatype failed as Map, trying as Array.\n" + e.getMessage());
                        ArrayList<Boolean> aux = dataSnapshot
                                .child(topicTags.get(i))
                                .child(MULTIPLE_QUESTION_CHILDREN_REFERENCE)
                                .child(""+difficultyLevel)
                                .getValue(d);
                        for (int j = 0; j < aux.size(); j++) {
                            ids.add(new Long(j));
                        }
                    }
                }
                Random r = new Random();
                // Auto complete (less than needed)
                if(ids.size() < questionQuantity){
                    ArrayList<Long> low_priority = new ArrayList<Long>();
                    GenericTypeIndicator<HashMap<String,Object>> u =
                            new GenericTypeIndicator<HashMap<String,Object>>() {};
                    // Filling other low_priority List with question on other difficulties same tags.
                    for (int i = 0; i < topicTags.size(); i++) {
                        HashMap<String,Object> aux =
                                dataSnapshot
                                        .child(topicTags.get(i))
                                        .child(MULTIPLE_QUESTION_CHILDREN_REFERENCE)
                                        .getValue(u);
                        if(aux != null){
                            for(String value : aux.keySet()){
                                if(!value.equals("" + difficultyLevel)){
                                    Object temp = aux.get(value);
                                    if(temp instanceof ArrayList) {
                                        for (int j = 0; j < ((ArrayList) aux.get(value)).size(); j++) {
                                            low_priority.add(new Long(j));
                                        }
                                    } else {
                                        Object[] stringsIds = ((HashMap) aux.get(value)).keySet().toArray();
                                        for (int j = 0; j < stringsIds.length; j++) {
                                            low_priority.add(new Long(""+ stringsIds[j]));
                                        }
                                    }
                                }
                            }
                        }
                    }
                    // No question at all, send error.
                    if(ids.size() + low_priority.size() == 0) {
                        Toast.makeText(mContext,R.string.no_questions_specified,Toast.LENGTH_SHORT).show();
                        mCommand.executeOnReceiveFail();
                        return;
                    }
                    // Less question than asked? Fill low_priority with repeated questions.
                    while(ids.size() + low_priority.size() < questionQuantity) {
                        low_priority.add(ids.get(r.nextInt(ids.size())));
                    }
                    //Let's add data to the ids List.
                    while(ids.size() < questionQuantity){
                        ids.add(low_priority.get(r.nextInt(low_priority.size())));
                    }
                }
                // Discarding leftovers (more than needed)
                while (ids.size() > questionQuantity){
                    ids.remove(r.nextInt(ids.size()));
                }
                // ids is meant to have the quantity of longs requiring, just need to bind question.
                getQuestionsFromDB(database.getReference().child(MULTIPLE_QUESTION_CHILDREN_REFERENCE)
                                ,ids,true,maxQuantityAnswers,mContext,activity_index,mCommand);
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(getClass().getSimpleName(),databaseError.getMessage());
                Toast.makeText(mContext,R.string.event_cancel,Toast.LENGTH_SHORT).show();
                mCommand.executeOnReceiveFail();
            }
        });
    }

    @Override
    public void sendInterviewQuiz(Quiz quiz,final Context mContext,final Command mCommand) {
        final QuizWrapper quizWrapper = new QuizWrapper(quiz);
        consultantTableReference
                .child(quizWrapper.RespondedBy)
                .child(QUIZ_INTERVIEW_REFERENCE)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        GenericTypeIndicator<ArrayList<QuizWrapper>> t =
                                new GenericTypeIndicator<ArrayList<QuizWrapper>>() {};
                        ArrayList<QuizWrapper> quizes = dataSnapshot.getValue(t);
                        if(quizes == null){
                            quizes = new ArrayList<QuizWrapper>();
                        }
                        quizes.add(quizWrapper);
                        consultantTableReference
                                .child(quizWrapper.RespondedBy)
                                .child(QUIZ_INTERVIEW_REFERENCE)
                                .setValue(quizes);
                        mCommand.executeOnSendSuccessful();
                    }
                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.e(getClass().getSimpleName(),databaseError.getMessage());
                        Toast.makeText(mContext,R.string.event_cancel,Toast.LENGTH_SHORT).show();
                        mCommand.executeOnSendFail();
                    }
                });
    }

    @Override
    public void sendTrainingQuiz(final Quiz quiz,final Context mContext,final Command mCommand) {
        final QuizWrapper quizWrapper = new QuizWrapper(quiz);
        consultantTableReference
                .child(quizWrapper.RespondedBy)
                .child(QUIZ_TRAININGS_REFERENCE)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        GenericTypeIndicator<ArrayList<QuizWrapper>> t =
                                new GenericTypeIndicator<ArrayList<QuizWrapper>>() {};
                        ArrayList<QuizWrapper> quizes = dataSnapshot.getValue(t);
                        if(quizes == null){
                            quizes = new ArrayList<QuizWrapper>();
                        }
                        quizes.add(quizWrapper);
                        consultantTableReference
                                .child(quizWrapper.RespondedBy)
                                .child(QUIZ_TRAININGS_REFERENCE)
                                .setValue(quizes);
                        mCommand.executeOnSendSuccessful();
                    }
                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.e(getClass().getSimpleName(),databaseError.getMessage());
                        Toast.makeText(mContext,R.string.event_cancel,Toast.LENGTH_SHORT).show();
                        mCommand.executeOnSendFail();
                    }
                });
    }

    /**
     * @itzcoatl90
     *
     * This methods sendQuestionSuggestion(BasicQuestion) and sendQuestionSuggestion(MultipleChoiceQuestion)
     * are for storing questionSuggestion on QuestionSuggestionActivity, receiveBasicQuestionSuggestions and
     * receiveMultipleChoiceQuestionSuggestions are for listing suggestion previously stored, usually the trainer.
     *
     * */

    @Override
    public boolean sendQuestionSuggestion(final BasicQuestion question,
                                          final Context mContext,
                                          final Command mCommand) {
        indexTableReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                // Check if somebody else is doing any operation.
                boolean editFlag = (boolean) snapshot.child(INDEX_MUTEX_LOCK_FLAG).getValue();
                if(editFlag) {
                    Toast.makeText(mContext, R.string.network_error, Toast.LENGTH_SHORT).show();
                    mCommand.executeOnSendFail();
                    return;
                }
                // Lock the db for preventing everybody to write while this is happening
                indexTableReference.child(INDEX_MUTEX_LOCK_FLAG).setValue(true);
                try {
                    long index = (long) snapshot.child(OPEN_QUESTION_CHILDREN_REFERENCE).getValue();
                    // Check if the question is not yet on the db, if it doesn't you need a new id.
                    boolean setNewIndexQuestion = false;
                    if(question.getmId() == -1) {
                        question.setmId(index);
                        index++;
                        // As it was required a new id, new length of the question array.
                        setNewIndexQuestion = true;
                        // As it is open question, no answers needed to be saved. Before ending, add to QuestionsSuggestions.
                        questionSuggestion(
                                database.getReference(SUGGESTION_CHILDREN_REFERENCE)
                                        .child(OPEN_QUESTION_CHILDREN_REFERENCE),
                                question.getmId());
                    }
                    // Let's check if we need to put the question into the tags pool
                    if(question.getmStatus() == 1) {
                        for (int i = 0; i < question.getmTags().size(); i++) {
                            tagsTableReference
                                    .child(question.getmTags().get(i))
                                    .child(OPEN_QUESTION_CHILDREN_REFERENCE)
                                    .child("" +question.getmDifficulty())
                                    .child("" + question.getmId())
                                        .setValue(true);
                        }
                    }
                    // If the question is validated or dismiss, it should be deleted from questionSuggestion.
                    if(question.getmStatus() == 1 || question.getmStatus() == 5) {
                        deleteQuestionSuggestion(
                                database.getReference(SUGGESTION_CHILDREN_REFERENCE).child(OPEN_QUESTION_CHILDREN_REFERENCE),
                                question.getmId());
                    }
                    // All set ups, let's write into db.
                    BasicQuestionWrapper questionWrapper = new BasicQuestionWrapper(question);
                    openQuestionTableReference.child("" + (question.getmId())).setValue(questionWrapper);
                    if(setNewIndexQuestion){
                        // We are doing this here because we need to ensure the attempt on the question was successful.
                        indexTableReference.child(OPEN_QUESTION_CHILDREN_REFERENCE).setValue(index);
                    }
                    // If everything went fine, invoke the onSuccess execute.
                    mCommand.executeOnSendSuccessful();
                } catch(Exception e) {
                    // If an exception occurred, invoke the onFail execute.
                    Toast.makeText(mContext,R.string.exeption_message,Toast.LENGTH_SHORT).show();
                    mCommand.executeOnSendFail();
                    e.printStackTrace();
                    Log.e(FirebaseCaller.this.getClass().getSimpleName(),e.getMessage());
                }
                // Regardless of what happens, free the lock on db.
                indexTableReference.child(INDEX_MUTEX_LOCK_FLAG).setValue(false);
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                // If the task is cancelled, free the lock on db.
                Log.e(getClass().getSimpleName(),databaseError.getMessage());
                Toast.makeText(mContext,R.string.event_cancel,Toast.LENGTH_SHORT).show();
                mCommand.executeOnSendFail();
                indexTableReference.child(INDEX_MUTEX_LOCK_FLAG).setValue(false);
            }
        });
        return true;
    }

    @Override
    public boolean sendQuestionSuggestion(final MultipleChoiceQuestion question,
                                          final Context mContext,
                                          final Command mCommand) {
        indexTableReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                // Check if somebody else is doing any operation.
                boolean editFlag = (boolean) snapshot.child(INDEX_MUTEX_LOCK_FLAG).getValue();
                if(editFlag) {
                    Toast.makeText(mContext, R.string.network_error, Toast.LENGTH_SHORT).show();
                    mCommand.executeOnSendFail();
                    return;
                }
                // Lock the db for preventing everybody to write while this is happening
                indexTableReference.child(INDEX_MUTEX_LOCK_FLAG).setValue(true);
                try {
                    long index = (long) snapshot.child(MULTIPLE_QUESTION_CHILDREN_REFERENCE).getValue();
                    long answerIndex = (long) snapshot.child(RESPONSES_CHILDREN_REFERENCE).getValue();
                    // Check if the question is not yet on the db, if it doesn't you need a new id.
                    boolean setNewIndexQuestion = false;
                    if(question.getmId() == -1){
                        question.setmId(index);
                        index++;
                        // As it was required a new id, new length of the question array.
                        setNewIndexQuestion = true;
                        // new Multiple choice questions also need to save the answers
                        for (int i = 0; i < question.getmRightAnswer().size(); i++) {
                            AnswerWrapper aux = new AnswerWrapper(answerIndex,question.getmRightAnswer().get(i));
                            answerTableReference.child("" + answerIndex).setValue(aux);
                            question.getmCorrectAnswerIndexes().add(answerIndex);
                            answerIndex++;
                        }
                        for (int i = 0; i < question.getmWrongAnswer().size(); i++) {
                            AnswerWrapper aux = new AnswerWrapper(answerIndex,question.getmWrongAnswer().get(i));
                            answerTableReference.child("" + answerIndex).setValue(aux);
                            question.getmWrongAnswerIndexes().add(answerIndex);
                            answerIndex++;
                        }
                        // Before ending, add to QuestionsSuggestions.
                        questionSuggestion(
                                database.getReference(SUGGESTION_CHILDREN_REFERENCE).child(MULTIPLE_QUESTION_CHILDREN_REFERENCE),
                                question.getmId());
                    }
                    // If is not new, but re-saved
                    else if(question.getmStatus() == 10){
                        // Multiple choice questions re save the answers
                        for (int i = 0; i < question.getmRightAnswer().size(); i++) {
                            AnswerWrapper aux =
                                    new AnswerWrapper(question.getmCorrectAnswerIndexes().get(i),
                                            question.getmRightAnswer().get(i));
                            answerTableReference.child("" + question.getmCorrectAnswerIndexes().get(i))
                                    .setValue(aux);
                        }
                        for (int i = 0; i < question.getmWrongAnswer().size(); i++) {
                            AnswerWrapper aux =
                                    new AnswerWrapper(question.getmWrongAnswerIndexes().get(i),
                                            question.getmWrongAnswer().get(i));
                            answerTableReference.child("" + question.getmWrongAnswerIndexes().get(i))
                                    .setValue(aux);
                        }
                    }
                    //Let's check if we need to put the question into the tags pool
                    if(question.getmStatus() == 1) {
                        for (int i = 0; i < question.getmTags().size(); i++) {
                            tagsTableReference
                                    .child(question.getmTags().get(i))
                                    .child(MULTIPLE_QUESTION_CHILDREN_REFERENCE)
                                    .child("" + question.getmDifficulty())
                                    .child("" + question.getmId())
                                        .setValue(true);
                        }
                    }
                    // If the question is validated or dismiss, it should be deleted from questionSuggestion.
                    if(question.getmStatus() == 1 || question.getmStatus() == 5) {
                        deleteQuestionSuggestion(
                                database.getReference(SUGGESTION_CHILDREN_REFERENCE).child(MULTIPLE_QUESTION_CHILDREN_REFERENCE),
                                question.getmId());
                    }
                    // All set ups, let's write into db.
                    MultipleQuestionWrapper questionWrapper = new MultipleQuestionWrapper(question);
                    multipleQuestionTableReference.child("" + (question.getmId())).setValue(questionWrapper);
                    if(setNewIndexQuestion){
                        // As answers where saved, new length of the answers array.
                        indexTableReference.child(RESPONSES_CHILDREN_REFERENCE).setValue(answerIndex);
                        // We are doing this here because we need to ensure the attempt on the question was successful.
                        indexTableReference.child(MULTIPLE_QUESTION_CHILDREN_REFERENCE).setValue(index);
                    }
                    // If everything went fine, invoke the onSuccess execute.
                    mCommand.executeOnSendSuccessful();
                } catch(Exception e){
                    // If an exception occurred, invoke the onFail execute.
                    mCommand.executeOnSendFail();
                    e.printStackTrace();
                    Log.e(FirebaseCaller.this.getClass().getSimpleName(),e.getMessage());
                    Toast.makeText(mContext,R.string.exeption_message,Toast.LENGTH_SHORT).show();
                }
                // Regardless of what happens, free the lock on db.
                indexTableReference.child(INDEX_MUTEX_LOCK_FLAG).setValue(false);
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                // If the task is cancelled, free the lock on db.
                Log.e(getClass().getSimpleName(),databaseError.getMessage());
                Toast.makeText(mContext,R.string.event_cancel,Toast.LENGTH_SHORT).show();
                indexTableReference.child(INDEX_MUTEX_LOCK_FLAG).setValue(false);
                mCommand.executeOnSendFail();
            }
        });
        return true;
    }

    @Override
    public void receiveBasicQuestionSuggestions(final int maxQuantityAnswers,
                                                final Context mContext,
                                                final int activity_index,
                                                final Command mCommand) {
        database.getReference(SUGGESTION_CHILDREN_REFERENCE)
                .child(OPEN_QUESTION_CHILDREN_REFERENCE).addListenerForSingleValueEvent(
                        new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ArrayList<Long> ids = null;
                try {
                    GenericTypeIndicator<ArrayList<Long>> t = new GenericTypeIndicator<ArrayList<Long>>() {};
                    ids = dataSnapshot.getValue(t);
                    if(ids == null){
                        Toast.makeText(mContext,R.string.no_content,
                                Toast.LENGTH_SHORT).show();
                        mCommand.executeOnReceiveFail();
                    } else {
                        getQuestionsFromDB(
                                database.getReference(OPEN_QUESTION_CHILDREN_REFERENCE),
                                ids,false,maxQuantityAnswers,mContext,activity_index,mCommand);
                    }
                } catch(Exception e){
                    e.printStackTrace();
                    Log.e(getClass().getSimpleName(),e.getMessage());
                    Toast.makeText(mContext,R.string.exeption_message,
                            Toast.LENGTH_SHORT).show();
                    mCommand.executeOnReceiveFail();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // If the task is cancelled, free the lock on db.
                Log.e(getClass().getSimpleName(),databaseError.getMessage());
                Toast.makeText(mContext,R.string.event_cancel,Toast.LENGTH_SHORT).show();
                indexTableReference.child(INDEX_MUTEX_LOCK_FLAG).setValue(false);
                mCommand.executeOnReceiveFail();
            }
        });
    }

    @Override
    public void receiveMultipleChoiceQuestionSuggestions(final int maxQuantityAnswers,
                                                         final Context mContext,
                                                         final int activity_index,
                                                         final Command mCommand) {
        database.getReference(SUGGESTION_CHILDREN_REFERENCE)
                .child(MULTIPLE_QUESTION_CHILDREN_REFERENCE).addListenerForSingleValueEvent(
                        new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        ArrayList<Long> ids = null;
                        try {
                            GenericTypeIndicator<ArrayList<Long>> t = new GenericTypeIndicator<ArrayList<Long>>() {};
                            ids = dataSnapshot.getValue(t);
                            if(ids == null){
                                Toast.makeText(mContext,R.string.no_content,
                                        Toast.LENGTH_SHORT).show();
                                mCommand.executeOnReceiveFail();
                            } else {
                                getQuestionsFromDB(
                                        database.getReference(MULTIPLE_QUESTION_CHILDREN_REFERENCE),
                                        ids,true,maxQuantityAnswers,mContext,activity_index,mCommand);
                            }
                        } catch(Exception e){
                            e.printStackTrace();
                            Log.e(getClass().getSimpleName(),e.getMessage());
                            Toast.makeText(mContext,R.string.exeption_message,
                                    Toast.LENGTH_SHORT).show();
                            mCommand.executeOnReceiveFail();
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        // If the task is cancelled, free the lock on db.
                        Log.e(getClass().getSimpleName(),databaseError.getMessage());
                        Toast.makeText(mContext,R.string.event_cancel,Toast.LENGTH_SHORT).show();
                        indexTableReference.child(INDEX_MUTEX_LOCK_FLAG).setValue(false);
                        mCommand.executeOnReceiveFail();
                    }
        });
    }

    /**
     * @itzcoatl90
     *
     * This methods are for multiple uses.
     *
     * */

    private void questionSuggestion(final DatabaseReference reference,final long id){
        reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ArrayList<Long> ids = null;
                try {
                    GenericTypeIndicator<ArrayList<Long>> t = new GenericTypeIndicator<ArrayList<Long>>() {};
                    ids = dataSnapshot.getValue(t);
                    if(ids == null){
                        ids = new ArrayList<Long>();
                        ids.add(id);
                    } else {
                        ids.add(id);
                    }
                } catch(Exception e){
                    e.printStackTrace();
                    Log.e(getClass().getSimpleName(),e.getMessage());
                    //Let's suppose that there's nothing on db.
                    ids = new ArrayList<Long>();
                    ids.add(id);
                }
                reference.setValue(ids);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                //For this one, do nothing
                Log.e(getClass().getSimpleName(),databaseError.getMessage());
            }
        });
    }

    private void deleteQuestionSuggestion(final DatabaseReference reference,final long id){
        reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ArrayList<Long> ids = null;
                try {
                    GenericTypeIndicator<ArrayList<Long>> t = new GenericTypeIndicator<ArrayList<Long>>() {};
                    ids = dataSnapshot.getValue(t);
                    if(ids == null){
                        ids = new ArrayList<Long>();
                    } else {
                        for (int i = 0; i < ids.size(); i++) {
                            if(ids.get(i) == id){
                                ids.remove(i);
                                break;
                            }
                        }
                    }
                } catch(Exception e){
                    e.printStackTrace();
                    Log.e(getClass().getSimpleName(),e.getMessage());
                    //Let's suppose that there's nothing on db.
                    ids = new ArrayList<Long>();
                }
                reference.setValue(ids);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                //For this one, do nothing
                Log.e(getClass().getSimpleName(),databaseError.getMessage());
            }
        });
    }

    private void getQuestionsFromDB(DatabaseReference reference,
                                    final ArrayList<Long> ids,
                                    final boolean multiple,
                                    final int cutAnswers,
                                    final Context mContext,
                                    final int activity_index,
                                    final Command mCommand){
        final ArrayList<BasicQuestion> output = new ArrayList<BasicQuestion>();
        reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (int i = 0; i < ids.size(); i++) {
                    if(multiple){
                        MultipleQuestionWrapper aux =
                                dataSnapshot.child("" + ids.get(i)).getValue(MultipleQuestionWrapper.class);
                        output.add(new MultipleChoiceQuestion(aux));
                    } else {
                        BasicQuestionWrapper aux =
                                dataSnapshot.child("" + ids.get(i)).getValue(BasicQuestionWrapper.class);
                        output.add(new BasicQuestion(aux));
                    }
                }
                if(multiple){
                    bindAnswers(output,cutAnswers,mContext,activity_index,mCommand);
                } else {
                    switch(activity_index){
                        case 0:
                            QuestionListScreenFragment.BASIC_SUGGESTIONS = output;
                            break;
                        case 1:
                            QuizActivity.QUIZ2SHOW = output;
                            break;
                        default:
                            break;
                    }
                    mCommand.executeOnReceiveSuccessful();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(getClass().getSimpleName(),databaseError.getMessage());
                Toast.makeText(mContext,R.string.event_cancel,Toast.LENGTH_SHORT).show();
                indexTableReference.child(INDEX_MUTEX_LOCK_FLAG).setValue(false);
                mCommand.executeOnReceiveFail();
            }
        });
    }

    private void bindAnswers(final ArrayList<BasicQuestion> input,
                             final int maxQuantityAnswers,
                            final Context mContext,
                            final int activity_index,
                            final Command mCommand){
        database.getReference(RESPONSES_CHILDREN_REFERENCE).addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for (int i = 0; i < input.size(); i++) {
                            MultipleChoiceQuestion aux = (MultipleChoiceQuestion) input.get(i);
                            for (int j = 0; j < aux.getmCorrectAnswerIndexes().size(); j++) {
                                aux.getmRightAnswer().add(
                                        (dataSnapshot.child("" + aux.getmCorrectAnswerIndexes().get(j)).getValue(AnswerWrapper.class)).value
                                );
                            }
                            for (int j = 0; j < aux.getmWrongAnswerIndexes().size(); j++) {
                                aux.getmWrongAnswer().add(
                                        (dataSnapshot.child("" + aux.getmWrongAnswerIndexes().get(j)).getValue(AnswerWrapper.class)).value
                                );
                            }
                        }
                        if(maxQuantityAnswers > 4) {
                            Random r = new Random();
                            for (int i = 0; i < input.size(); i++) {
                                while(((MultipleChoiceQuestion)input.get(i))
                                        .getmWrongAnswer().size() + 1 > maxQuantityAnswers) {
                                    int toRemove = r.nextInt(((MultipleChoiceQuestion)input.get(i))
                                            .getmWrongAnswer().size());
                                    ((MultipleChoiceQuestion)input.get(i))
                                            .getmWrongAnswer().remove(toRemove);
                                    ((MultipleChoiceQuestion)input.get(i))
                                            .getmWrongAnswerIndexes().remove(toRemove);
                                }
                            }
                        }
                        switch(activity_index) {
                            case 0:
                                QuestionListScreenFragment.BASIC_SUGGESTIONS = input;
                                break;
                            case 1:
                                QuizActivity.QUIZ2SHOW = input;
                                break;
                            default:
                                break;
                        }
                        mCommand.executeOnReceiveSuccessful();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.e(getClass().getSimpleName(),databaseError.getMessage());
                        Toast.makeText(mContext,R.string.event_cancel,Toast.LENGTH_SHORT).show();
                        indexTableReference.child(INDEX_MUTEX_LOCK_FLAG).setValue(false);
                        mCommand.executeOnReceiveFail();
                    }
                }
        );


    }

    @Override
    public void receiveConsultantsInfo(final Context context,final Command mCommand) {
        consultantTableReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try{
                    GenericTypeIndicator<HashMap<String,HashMap<String,ArrayList<QuizWrapper>>>> t =
                            new GenericTypeIndicator<HashMap<String,HashMap<String,ArrayList<QuizWrapper>>>>() {};
                    HashMap<String,HashMap<String,ArrayList<QuizWrapper>>> data =
                            dataSnapshot.getValue(t);//calculate all
                    ArrayList<Quiz> output = new ArrayList<Quiz>();
                    for(String key : data.keySet()){
                        HashMap<String,ArrayList<QuizWrapper>> temporal = data.get(key);

                        for (String keyInsider : temporal.keySet()) {
                            ArrayList<QuizWrapper> temp_array_quizes = temporal.get(keyInsider);

                            for (int i = 0; i < temp_array_quizes.size(); i++) {
                                temp_array_quizes.get(i);
                                output.add(new Quiz(temp_array_quizes.get(i)));
                            }
                        }
                    }
                    MainActivity.QUIZES.addAll(output);
                    mCommand.executeOnReceiveSuccessful();
                } catch (Exception e){
                    //TODO manda un mensaje
                    e.printStackTrace();
                    Log.e(getClass().getSimpleName(),e.getMessage());
                    mCommand.executeOnReceiveFail();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }

    @Override
    public void requestTags(final ArrayList<String> tagsArray, final Command command) {
        if(tagsArray != null && command != tagsArray) {
            tagsTableReference.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    tagsArray.clear();
                    for (DataSnapshot tag :
                            dataSnapshot.getChildren()) {
                        tagsArray.add(tag.getKey());
                    }
                    command.executeOnReceiveSuccessful();
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    command.executeOnReceiveFail();
                }
            });
        }
    }

    /*private ArrayList<BasicQuestion> filteredBasicQuestions;
    private ArrayList<MultipleChoiceQuestion> filteredMultipleChoiceQuestions;

    //private ArrayList<BackendCallerListener> listeners;

    private GenericTypeIndicator<ArrayList<HashMap<String, String>>> arrayTypeIndicator =
            new GenericTypeIndicator<ArrayList<HashMap<String, String>>>() {
            };* /

    / **
     * Method for subscribing all listeners interested in the Firebase database responses*/
    /*public void subscribeListener(BackendCallerListener backendCallerListener) {
        listeners.add(backendCallerListener);

    }*/

/*public FirebaseCaller() {
        database = FirebaseDatabase.getInstance();
        answerTableReference = database.getReference(ANSWER_CHILDREN_REFERENCE);

        filteredBasicQuestions = new ArrayList<>();
        filteredMultipleChoiceQuestions = new ArrayList<>();
        listeners = new ArrayList<>();
    }*/
  /*
//    TODO: Remove once this method isn't referenced any longer
    @Override
    public ArrayList<BasicQuestion> getBasicQuestions(int questionQuantity,
                                                      int difficultyLevel,
                                                      final ArrayList<String> topicTags) {
        return null;
    }

//    TODO: Remove once this method isn't referenced any longer
    @Override
    public ArrayList<MultipleChoiceQuestion> getMultipleChoiceQuestions(int questionQuantity,
                                                                        final int difficultyLevel,
                                                                        final ArrayList<String> topicTags,
                                                                        int responseQuantity) {


        return null;
    }
    */


    //AsyncTask for retrieving the answers needed for completing the MultipleChoiceQuestion object
    /*private class AnswerRetrievalTask extends AsyncTask<
            Integer, Void, Integer> {

        @Override
        protected Integer doInBackground(Integer... params) {
            for (MultipleChoiceQuestion question :
                    filteredMultipleChoiceQuestions) {
//                Selecting a correct answer
                if (question.getmCorrectAnswerIndexes().size()>1){

                }
                else {

                }
            }

//            Passing the second parameter which contains the number of questions requested
            return params[1];
        }

        @Override
        protected void onPostExecute(Integer numberOfQuestionsRequired) {
            super.onPostExecute(numberOfQuestionsRequired);
            // Sending the array of filtered results to all listeners
            for (BackendCallerListener listener : listeners) {
                if (numberOfQuestionsRequired < filteredMultipleChoiceQuestions.size()) {
                    listener.onMultipleChoiceQuestionResponse(filteredMultipleChoiceQuestions);
                } else {
                    listener.onMultipleChoiceQuestionResponse(
                            (ArrayList<MultipleChoiceQuestion>) filteredMultipleChoiceQuestions.
                                    subList(0, numberOfQuestionsRequired - 1));
                }
            }
        }

    }*/

    /*
    requestBasicQuestionsWrapper(){
//        Filter for all open questions
        /*Query difficultyQuestionQuery =
                questionTableReference.orderByChild(ORDER_TYPE_KEYWORD).
                        equalTo(ORDER_TYPE_INTERVIEW_KEYWORD);*/

        /*difficultyQuestionQuery.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                BasicQuestion temp;
                filteredBasicQuestions.clear();
                for (DataSnapshot questionSnapShot :
                        dataSnapshot.getChildren()) {

                    //Extracting all the tags from the object since they couldn't be automatically parsed
                    ArrayList<HashMap<String, String>> tagsArray =
                            questionSnapShot.child(TAGS_CHILDREN_REFERENCE).
                                    getValue(arrayTypeIndicator);

//                    Getting the question object
                    temp = questionSnapShot.getValue(BasicQuestion.class);

//                    Unpacking the tags from the map and storing them in the Question object
                    for (HashMap h :
                            tagsArray) {
                        temp.getmTags().addAll(h.values());
                    }

//                    Filtering the questions by tag and difficulty
                    if (temp.getmTags().containsAll(topicTags) &&
                            temp.getmDifficulty() == difficultyLevel) {
                        filteredBasicQuestions.add(temp);
                    }
                }

//                Sending the array of filtered results to all listeners
                for (BackendCallerListener listener :
                        listeners) {
                    listener.onBasicQuestionResponse(filteredBasicQuestions);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });* /
    }
    * */

    /*
    * requestMultipleChoiceQuestionsWrapper(){
    // Filter for all questions that are multiple choice
        /*Query typeFilterQuery = questionTableReference.orderByChild(ORDER_TYPE_KEYWORD).
                equalTo(ORDER_TYPE_TRAINING_KEYWORD);*/

        /*typeFilterQuery.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
//                Temporal object containing the data retrieved from the database
                MultipleChoiceQuestion temp;
                filteredMultipleChoiceQuestions.clear();
                GenericTypeIndicator<ArrayList<HashMap<String, Integer>>> indexesTypeIndicator =
                        new GenericTypeIndicator<ArrayList<HashMap<String, Integer>>>() {
                        };
                for (DataSnapshot questionSnapShot :
                        dataSnapshot.getChildren()) {
//                    Extracting all the tags from the object since they couldn't be automatically parsed
                    ArrayList<HashMap<String, String>> tagsArray =
                            questionSnapShot.child(TAGS_CHILDREN_REFERENCE).
                                    getValue(arrayTypeIndicator);

//                    Getting the question object
                    temp = questionSnapShot.getValue(MultipleChoiceQuestion.class);

//                    Unpacking the tags from the map and storing them in the Question object
                    for (HashMap h :
                            tagsArray) {
                        temp.getmTags().addAll(h.values());
                    }

//                    Filtering the questions by tag and difficulty
                    if (temp.getmTags().containsAll(topicTags) &&
                            temp.getmDifficulty() == difficultyLevel) {

//                        Adding indexes for correct and wrong answers
                        ArrayList<HashMap<String, Integer>> correctIndexesArray =
                                questionSnapShot.child(CORRECT_ANSWERS_CHILDREN_REFERENCE).
                                        getValue(indexesTypeIndicator);

                        ArrayList<HashMap<String, Integer>> wrongIndexesArray =
                                questionSnapShot.child(WRONG_ANSWERS_CHILDREN_REFERENCE).
                                        getValue(indexesTypeIndicator);

                        //Unpacking the indexes from the maps and storing them in the Question object
                        for (HashMap h :
                                correctIndexesArray) {
                            temp.getmCorrectAnswerIndexes().addAll(h.values());
                        }

                        for (HashMap h :
                                wrongIndexesArray) {
                            temp.getmWrongAnswerIndexes().addAll(h.values());
                        }

                        filteredMultipleChoiceQuestions.add(temp);
                    }
                }

                new AnswerRetrievalTask().execute(responseQuantity, questionQuantity);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });* /
    * }
    * */

}
