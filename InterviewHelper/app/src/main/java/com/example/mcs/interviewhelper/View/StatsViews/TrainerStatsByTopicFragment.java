package com.example.mcs.interviewhelper.View.StatsViews;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.mcs.interviewhelper.MainActivity;
import com.example.mcs.interviewhelper.R;

/**franco_trujillo
 * this fragment is in charge of show the questions from the quiz
 */
public class TrainerStatsByTopicFragment extends Fragment {


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_trainer_stats_topic, container, false);
        MainActivity.INTERACTOR.initiateStatsByTopicFragment(view);
        return view;
    }

}
