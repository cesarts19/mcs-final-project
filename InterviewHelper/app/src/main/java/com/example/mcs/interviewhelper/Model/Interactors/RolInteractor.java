package com.example.mcs.interviewhelper.Model.Interactors;

import android.app.Activity;
import android.database.Cursor;
import android.net.Uri;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mcs.interviewhelper.MainActivity;
import com.example.mcs.interviewhelper.Model.data.Command;
import com.example.mcs.interviewhelper.Model.data.ContentProvider.InterviewHelperContentProvider;
import com.example.mcs.interviewhelper.Model.data.ContentProvider.TagsTable;
import com.example.mcs.interviewhelper.Model.data.Stat;
import com.example.mcs.interviewhelper.Presenter.MainScreenPresenters.MenuPresenter;
import com.example.mcs.interviewhelper.Presenter.SuggestionPresenters.AddAndRemoveItemsAdapter;
import com.example.mcs.interviewhelper.QuestionSuggestionActivity;
import com.example.mcs.interviewhelper.QuizActivity;
import com.example.mcs.interviewhelper.R;
import com.example.mcs.interviewhelper.View.Suggestions.QuestionEditingScreenFragment;
import com.example.mcs.interviewhelper.View.Suggestions.QuestionTypeSuggestionSelectionScreenFragment;

import java.util.ArrayList;
import java.util.HashMap;

public abstract class RolInteractor implements Command {

    public static final String PERCENTAGE_OVERALL = "percentage_overall";
    public static int questionTotalGeneral = 0;
    public static int questionCorrectGeneral = 0;
    private MenuPresenter PRESENTER = null;
    public ArrayList<Stat> statsByTAG;
    public ArrayList<Stat> statsByConsultant;

    public RolInteractor(MenuPresenter PRESENTER){
        this.PRESENTER = PRESENTER;
    }

    public RolInteractor(){}

    // This abstract void is meant to control the fragments behavior on the Question sugestion activity depending on the rol.
    public abstract void goToNextScreen(QuestionSuggestionActivity activity);
    /**
     * @itzcoatl90
     *
     * This method is for controlling the previous fragment on the QuestionSuggestionActivity, regardless of rol.
     *
     * Function that returns to the previous fragment when the user press the cancel option.
     * This function is supposed to be updated.
     *
     * */
    public void goToPreviousScreen(QuestionSuggestionActivity activity) {
        if(activity.NEXT_FRAGMENT instanceof QuestionEditingScreenFragment){
            activity.setMainToolbar();
            activity.startNextFragment(activity.PREVIOUS_FRAGMENT);
            return;
        }
        throw new UnsupportedOperationException("CAN'T GO TO PREVIOUS SCREEN");
    }
    // This abstract int is meant to return the id of the Resource for the Toolbar layout file, it depends on the Rol.
    public abstract int getIdToolbar();

    // This abstract int is meant to return the id of the Resource for the Drawer menu file, it depends on the Rol.
    public abstract int getIdDrawer();


    /**
     * @itzcoatl90
     * This method is for binding the listeners to the adding buttons on the QuestionSuggestionActivity,
     * specifically on the fragment for editing the Questions (QuestionEditingScreenFragment).
     *
     * This method runs regardless of the Type of Question by ignoring the buttons that are nulls.
     *
     * */
    public void bindListenersToAddButtons(View addingTrueAnswersButton, final ArrayList<String> trueAnswersArray,
                                          final AddAndRemoveItemsAdapter trueAnswerAdapter,
                                          View addingFalseAnswersButton, final ArrayList<String> falseAnswersArray,
                                          final AddAndRemoveItemsAdapter falseAnswerAdapter,
                                          View addingTagsButton, final ArrayList<String> tagsArray,
                                          final AddAndRemoveItemsAdapter tagsAdapter){
        // First two buttons may be null if the question is BasicQuestion (Open Question type).
        if(addingTrueAnswersButton != null){
            addingTrueAnswersButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    QuestionEditingScreenFragment.question.requestFocus();
                    if(!trueAnswersArray.get(trueAnswersArray.size()-1).equals("")){
                        trueAnswersArray.add("");
                        trueAnswerAdapter.notifyDataSetChanged();
                    } else {
                        Toast.makeText(QuestionSuggestionActivity.CONTEXT,
                                "Not blanks allowed in correct answers.",Toast.LENGTH_SHORT)
                                .show();
                    }
                }
            });
        }
        if(addingFalseAnswersButton != null){
            addingFalseAnswersButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    QuestionEditingScreenFragment.question.requestFocus();
                    if(!falseAnswersArray.get(falseAnswersArray.size()-1).equals("")){
                        falseAnswersArray.add("");
                        falseAnswerAdapter.notifyDataSetChanged();
                    } else {
                        Toast.makeText(QuestionSuggestionActivity.CONTEXT,
                                "Not blanks allowed in wrong answers.",Toast.LENGTH_SHORT)
                                .show();
                    }
                }
            });
        }
        // This button is never meant to be null, but we check it anyway.
        if(addingTagsButton != null){
            addingTagsButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    QuestionEditingScreenFragment.question.requestFocus();
                    if(!tagsArray.get(tagsArray.size()-1).equals("")){
                        tagsArray.add("");
                        tagsAdapter.notifyDataSetChanged();
                    } else {
                        Toast.makeText(QuestionSuggestionActivity.CONTEXT,
                                "Not blanks allowed in topic tags.",Toast.LENGTH_SHORT)
                                .show();
                    }
                }
            });
        }
    }

    /**
     * @itzcoatl90
     * This method is for binding the listeners needed for each row, on the QuestionSuggestionActivity, in it's
     * QuestionEditingScreenFragment. This behavior is regardless of the type of the row.
     *
     * The type of the row maybe in one of theese three:
     *
     * 1.- Correct Answer
     * 2.- Wrong Answer
     * 3.- Topic Tag
     *
     * NOTE: The "Removing row bug" is meant to be fixed here and in the AddAndRemoveItemsAdapter.getView()
     *
     * */
    public void bindListenerToRemoveButton(View removeButton,
                                           //final EditText editText,
                                           final int position,
                                           //final ArrayList<String> container,
                                           final AddAndRemoveItemsAdapter adapter//,
                                           //final int lastRemoved
                                        ){
        /*final int localPosition = (position >= lastRemoved) ?
                position-1 : position;*/
        //final int localPosition = position;
        // Binding the listener to the new remove button.
        if(removeButton != null){
            removeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(adapter.list.size()<= 1) {
                        Toast.makeText(QuestionSuggestionActivity.CONTEXT,
                                "You can't remove the last item.", Toast.LENGTH_SHORT)
                                .show();
                    } else {
                        //System.out.println("TOKEN MAGICO erasing " + localPosition);
                        //System.out.println("BEFORE " + adapter.list.toString());
                        adapter.list.remove(position);
                        //System.out.println("AFTER  " + adapter.list.toString());
                        //adapter.lastRemoved = localPosition;
                        adapter.notifyDataSetChanged();
                    }
                }
            });
        }
        // Binding the listener to the change (for persisting the information in the editText).
        /*if(editText != null){
            editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                private int pos = localPosition;
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (!hasFocus) {
                        try {
                            System.out.println("TOKEN MAGICO " + localPosition + " - " + adapter.list.size());
                            //adapter.list.set(localPosition,editText.getText().toString());
                            adapter.list.set(pos,editText.getText().toString());
                        } catch(IndexOutOfBoundsException e) {
                            e.printStackTrace();
                            System.out.println("TOKEN MAGICO " + localPosition + " - " + adapter.list.size());
                        }
                        //container.set(position,editText.getText().toString());
                        //adapter.notifyDataSetChanged();
                    } else {
                        adapter.positionHolder = localPosition;
                    }
                }
            });
        }*/
    }

    public void bindListenerToEditText(
            final EditText editText,
            final int position,
            final AddAndRemoveItemsAdapter adapter
    ) {
// Binding the listener to the change (for persisting the information in the editText).
        if(editText != null){
            /*editText.addTextChangedListener(new TextWatcher() {

                @Override
                public void afterTextChanged(Editable s) {}

                @Override
                public void beforeTextChanged(CharSequence s, int start,
                                              int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start,
                                          int before, int count) {
                    System.out.println("TOKEN MAGICO ON CHANGE TO " + s.toString());
                    adapter.list.set(position,s.toString());
                    for (int i = 0; i < adapter.list.size(); i++) {
                        System.out.print("-" + adapter.list.get(i));
                    }
                    System.out.println("///////////////////////////////////////");
                }
            });*/
            editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (!hasFocus) {
                        //try {
                            //System.out.println("TOKEN MAGICO " + position + " - " + adapter.list.size());
                            //adapter.list.set(localPosition,editText.getText().toString());
                            adapter.list.set(position,editText.getText().toString());
                        //System.out.print("TOKEN MAGICO --");
                            //for (int i = 0; i < adapter.list.size(); i++) {
                                //System.out.print(adapter.list.get(i));
                            //}
                        //System.out.println();
                        //} catch(IndexOutOfBoundsException e) {
                            //e.printStackTrace();
                            //System.out.println("TOKEN MAGICO " + position + " - " + adapter.list.size());
                        //}
                        //container.set(position,editText.getText().toString());
                        //adapter.notifyDataSetChanged();
                    } else {
                        adapter.positionHolder = position;
                    }
                }
            });
        }
    }

    // This method is meant to be used for the MainActivity,
    public void bindListenersToMainScreen(MainActivity activity){
        PRESENTER.PresentMainView(activity,this);
    }

    // This method returns a new drawer setted for the toogle.
    public ActionBarDrawerToggle getDrawer(MainActivity activity,DrawerLayout drawer,Toolbar toolbar){
        return new ActionBarDrawerToggle(
                activity, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
    }

    /* This method is free for doing something on the fab button,
     it maybe useful for the "statistics" implementation if needed.*/
    public void bindListenerToHelpButtonOnMainScreen(FloatingActionButton fab){
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Help Action, not yet implemented.", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    // This method is the Listener for the default back action.
    public void bindListenerToBackButton(MainActivity activity){
        DrawerLayout drawer = (DrawerLayout) activity.findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            activity.superOnBackPressed();
        }
    }

    // This is for implementing the listeners on the Drawer menu... It depends on the Rol.
    public abstract boolean onNavigationItemSelected(MainActivity activity,MenuItem item);

    // This is for starting the very first fragment on the QuestionSuggestionActivity regardless on the Rol.
    public void startFirstFragmentOnSuggestion(QuestionSuggestionActivity activity){
        activity.setMainToolbar();
        activity.startNextFragment(new QuestionTypeSuggestionSelectionScreenFragment());
    }

    /**
     * @franco_trujillo
     * methods to manage statistics
     */
    public abstract void loadStatsFragment(Activity activity);
    public abstract void initiateStatsByTopicFragment(View view);
    public abstract void initiateStatsByConsultantFragment(View view);
    public abstract void initiateOverallFragment(View view, int percentage);

    public ArrayList<Stat> getStats(int type){

        ArrayList<Stat> stats = new ArrayList<Stat>();

        switch (type){
            case 1:
                //case 1 means stats by consultant
                //TODO get stats from somewhere else

                stats.add(new Stat("Franco",100,90));
                stats.add(new Stat("Rob",100,60));
                stats.add(new Stat("Itzco",100,70));
                break;
            case  2 :
                //TODO get stats from somewhere else
                stats.add(new Stat("Content Provider", 10, 8));
//                stats.add(new Stat("Activity", 10, 5));
//                stats.add(new Stat("Services", 10, 3));
//                stats.add(new Stat("Firebase", 15, 5));
//                stats.add(new Stat("Recycler", 10, 8));
//                stats.add(new Stat("Android", 10, 5));
//                stats.add(new Stat("JAVA", 10, 3));
//                stats.add(new Stat("Fragment", 15, 5));
                break;
            case 3:
                    /* @roberto.cervantes1988@gmail.com
                    Retrieves a Hash Map of stats from the SQLite DB and store a single Stat in a
                    list for every element in the Hash.
                    */
                HashMap<String, HashMap<String, Integer>>  dataHash =
                retrieveFromDB(MainActivity.mUser.getUserId());
                questionTotalGeneral = 0;
                questionCorrectGeneral = 0;
                    HashMap<String, Integer> tagsHash = dataHash.get("tags");
                    HashMap<String, Integer> correctHash = dataHash.get("correct");
                    //if(tagsHash.size() != 0){
                        for(String key : tagsHash.keySet()){
                            System.out.println(key + " " + tagsHash.get(key) + " " + correctHash.get(key));
                            questionTotalGeneral = questionTotalGeneral + tagsHash.get(key);
                            questionCorrectGeneral = questionCorrectGeneral + correctHash.get(key);
                            stats.add(new Stat(key, tagsHash.get(key), correctHash.get(key)));
                        }
                   // }
                break;
            default:
                break;
        }

        return stats;
    }

    /**
     * @roberto.cervantes1988@gmail.com
     *
     * Function that perform a query to the SQLite DB through a Content Provider
     * and retrieves the stats for a particular consultant, it returns a HashMap of HashMap's.
     *
     * */
    public static HashMap<String, HashMap<String, Integer>> retrieveFromDB(String user){
        HashMap<String, HashMap<String, Integer>> data = new HashMap<>();
        HashMap<String, Integer> tagsMap = new HashMap<>();
        HashMap<String, Integer> correctMap = new HashMap<>();
        Integer total = 0, correct = 0;
        Uri uri = InterviewHelperContentProvider.CONTENT_URI;
        String [] projection = new String[]{
                TagsTable.COLUMN_ID,
                TagsTable.COLUMN_USER,
                TagsTable.COLUMN_TAG,
                TagsTable.COLUMN_TOTAL,
                TagsTable.COLUMN_CORRECT
        };

        String selectionClause = TagsTable.COLUMN_USER + " = ?";
        String[] selectionArgs = { user };
        Cursor cursor = MainActivity.CONTEXT.getContentResolver().query(
                uri,
                projection,
                selectionClause,
                selectionArgs,
                null
        );
        if(cursor.getCount() == 0){
            System.out.println("Cursor is empty he first time until consultor perform a Quiz");
        }else{
            cursor.moveToFirst();
            do{
                System.out.println(cursor.getString(cursor.getColumnIndex(TagsTable.COLUMN_TAG)));
                String tag = cursor.getString(cursor.getColumnIndex(TagsTable.COLUMN_TAG));
                if(tagsMap.containsKey(tag)){
                    total = tagsMap.get(tag) + 1;
                    tagsMap.put(tag, total);
                    if(cursor.getInt(cursor.getColumnIndex(TagsTable.COLUMN_CORRECT)) == 1){
                        correct = correctMap.get(tag) + 1;
                        correctMap.put(tag, correct);
                    }
                }else{
                    tagsMap.put(tag, 1);
                    if(cursor.getInt(cursor.getColumnIndex(TagsTable.COLUMN_CORRECT)) == 1){
                        correctMap.put(tag, 1);
                    }else{
                        correctMap.put(tag, 0);
                    }
                }

            }while (cursor.moveToNext());
        }
        data.put("tags", tagsMap);
        data.put("correct", correctMap);
        return data;
    }

    public abstract void getQuestionsFromDB();


}
