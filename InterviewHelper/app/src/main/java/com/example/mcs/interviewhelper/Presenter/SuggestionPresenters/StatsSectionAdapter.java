package com.example.mcs.interviewhelper.Presenter.SuggestionPresenters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class StatsSectionAdapter extends FragmentPagerAdapter{

    private final List<Fragment> mFragmentList = new ArrayList<>();
    public void addFragment(Fragment fragment){
        mFragmentList.add(fragment);
    }
    public void addFragments(ArrayList<Fragment> fragments){
        mFragmentList.addAll(fragments);
    }

    public StatsSectionAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }
}
