package com.example.mcs.interviewhelper.Model.data;

public interface Command {
    void executeOnSendSuccessful();
    void executeOnSendFail();
    void executeOnReceiveSuccessful();
    void executeOnReceiveFail();
}
