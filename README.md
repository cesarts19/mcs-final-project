-- README to show the steps to cloning a project and pushing changes --

Cloning the project from repository:

Using SourceTree:
- Download and install SourceTree  (https://www.sourcetreeapp.com/)
- Follow the installation wizard and enter your bitbucket credentials
- If asked for importing an SSH select No
- Go to https://bitbucket.org/cesarts19/mcs-final-project
- Press the button "Clone in sourcetree" under the Overview header
  OR
  * Open SourceTree
  * Open a new tab and select clone
  * Paste the URL under the Overview header
  * Select directory to download the copy.
  * Press clone
- Open Android Studio
- In Android Studio select Open
- Navigate to the directory where the project was cloned and select the Android project
- If you can't see the VCS update and commit buttons beside the Graddle sync button:
    *Go to menu VCS > Enable version control integration
- Select Git
- Press update project (Downward blue arrow or Ctrl+T)

Pushing changes:

From Android Studio:
- Press update project
- Resolve conflicts if needed
- Press "Commit changes" (Upward green arrow or Ctrl+K)
- Write a descriptive comment about the changes made
- Select the files whith relevant changes to be commited
- Select commit and push
- Commit
- Select branch
- Push